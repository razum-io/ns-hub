﻿# Start the docker machine
docker-machine start
docker-machine env | iex
$machineIP = docker-machine ip

echo "Docker machine IP: $machineIP

# Docker hosts that need adding to the host's host file.
$machineIP uws.docker.local uws.edweb.docker.local
$machineIP portainer.uws.docker.local mailhog.uws.docker.local

"

# Testing only. Ensure containers are removed from last run before bringing up again.
# NB You will reset the database if you run this!
# docker-compose down | Out-Null

# Bring up the containers.
docker-compose up -d

# Identify the Drupal PHP container.
$container = docker ps -aqf "name=php"  
Set-Item Env:DRUSH_CONTAINER $container

# To avoid a wodby include in the settings.php file being committed, we block it with a comment and
# include it here.  NB it is ignored in the .gitignore file.
docker exec $container cp /var/www/conf/wodby.settings.php /var/www/html/sites/default/settings.docker.inc

# We create a special version of the release.properties file without # comments which can cause some parsing errors.
docker exec $container sh -c "grep -v '#' /var/www/conf/release.properties > /var/www/conf/modules.ini"

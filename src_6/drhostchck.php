<?php
/*
 * Script for testing if choosen webhosting meet Drupal 5.0 - 7.0 system requirements.
 * Copyright (c) 2011 by http://www.seo-web-design-aberdeen.co.uk
 */
?>


<style>
body, td {font-family: sans-serif;}
table {border-collapse: collapse;margin-left: auto; margin-right: auto; text-align: left;width:400px;}
td, th { border: 1px solid #000000; font-size: 75%; vertical-align: baseline;}
.e {background-color: #ccccff; font-weight: bold; color: #000000;}
.h {background-color: #9999cc; font-weight: bold; color: #000000;font-size: 120%;text-align:center;}
.ok {background-color: #87c187; color: #000000;text-align:center;}
.issue {background-color: #f85454;}
.warning {background-color: #edc37b;}
.f {width:10%;}
</style>
<title>Drupal hosting check</title>

<table>
    <td class="h">Drupal hosting check</td>
</table>

<?php

/*
 * MySQL account - set up this variables to fit your setting. You will likely need setup database and database user account in your hosting control panel.
 */

$db_host = 'localhost';
$db_pass = 'ghTTjku88lloKlls';
$db_user = 'WiKidbfAdmin';

/*
 * Common variables
 */

print '<table>';
$results['drupal_5']['name'] = "Drupal 5.x";
$results['drupal_6']['name'] = "Drupal 6.x";
$results['drupal_7']['name'] = "Drupal 7.x";

/*
 * Check PHP maximum execution time
 */

$exectime = ini_get('max_execution_time');
$seconds = $exectime % 60;
print '<tr><td class="e">PHP max execution time</td>';
if ($exectime < 60) {
     print "<td>" . $exectime . "seconds." . "</td></tr>";
     if ($exectime < 30) {
         $results['drupal_5']['content']['exectime']['value'] = $results['drupal_6']['content']['exectime']['value'] = $results['drupal_7']['content']['exectime']['value'] = "PHP max execution time is insufficient. Many admin scripts will need more. Ask your provider for increase or change hosting.";
         $results['drupal_5']['content']['exectime']['flag'] = $results['drupal_6']['content']['exectime']['flag'] = $results['drupal_7']['content']['exectime']['flag'] = "issue";
     } else {
         $results['drupal_5']['content']['exectime']['value'] = $results['drupal_6']['content']['exectime']['value'] = $results['drupal_7']['content']['exectime']['value'] = "PHP max execution time is so-so useful. Simple website will work fine, but for complex sites you should ask your hosting for increase.";
         $results['drupal_5']['content']['exectime']['flag'] = $results['drupal_6']['content']['exectime']['flag'] = $results['drupal_7']['content']['exectime']['flag'] = "warning";
     }
} else {
    printf ("<td>%d minutes %d seconds</td></tr>", $exectime / 60, $seconds);
}

/*
 * Check PHP memory limit
 */

preg_match('/^[0-9]*/',ini_get('memory_limit'),$memlimit);
if ($memlimit[0] < 16) {
         $results['drupal_5']['content']['memlimit']['value'] = $results['drupal_6']['content']['memlimit']['value'] = $results['drupal_7']['content']['memlimit']['value'] = "PHP memory limit is insufficient. Ask your provider for increase or change hosting.";
         $results['drupal_5']['content']['memlimit']['flag'] = $results['drupal_6']['content']['memlimit']['flag'] = $results['drupal_7']['content']['memlimit']['flag'] = "issue";
     } elseif ($memlimit[0] < 32) {
         $results['drupal_7']['content']['memlimit']['value'] = "PHP memory limit is insufficient. Ask your provider for increase or change hosting.";
         $results['drupal_7']['content']['memlimit']['flag'] = "issue";
     } elseif ($memlimit[0] < 96) {
         $results['drupal_5']['content']['memlimit']['value'] = $results['drupal_6']['content']['memlimit']['value'] = $results['drupal_7']['content']['memlimit']['value'] = "PHP memory limit is so-so useful. Basic Drupal website will work fine, but complex site with views will need more. Ask your hosting for increasing to at least 96M.";
         $results['drupal_5']['content']['memlimit']['flag'] = $results['drupal_6']['content']['memlimit']['flag'] = $results['drupal_7']['content']['memlimit']['flag'] = "warning";
     }
print '<tr><td class="e">PHP memory limit</td><td>' . $memlimit[0] . "M</td></tr>";

/*
 * Check MySQL extension
 */


print (function_exists('mysqli_connect')) ? '<tr><td class="e">MySQLi connector</td><td>Presented</td></tr>' : '<tr><td class="e">MySQLi connector</td><td>Missing</td></tr>';
print (function_exists('mysql_connect')) ? '<tr><td class="e">MySQL connector</td><td>Presented</td></tr>' : '<tr><td class="e">MySQL connector</td><td>Missing</td></tr>';
if (!function_exists('mysqli_connect') && !function_exists('mysql_connect')) {
    $results['drupal_5']['content']['mysqlconnector']['value'] = $results['drupal_6']['content']['mysqlconnector']['value'] = $results['drupal_7']['content']['mysqlconnector']['value'] = "MySQL connector is missing, As MySQL is the best supported database for Drupal, you should change hosting.";
    $results['drupal_5']['content']['mysqlconnector']['flag'] = $results['drupal_6']['content']['mysqlconnector']['flag'] = $results['drupal_7']['content']['mysqlconnector']['flag'] = "issue";
}

/*
 * Check GD library
 */

if (function_exists("gd_info")) {
    print '<tr><td class="e">GD library</td><td>Presented</td></tr>';
} else {
    $results['drupal_5']['content']['gd']['value'] = $results['drupal_6']['content']['gd']['value'] = $results['drupal_7']['content']['gd']['value'] = "GD library is not presented. Ask your hosting provider for upload it or find another hosting.";
    $results['drupal_5']['content']['gd']['flag'] = $results['drupal_6']['content']['gd']['flag'] = $results['drupal_7']['content']['gd']['flag'] = "issue";
    print '<tr><td class="e">GD library</td><td>Not presented</td></tr>';
}

/*
 * Check if session Autostart is off
 */

if (!ini_get('session.auto_start')) {
    print '<tr><td class="e">Session Autostart</td><td>Off</td></tr>';
} else {
    print '<tr><td class="e">Session Autostart</td><td>On</td></tr>';
    $results['drupal_5']['content']['autostart']['value'] = $results['drupal_6']['content']['autostart']['value'] = $results['drupal_7']['content']['autostart']['value'] = "PHP Session Autostart is on. Ask your provider to set it off.";
    $results['drupal_5']['content']['autostart']['flag'] = $results['drupal_6']['content']['autostart']['flag'] = $results['drupal_7']['content']['autostart']['flag'] = "issue";
}

/*
 * Check if PHP safe mode is off
 */

if (!ini_get('safe_mode')) {
    print '<tr><td class="e">PHP Safe Mode</td><td>Off</td></tr>';
} else {
    print '<tr><td class="e">PHP Safe Mode</td><td>On</td></tr>';
    $results['drupal_5']['content']['safemode']['value'] = $results['drupal_6']['content']['safemode']['value'] = $results['drupal_7']['content']['safemode']['value'] = "PHP Safe Mode is on. Ask your provider to set it off.";
    $results['drupal_5']['content']['safemode']['flag'] = $results['drupal_6']['content']['safemode']['flag'] = $results['drupal_7']['content']['safemode']['flag'] = "issue";
}

/*
 * Check PDO
 */

if (extension_loaded ('PDO')) {
    print '<tr><td class="e">PDO extension</td><td>Loaded</td></tr>';
} else {
    $results['drupal_7']['content']['pdo']['value'] = "PHP PDO is not loaded.";
    $results['drupal_7']['content']['pdo']['flag'] = "issue";
    print '<tr><td class="e">PDO extension</td><td>Not loaded</td></tr>';
}

/*
 * Check PHP version
 */

$phpver = phpversion();
if (version_compare($phpver,'4.4.0') === -1) {
    $results['drupal_5']['content']['phpver']['value'] = $results['drupal_6']['content']['phpver']['value'] = $results['drupal_7']['content']['phpver']['value'] = "PHP version is too low. Ask your hosting provider for upgrade to version 5.2.5.";
    $results['drupal_5']['content']['phpver']['flag'] = $results['drupal_6']['content']['phpver']['flag'] = $results['drupal_7']['content']['phpver']['flag'] = "issue";
} elseif (version_compare($phpver,'5.2.5') === -1) {
    $results['drupal_7']['content']['phpver']['value'] = "PHP version is too low. Ask your hosting provider for upgrade to version 5.2.5 or more.";
    $results['drupal_7']['content']['phpver']['flag'] = "issue";
} elseif (version_compare($phpver,'5.3') >= 0) {
    $results['drupal_5']['content']['phpver']['value'] = $results['drupal_6']['content']['phpver']['value'] = "PHP version is too high. Ask your hosting provider for downgrade to version 5.2.5.";
    $results['drupal_5']['content']['phpver']['flag'] = $results['drupal_6']['content']['phpver']['flag'] = "issue";
}
print '<tr><td class="e">PHP version</td><td>' . $phpver . '</td></tr>';

/*
 * Check if Register globals is off
 */

if (!ini_get('register_globals')) {
    print '<tr><td class="e">PHP Register Globals</td><td>Off</td></tr>';
} else {
    print '<tr><td class="e">PHP Register Globals</td><td>On</td></tr>';
    $results['drupal_5']['content']['reglob']['value'] = $results['drupal_6']['content']['reglob']['value'] = $results['drupal_7']['content']['reglob']['value'] = "PHP Register Globals is on. Ask your provider to set it off.";
    $results['drupal_5']['content']['reglob']['flag'] = $results['drupal_6']['content']['reglob']['flag'] = $results['drupal_7']['content']['reglob']['flag'] = "issue";
}

/*
 * Check XML extension
 */

if (class_exists('DOMDocument')) {
    print '<tr><td class="e">XML extension</td><td>Enabled</td></tr>';
} else {
    print '<tr><td class="e">XML extension</td><td>Disabled</td></tr>';
    $results['drupal_5']['content']['xml']['value'] = $results['drupal_6']['content']['xml']['value'] = $results['drupal_7']['content']['xml']['value'] = "XML extension is disabled. Ask your hosting provider for enabling or change hosting.";
    $results['drupal_5']['content']['xml']['flag'] = $results['drupal_6']['content']['xml']['flag'] = $results['drupal_7']['content']['xml']['flag'] = "issue";
}

/*
 * Check Mod_rewrite
 */

if (function_exists('apache_get_modules')) {
    if (in_array('mod_rewrite',apache_get_modules())) {
        print '<tr><td class="e">Apache mod_rewrite</td><td>Enabled</td></tr>';
    } else {
        $results['drupal_5']['content']['modrewrite']['value'] = $results['drupal_6']['content']['modrewrite']['value'] = $results['drupal_7']['content']['modrewrite']['value'] = "Apache mod_rewrite is disabled. Ask your hosting provider for enabling or change hosting.";
        $results['drupal_5']['content']['modrewrite']['flag'] = $results['drupal_6']['content']['modrewrite']['flag'] = $results['drupal_7']['content']['modrewrite']['flag'] = "issue";
        print '<tr><td class="e">Apache mod_rewrite</td><td>Disabled</td></tr>';
    }
}

/*
 * Check MySQL setting
 */

if (@mysql_connect($db_host, $db_user, $db_pass)) {

    // Max allowed packet check
    $result = mysql_query("SHOW VARIABLES LIKE 'max_allowed_packet'");
    $row = mysql_fetch_assoc($result);
    $max_allpacket = $row['Value'] / 1048576;
    if ($max_allpacket < 16) {
        $results['drupal_5']['content']['ma_packet']['value'] = $results['drupal_6']['content']['ma_packet']['value'] = $results['drupal_7']['content']['ma_packet']['value'] = "Max Allowed Packet in MySQL is too low. Ask your hosting provider to increase it to at least 16M.";
        $results['drupal_5']['content']['ma_packet']['flag'] = $results['drupal_6']['content']['ma_packet']['flag'] = $results['drupal_7']['content']['ma_packet']['flag'] = "issue";
    }
    printf ('<tr><td class="e">MySQL Max allowed packet</td><td>%d MB</td></tr>', $max_allpacket);

    // Check MySQL version
    $result = mysql_query("SELECT VERSION()");
    $row = mysql_fetch_assoc($result);
    preg_match('/^[0-9.]*/',$row['VERSION()'],$result);
    $result = $result[0];

    if (version_compare($result,'3.23.17') === -1) {
        $results['drupal_5']['content']['mysql_version']['value'] = $results['drupal_6']['content']['mysql_version']['value'] = $results['drupal_7']['content']['mysql_version']['value'] = "Installed MySQL is too old.";
        $results['drupal_5']['content']['mysql_version']['flag'] = $results['drupal_6']['content']['mysql_version']['flag'] = $results['drupal_7']['content']['mysql_version']['flag'] = "issue";
    } elseif (version_compare($result,'4.1') === -1) {
        $results['drupal_6']['content']['mysql_version']['value'] = $results['drupal_7']['content']['mysql_version']['value'] = "Installed MySQL is too old.";
        $results['drupal_6']['content']['mysql_version']['flag'] = $results['drupal_7']['content']['mysql_version']['flag'] = "issue";
    } elseif (version_compare($result,'5.0.15') === -1) {
        $results['drupal_7']['content']['mysql_version']['value'] = "Installed MySQL is too old.";
        $results['drupal_7']['content']['mysql_version']['flag'] = "issue";
    }
    print '<tr><td class="e">MySQL version</td><td>' . $result . '</td></tr>';
} elseif (is_null($results['drupal_5']['content']['mysqlconnector'])) {
    $results['drupal_5']['content']['mysql_version']['value'] = $results['drupal_6']['content']['mysql_version']['value'] = $results['drupal_7']['content']['mysql_version']['value'] = "Unable to connect MySQL server, check your account settings. This is likely your and not web hostings fault.";
    $results['drupal_5']['content']['mysql_version']['flag'] = $results['drupal_6']['content']['mysql_version']['flag'] = $results['drupal_7']['content']['mysql_version']['flag'] = "issue";
}
print "</table><br />";

foreach ($results as $drupal => $result) {
    print '<table>';
    print '<tr><td class="h">' . $result['name'] . '</td></tr>';
    if (is_null($result['content'])) {
        print '<tr><td class="ok">Your hosting seems to be OK for this version of Drupal.</td></tr></table><br />';
    } else {
        print '</table><table>';
        foreach ($result['content'] as $type => $values) {
            print '<tr class="' . $values['flag'] . '">';
            print ($values['flag'] == "issue") ? '<td class="f">!!!</td>' : '<td class="f">!</td>';
            print '<td>' . $values['value'] . '</td></tr>';
        }
        print '</table><br />';
    }
}
?>

<p style="text-align:center;">Copyright (c) 2011 by <a href ="http://www.seo-web-design-aberdeen.co.uk">http://www.seo-web-design-aberdeen.co.uk</a></p>
<?php
 /**
  * This template is used to print a single field in a view. It is not
  * actually used in default Views, as this is registered as a theme
  * function which has better performance. For single overrides, the
  * template is perfectly okay.
  *
  * Variables available:
  * - $view: The view object
  * - $field: The field handler object that can process the input
  * - $row: The raw SQL result that can be used
  * - $output: The processed output that will normally be used.
  *
  * When fetching output from the $row, this construct should be used:
  * $data = $row->{$field->field_alias}
  *
  * The above will guarantee that you'll always get the correct data,
  * regardless of any changes in the aliasing that might happen if
  * the view is modified.
  */
?>
<?php  
    preg_match_all('/<a[^>]+href=([\'"])(.+?)\1[^>]*>/i', $output, $result);
    preg_match_all("/<a[^>]*>(.*?)<\\/a>/si", $output, $match);
    $stripped = strip_tags($output);
    if (isset($match[1][0]) && (strpos($stripped, $match[1][0]) !== false) && (!isset($match[1][1]) || (strpos($stripped, $match[1][1]) !== false))) {
        $chopped = trim(str_replace($match[1][0], "", $stripped));
        $url = substr($result[2][0], 1);
        if ($router_item = menu_get_item($url)) {
            if ($router_item['access']) {
                $better_output = l($match[1][0], $url);
            } else {
                $better_output = $match[1][0];
            }
        } else {
            $better_output = $match[1][0];
        }
        if (isset($match[1][1])) {
            $better_output .= " " . trim(str_replace($match[1][1], "", $chopped)) . " ";
            $url = drupal_get_normal_path(urldecode(substr($result[2][1], 1)));
            if ($router_item = menu_get_item($url)) {
                if ($router_item['access']) {
                    $better_output .= l($match[1][1], $url);
                } else {
                    $better_output .= $match[1][1];
                }
            } else {
                $better_output .= $match[1][1];
            }
        } else {
            $better_output .= " " . $chopped;
        }
        print $better_output;
    } else {
        print $output;
    }
?>
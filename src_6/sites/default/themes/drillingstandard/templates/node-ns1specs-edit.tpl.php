<?php print drupal_render($form['title']); ?>
<?php print drupal_render($form['field_list_title']); ?>
<?php print drupal_render($form['group_specification']); ?>

<fieldset>
    <legend>Material</legend>
    <?php
    foreach ($form['field_material']['value']['#options'] as $label => $value) {
        $counter++;
        if ($counter == 1) {print '<div class="clear-block">';}
        print '<div class="itemclass">' . drupal_render($form['field_material']['value'][$label]) . '</div>';
        if ($counter == 4) {$counter = 0; print '</div>';}
    }
    if ($counter != 0) { print '</div>'; $counter = 0; }
    unset($form['field_material']);
    ?>
</fieldset>

<fieldset>
    <legend>Service</legend>
    <?php
    foreach ($form['field_service']['value']['#options'] as $label => $value) {
        $counter++;
        if ($counter == 1) {print '<div class="clear-block">';}
        print '<div class="itemclass">' . drupal_render($form['field_service']['value'][$label]) . '</div>';
        if ($counter == 4) {$counter = 0; print '</div>';}
    }
    if ($counter != 0) { print '</div>'; $counter = 0; }
    unset($form['field_service']);
    ?>
</fieldset>


<fieldset>
    <legend>Items</legend>
    <?php
    foreach ($form['field_sub_specification']['nid']['nid']['#options'] as $label => $value) {
        $counter++;
        if ($counter == 1) {print '<div class="clear-block">';}
        print '<div class="itemclass">' . drupal_render($form['field_sub_specification']['nid']['nid'][$label]) . '</div>';
        if ($counter == 4) {$counter = 0; print '</div>';}
    }
    if ($counter != 0) { print '</div>'; $counter = 0; }
    unset($form['field_sub_specification']);
    ?>
</fieldset>

<?php
  $form['buttons']['#weight'] = 50;
  print drupal_render($form);

  //Enable below to show all Array Variables of Form

  //print '<pre>';
  //print_r($form);
  //print '</pre>';

?>
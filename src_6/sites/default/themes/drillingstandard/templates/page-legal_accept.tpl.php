<?php
global $user;
if (!in_array('anonymous user', array_values($user->roles))) {
    include_once("page.tpl.php");
  } else {
    include_once("legal_templates/anonymous.php");
  }
?>
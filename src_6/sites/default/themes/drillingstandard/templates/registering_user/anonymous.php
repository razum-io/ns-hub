<?php
/**
 * @file
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $submitted: Themed submission information output from
 *   theme_node_submitted().
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $build_mode: Build mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $build_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * The following variable is deprecated and will be removed in Drupal 7:
 * - $picture: This variable has been renamed $user_picture in Drupal 7.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess()
 * @see zen_preprocess_node()
 * @see zen_process()
 */
?>
<div id="frontpage-login" class="tweakedaccount">
    <div id="flp-inner">
        <div id="flp-inner-content">
            <div class="login-menu-login clear-block">
                <a href="http://www.fearnleyproctergroup.com" title="FPG Home" class="lm-fr"><img src="/sites/default/themes/drillingstandard/images/hexagon.png" alt="FPG Logo" title="FPG Home"></a>
                <a href="/user/password" class="lm-all">NEW PASSWORD</a>
                <a href="/user" class="lm-all">LOGIN</a>
                <a href="/aboutus" class="lm-all">ABOUT</a>
                <a href="/legal" class="lm-all">LEGAL</a>
                <a href="/bulletins" class="lm-all">BULLETINS</a>
            </div>
            <div class="clear-block">
                <div class="main-logo-pass">
                    <h1 title="NS Drilling Standards"><a href="/" title="NS Drilling Standards">NS Drilling Standards</a></h1>
                </div>

<?php
   unset($form['account']['name']['#title']);
   unset($form['account']['name']['#description']);
   $form['account']['name']['#value'] = "USERNAME";
   $form['account']['name']['#attributes']['class'] = "user-user";
   $form['account']['name']['#attributes']['onFocus'] = "clearText(this)";
   $form['account']['name']['#attributes']['onBlur'] = "clearText(this)";
   $form['account']['name']['#size'] = "25";
   $form['account']['name']['#maxlength'] = "60";
   unset($form['account']['mail']['#title']);
   unset($form['account']['mail']['#description']);
   $form['account']['mail']['#maxlength'] = "60";
   $form['account']['mail']['#attributes']['class'] = "password-clear";
   $form['account']['mail']['#attributes']['onFocus'] = "clearText(this)";
   $form['account']['mail']['#attributes']['onBlur'] = "clearText(this)";
   $form['account']['mail']['#size'] = "15";
   $form['account']['mail']['#value'] = "EMAIL";
   $form['submit']['#attributes']['class'] = "my-user-submit";
?>

        <div id="passblock">
            <h2>Create new account</h2>
            <?php
                 /* delete after
                print '<pre>';
                print_r($form);
                print '</pre>';
                //unset($form['legal']['#title']);
                print drupal_render($form['legal']['extras-1']);
                print drupal_render($form['legal']['legal_accept']);
                unset($form['legal']);
                /**/

                print drupal_render($form['account']['name']); // prints the username field
                print drupal_render($form['account']['mail']); // print the email field
                unset($form['account']);
                unset($form['captcha']['captcha_widgets']['captcha_response']['#title']);
                unset($form['captcha']['captcha_widgets']['captcha_response']['#title']);
                unset($form['captcha']['captcha_widgets']['captcha_response']['#description']);
                $form['captcha']['captcha_widgets']['captcha_response']['#attributes']['value'] = "Enter letters from image";
                $form['captcha']['#pre_render'][0] = "overrides_pre_render_process";
$form['captcha']['captcha_widgets']['captcha_image']['#prefix'] = '<fieldset class="captcha"><legend>CAPTCHA</legend><div class="captcha-image-wrapper">';
$form['captcha']['captcha_widgets']['captcha_image']['#suffix'] = '</div>';
/*
print "<pre>";
     print_r($form['captcha']['captcha_widgets']);
print "</pre>";
 */
$form['captcha']['captcha_widgets']['captcha_response']['#attributes']['onFocus'] = "clearText(this)";
$form['captcha']['captcha_widgets']['captcha_response']['#attributes']['onBlur'] = "clearText(this)";
print drupal_render($form['captcha']);
print drupal_render($form['legal']['legal_accept']);
unset($form['legal']);
print '<p>' . drupal_render($form['submit']) . '</p>'; // print the submit button
?>
</div>
</div>
<?php
print drupal_render($form); //print remaining form elements like "create new account"
?>
</div></div></div>
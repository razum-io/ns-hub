<?php print drupal_render($form['title']); ?>
<?php print drupal_render($form['field_list_title']); ?>

<fieldset>
    <legend>Class</legend>
    <?php
    foreach ($form['field_class']['value']['#options'] as $label => $value) {
        $counter++;
        if ($counter == 1) {print '<div class="clear-block">';}
        print '<div class="itemclass">' . drupal_render($form['field_class']['value'][$label]) . '</div>';
        if ($counter == 4) {$counter = 0; print '</div>';}
    }
    if ($counter != 0) { print '</div>'; $counter = 0; }
    unset($form['field_class']);
    ?>
</fieldset>

<fieldset>
    <legend>Material</legend>
    <?php
    foreach ($form['field_material_list']['value']['#options'] as $label => $value) {
        $counter++;
        if ($counter == 1) {print '<div class="clear-block">';}
        print '<div class="itemmaterial">' . drupal_render($form['field_material_list']['value'][$label]) . '</div>';
        if ($counter == 3) {$counter = 0; print '</div>';}
    }
    if ($counter != 0) { print '</div>'; $counter = 0; }
    unset($form['field_material_list']);
    ?>
</fieldset>


<fieldset>
    <legend>Specification</legend>
    <?php
    foreach ($form['field_sub_specification_item']['nid']['nid']['#options'] as $label => $value) {
        $chunks = explode('.',$value);
        $mynumber = '';
        foreach ($chunks as $onechunk) {
            $mynumber .= substr('0000' . trim(strip_tags($onechunk)),-4);
        }
        $sortedarray[substr($mynumber . '000000000000000', 0, 16)] = $label;
    }
    ksort($sortedarray);
    foreach ($sortedarray as $value => $label) {
        $counter++;
        if ($counter == 1) {print '<div class="clear-block">';}
        print '<div class="itemclass">' . drupal_render($form['field_sub_specification_item']['nid']['nid'][$label]) . '</div>';
        if ($counter == 4) {$counter = 0; print '</div>';}
    }
    if ($counter != 0) { print '</div>'; $counter = 0; }
    unset($form['field_sub_specification_item']);
    ?>
</fieldset>

<?php
  $form['buttons']['#weight'] = 50;
  print drupal_render($form);

  //Enable below to show all Array Variables of Form

  //print '<pre>';
  //print_r($form);
  //print '</pre>';

?>
<?php
global $user;
if (!in_array('anonymous user', array_values($user->roles))) {
    include_once("registering_user/authenticated.php");
  } else {
    include_once("registering_user/request-account.php");
  }
?>
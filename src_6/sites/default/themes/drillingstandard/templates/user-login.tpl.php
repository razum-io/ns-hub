<?php
/**
 * @file
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $display_submitted: whether submission information should be displayed.
 * - $submitted: Themed submission information output from
 *   theme_node_submitted().
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 *   The following applies only to viewers who are registered users:
 *   - node-by-viewer: Node is authored by the user currently viewing the page.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $build_mode: Build mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $build_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * The following variable is deprecated and will be removed in Drupal 7:
 * - $picture: This variable has been renamed $user_picture in Drupal 7.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see zen_preprocess()
 * @see zen_preprocess_node()
 * @see zen_process()
 */
?>
<div id="frontpage-login" class="tweakedlogin">
    <div class="main-logo-log">
    <div class="login-menu-login clear-block">
        <a href="http://www.fearnleyproctergroup.com" title="FPG Home" class="lm-fr"><img src="/sites/default/themes/drillingstandard/images/hexagon.png" alt="FPG Logo" title="FPG Home"></a>
        
        <a href="/user/password" class="lm-all">NEW PASSWORD</a>
        <a href="/legal" class="lm-all">LEGAL</a>
        <a href="/<?php print drupal_get_path_alias('node/1901'); ?>" class="lm-all">REGISTER</a>
        <a href="/aboutus" class="lm-all">ABOUT</a>
        <a href="https://www.fp.international/technical-bulletins-nih" class="lm-all">BULLETINS</a>
    </div>
    <h1 class="main-logo-header" title="NS Drilling Standards">
        <a href="/" title="NS Drilling Standards">NS Drilling Standards</a>
    </h1>
    </div>

<?php
   unset($form['name']['#title']);
   unset($form['name']['#description']);
   $form['name']['#value'] = "USERNAME";
   $form['name']['#attributes']['class'] = "user-user";
   $form['name']['#attributes']['onFocus'] = "clearText(this)";
   $form['name']['#attributes']['onBlur'] = "clearText(this)";
   $form['name']['#size'] = "15";
   unset($form['pass']['#title']);
   unset($form['pass']['#description']);
   $form['pass']['#attributes']['value'] = "";
   $form['pass']['#attributes']['autocomplete'] = "off";
   $form['pass']['#attributes']['class'] = "password-password";
   $form['pass']['#size'] = "15";
   $form['password-clear']['#attributes']['class'] = "password-clear";
   $form['password-clear']['#attributes']['autocomplete'] = "off";
   $form['password-clear']['#size'] = "15";
   $form['password-clear']['#maxlength'] = "60";
   $form['password-clear']['#id'] = "password-clear";
   $form['password-clear']['#value'] = "PASSWORD";
   $form['password-clear']['#type'] = "textfield";
   $form['submit']['#attributes']['class'] = "my-user-submit-img";
?>

<div id="loginblock">
<div class="clear-block user-credit">
  <?php
     print drupal_render($form['name']); // prints the username field
  ?>
  <div class="passwords form-item">
     <?php
        print drupal_render($form['pass']); // print the password field
        print drupal_render($form['password-clear']); // print the password field
     ?>
  </div><!-- passwords -->
</div><!-- clear  -->
<div class="clear-block login-menu-wrapper">
<div class="login-offer">
<?php
print drupal_render($form['submit']); // print the submit button
?>
</div>
</div>
<?php
print drupal_render($form); //print remaining form elements like "create new account"
?>
</div><!-- loginblock -->
</div><!-- /.node -->

<?php
/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, intercept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to drillingstandard_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: drillingstandard_breadcrumb()
 *
 *   where drillingstandard is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */


/**
 * Implementation of HOOK_theme().
 */
function drillingstandard_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:

  $hooks['user_login'] = array(
      'template' => 'templates/user-login',
      'arguments' => array('form' => NULL)
      );

  $hooks['user_register'] = array(
      'template' => 'templates/user-register',
      'arguments' => array('form' => NULL)
      );

  $hooks['user_pass'] = array(
      'template' => 'templates/user-pass',
      'arguments' => array('form' => NULL)
      );

  $hooks['items_node_form'] = array(
        'arguments' => array('form' => NULL),
        'template' => '/templates/node-items-edit' //specify the templates directory
    );

  $hooks['ns1_specs_node_form'] = array(
        'arguments' => array('form' => NULL),
        'template' => '/templates/node-ns1specs-edit' //specify the templates directory
    );
  $hooks['webform_form_1901'] = array(
        'arguments' => array('form' => NULL),
        'template' => '/templates/user-request' //specify the templates directory
    );
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}

/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
/* -- Delete this line if you want to use this function
function drillingstandard_preprocess(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function drillingstandard_preprocess_page(&$vars, $hook) {
  if (arg(0) == 'node' && is_numeric(arg(1)) && arg(2) == 'edit') {
      $node = node_load(arg(1));
      if (in_array($node->type,array('ns1_specs','items','taxo_addi_pdf','node_addi_pdf'))) {
          drillingstandard_remove_tab('View', $vars);
          drillingstandard_remove_tab('Edit', $vars);
      }
  }
    // Registration webform
  if (arg(0) == 'node' && arg(1) == '1901' && arg(2) == 'webform-results') {
      $tabs = explode("\n", $vars['tabs']);
      $vars['tabs'] = '';

      foreach ($tabs as $tab) {
        if (strpos($tab, '>View<') === FALSE) {
            $vars['tabs'] .= $tab . "\n";
        }
      }
      if (strpos($vars['tabs'], '<ul') !== 0) {
        $vars['tabs'] = '<ul class="tabs primary clear-block">' . $vars['tabs'];
      }
      if (strpos($vars['tabs'], '</ul') === FALSE) {
        $vars['tabs'] = $vars['tabs'] . '</ul>';
      }
  }
  // To remove a class from $classes_array, use array_diff().
  //$vars['classes_array'] = array_diff($vars['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function drillingstandard_preprocess_node(&$vars, $hook) {
  // $vars['sample_variable'] = t('Lorem ipsum.');
  // Optionally, run node-type-specific preprocess functions, like
  // drillingstandard_preprocess_node_page() or drillingstandard_preprocess_node_story().
  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars, $hook);
  }
}

function drillingstandard_preprocess_node_wiki(&$variables, $hook) {
    unset($variables['links']);
}

function drillingstandard_preprocess_node_page(&$variables, $hook) {
    unset($variables['links']);
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function drillingstandard_preprocess_comment(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function drillingstandard_preprocess_block(&$vars, $hook) {
  $vars['sample_variable'] = t('Lorem ipsum.');
}
//  

 */

function drillingstandard_remove_tab($label, &$vars) {
  $tabs = explode("\n", $vars['tabs']);
  $vars['tabs'] = '';

  foreach ($tabs as $tab) {
    if (strpos($tab, '>' . $label . '<') === FALSE) {
      $vars['tabs'] .= $tab . "\n";
    }
  }
  if (strpos($vars['tabs'], '<ul') === FALSE) {
    $vars['tabs'] = '<ul class="tabs primary clear-block">' . $vars['tabs'];
  }
  if (strpos($vars['tabs'], '</ul') === FALSE) {
    $vars['tabs'] = $vars['tabs'] . '</ul>';
  }
}

function drillingstandard_lt_access_denied() {
	$output = '<div style="border: 2px #132D25 solid;padding:1em;"><h3>Dear User</h3><p>Unfortunately your account does not allow you access to this document.</p><p>If you would like to receive more information ' .
				  "with regards to changing your account level so that you can use the full information included within the NS Information Hub, then please " .
				  '<a href="/node/1673" title="Contact us">let us know</a>.</p><p>&nbsp;</p><p><strong>We look forward to hearing from you.</strong></p></div>';

  return $output;
}
function clearText(field){
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}

$(document).ready(function() {
  $('#password-clear').show();
$('.password-password').hide();

$('#password-clear').focus(function() {
    $('#password-clear').hide();
    $('.password-password').show();
    $('.password-password').focus();
});
$('.password-password').blur(function() {
    if($('.password-password').val() == '') {
        $('#password-clear').show();
        $('.password-password').hide();
    }
});
});


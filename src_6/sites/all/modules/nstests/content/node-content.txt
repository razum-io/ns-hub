[[Title:Benchmark document|1|numbering-off|nomenu]]

[[Title:Table|2|numbering-off|nomenu]]

{| style="text-align:center;border-width:0;width:100%"
| style="background-color:LightGray; border-width: 0 0 2px 0; border-style:solid;" colspan=7 | Average speed when creating this table
|-
| &nbsp;
| colspan=2 style="border-width: 1px; border-style:solid;" | Skills of editor
| colspan=2 style="border-width: 1px; border-style:solid;" | Speed of procesor
| colspan=2 style="border-width: 1px 2px 1px 1px; border-style:solid;" | Size of computer’s screen
|-
| style="width:16%;" | &nbsp;
| style="width:14%; border-width: 1px 1px 2px 1px; border-style:solid;" | Beginner
| style="width:14%; border-width: 1px 1px 2px 1px; border-style:solid;" | Skilled
| style="width:14%; border-width: 1px 1px 2px 1px; border-style:solid;" | i3
| style="width:14%; border-width: 1px 1px 2px 1px; border-style:solid;" | i5
| style="width:14%; border-width: 1px 1px 2px 1px; border-style:solid;" | 17”
| style="width:14%; border-width: 1px 2px 2px 1px; border-style:solid;" | 21”
|-
| style="text-align:left;" | MediaWiki
| style="border-width: 1px; border-style: solid dotted solid solid;" | 5 min
| style="border-width: 0 1px 1px 0; border-style: solid;" | 2 min
| style="border-width: 1px; border-style: solid dotted solid solid;" | 0%
| style="border-width: 0 1px 1px 0; border-style: solid;" | 0%
| style="border-width: 1px; border-style: solid dotted solid solid;" | 0%
| style="border-width: 0 2px 1px 0; border-style: solid;" | 0%
|-
| style="text-align:left;" | MS Word 2010
| style="border-width: 1px; border-style: solid dotted solid solid;" | 15 min
| style="border-width: 0 1px 1px 0; border-style: solid;" | 5 min
| style="border-width: 1px; border-style: solid dotted solid solid;" | +30%
| style="border-width: 0 1px 1px 0; border-style: solid;" | 0%
| style="border-width: 1px; border-style: solid dotted solid solid;" | 0%
| style="border-width: 0 2px 1px 0; border-style: solid;" | -20%
|-
| style="text-align:left; border-width: 0 0 2px 0;border-style:solid;" | InDesign CS5
| style="border-width: 1px 1px 2px 1px; border-style: solid dotted solid solid;" | 30-60 min
| style="border-width: 0 1px 2px 0; border-style: solid;" | 20 min
| style="border-width: 1px 1px 2px 1px; border-style: solid dotted solid solid;" | +50%
| style="border-width: 0 1px 2px 0; border-style: solid;" | +10%
| style="border-width: 1px 1px 2px 1px; border-style: solid dotted solid solid;" | +20%
| style="border-width: 0 2px 2px 0; border-style: solid;" | 0%
|}

[[Title:List|2|numbering-off|nomenu]]

# '''one'''
# '''two'''
#* two point one
#* two point two
# '''three'''<br/>With more explanations<br/>Even 2

#** Which can have indented explanations
#** as well
#### Even numbered sub-explanations if desired
# '''four''' is just next item
# '''five'''
## five sub 1
### five sub 1 sub 1
## five sub 2

[[Title:Images|2|numbering-off|nomenu]]

[[Image:Smiley_Face_clip_art.eps.converted.png|Smiley_Face_clip_art.eps.converted.png|none|||]]

[[Image:Smiley_Face_clip_art_medium.png|Smiley_Face_clip_art_medium.png|none|||]]

[[Title:Chinese|2|numbering-off|nomenu]]

本标准涵盖了API标准规定的钻柱构件的检测要求。本标准中，钻柱是指钻头螺纹以上、方钻杆以下的所有构件组合。本标准中不含顶驱的要求，顶驱的检测须依据制造商的程序进行。
<?php

/**
 * Create form for testing page
 */

function nstests_testspage_form($form) {
    // Prepare variables
    $form = array();
    $result = db_result(db_query_range("SELECT lid FROM {linkchecker_links} WHERE ((status = 1) and (code = '-1')) ORDER BY lid ASC", 0, 1));
    $last = db_result(db_query_range("SELECT last_checked FROM {linkchecker_links} WHERE ((status = 1) and (code <> '-1')) ORDER BY last_checked DESC", 0, 1));
    $since_last = ($last) ? time() - $last : false;
    $ignore_response_codes = preg_split('/(\r\n?|\n)/', variable_get('linkchecker_ignore_response_codes', "200\n206\n302\n304\n401\n403"));
    $links_report_sql = "SELECT COUNT(*)
    FROM {linkchecker_links} ll
    INNER JOIN (
      SELECT lid FROM (
        SELECT DISTINCT lid FROM {linkchecker_boxes}
        UNION
        SELECT DISTINCT lid FROM {linkchecker_comments}
        UNION
        SELECT DISTINCT lid FROM {linkchecker_nodes}
      ) q1
    ) q2 ON q2.lid = ll.lid
    WHERE ll.last_checked <> 0 AND ll.status = 1 AND ll.code NOT IN (" . implode(',', $ignore_response_codes) . ")";
    $count_broken = db_result(db_query($links_report_sql));
    // Create form
    $broken_articles = variable_get("nstests_errors", array("error" => array(), "empty" => array(), 'message' => "Not fully tested."));
    $html = "";
    if (count($broken_articles['error'])) {
        $html .= theme_item_list($broken_articles['error']);
    }
    if (count($broken_articles['empty'])) {
        $html .= theme_item_list($broken_articles['empty']);
    }
    if ($html) {
        $html = '<div><strong>Broken links (up to first 10)</strong></div>' . $html;
    }

    if ($broken_articles['message'] != "Not fully tested.") {
        $html .= "Last tested on: " . $broken_articles['message'];
    }

    $html = ($html) ? $html : $broken_articles['message'];
    
    // Add info about last tested node
    $last_nid = variable_get('nstests_last_nid', 0);
    if ($last_nid) {
        $path = drupal_get_path_alias('node/' . $last_nid);
        $html .= '<div>Last tested node: ' . l($path, $path) . '</div>';
    }

    $form['empty-and-error-messages-markup'] = array(
        '#type' => 'markup',
        '#value' => '<div><h3>Check Wiki pages on NS HUB for error messages and empty content</h3></div><div>' . $html . '</div><br /><div>',
    );
    $form['empty-and-error-messages-ten'] = array(
        '#name' => "empty-ten",
        '#type' => 'submit',
        '#value' => t('Check next 10')
    );
    $form['empty-and-error-messages'] = array(
        '#name' => "empty",
        '#type' => 'submit',
        '#suffix' => '</div>',
        '#value' => t('Check all')
    );
    $date_count = ($since_last) ? floor($since_last/86400) : false;
    $last_check = ($date_count !== false) ? format_plural($date_count, 'yesterday.', '@count days ago') : "never performed.";
    $broken_links_sum = (is_numeric($count_broken)) ? ', <strong>Number of broken links: </strong> ' . $count_broken . ", ": ", ";
    $form['missing-links-and-images-markup'] = array(
        '#type' => 'markup',
        '#value' => '<hr /><div><h3>Check for broken links to pages and images</h3></div><div><strong>Last check: </strong>' . $last_check . $broken_links_sum .
                    '<strong>More: </strong><a href="/admin/reports/linkchecker">Linkchecker</a></div><br /><div>',
    );
    $result = db_result(db_query_range("SELECT lid FROM {linkchecker_links} WHERE ((status = 1) and (code = '-1')) ORDER BY lid ASC", 0, 1));
    $last = db_result(db_query_range("SELECT last_checked FROM {linkchecker_links} WHERE ((status = 1) and (code <> '-1')) ORDER BY last_checked DESC", 0, 1));
    if ($result && $last && ($since_last < 86400)) {
        $text =  "Continue batch";
        $continue = true;
        $form['missing-links-and-images-startover'] = array(
            '#name' => "links-over",
            '#type' => 'submit',
            '#value' => t('Start over')
        );
    } else {
        $text =  'Check';
        $continue = false;
    }
    $form['missing-links-and-images'] = array(
        '#name' => "links",
        '#type' => 'submit',
        '#suffix' => '</div>',
        '#value' => t($text)
    );
    $form['hidden'] = array(
        '#type' => 'value',
        '#value' => $continue
    );
    $result_bench = variable_get("nstests_testtid", array());
    if (isset($result_bench['error'])) {
        $html = '<strong>Failed:</strong> ';
        switch ($result_bench['error']) {
            case "Permissions":
                $html .= "creation of folders. Possibly permissions' misconfiguration.";
            break;
            case "Missing PDF":
                $html .= 'PDF creation. Possibly LaTeX misconfiguration. Check the log: <a href="' . $result_bench['link'] .
                         '">This is actual version</a>.';
            break;
            case "HTML incorrect":
                $html .= 'HTML is incorrect. <a href="/nstests/outpage">This is the correct version</a> and <a href="/node/' . $result_bench['nid'] .
                         '">This is actual version</a>.';
            break;
            case "PDF incorrect":
                $html .= 'PDF is incorrect. <a href="/sites/all/modules/nstests/results/First-article-Default-a4.pdf">This is the correct version</a> and <a href="/' . $result_bench['link'] .
                         '">This is actual version</a>.';
            break;
        }
    } else {
        $html = 'Last test was successful.';
    }
    $form['functionality-test-markup'] = array(
        '#type' => 'markup',
        '#value' => '<hr /><div><h3>Check for Edit functionality</h3> (creates NS document, article for this document, user for this document,<br /> and prints it).</div>' .
                    $html . '<div>',
    );
    $form['functionality-test'] = array(
        '#name' => "functionality",
        '#type' => 'submit',
        '#suffix' => '</div>',
        '#value' => t('Check')
    );
    return $form;
}

/**
 * Submitted form
 * @param object $form
 * @param object $form_state
 */

function nstests_testspage_form_submit($form, &$form_state) {
    switch ($form_state['clicked_button']['#name']) {
        case "empty":
            nstests_testspages();
        break;
        case "empty-ten":
            nstests_testspages(true);
        break;
        case "links-over":
            nstests_check_missing_links();
        break;
        case "links":
            nstests_check_missing_links($form_state['values']['hidden']);
        break;
        case "functionality":
            nstests_func_tests();
        break;
    }
}

/**
 * === FUNCTION "links" ===
 * Refreshes a list of hyperlinks inside Wiki nodes,
 * checks for broken or redirected links.
 * It is using the functionality of the Linkchecker module.
 */

function nstests_check_missing_links($continue = false) {
    $operations = array();
    if(!$continue) {
        // Truncate old links
        db_query("TRUNCATE TABLE {linkchecker_nodes}");
        db_query("TRUNCATE TABLE {linkchecker_comments}");
        db_query("TRUNCATE TABLE {linkchecker_boxes}");
        db_query("TRUNCATE TABLE {linkchecker_links}");

        // Get a list of nodes
        $result = db_query('SELECT n.nid FROM {node} n WHERE n.status = %d AND n.type IN (' . db_placeholders(array("wiki"), 'varchar') .
                           ') ORDER BY n.nid', array_merge(array(1), array("wiki")));

        // Create a list of operations on nodes
        while ($row = db_fetch_array($result)) {
            $operations[] = array('_nstests_batch_checknodes_op', array($row['nid']));
        }
    }
    
    // Add linkchecker
    $operations[] = array('_nstests_batch_checklinks_op', array());
    $batch = array(
        'file' => drupal_get_path('module', 'nstests') .'/includes/nstests.pages.inc',
        'finished' => '_nstests_batch_checklinks_finished',
        'operations' => $operations,
        'title' => t('Checking links')
    );
    batch_set($batch);
}

/**
 * Batch: scan nodes
 */

function _nstests_batch_checknodes_op($nid, &$context) {
    $node = node_load($nid, NULL, TRUE);
    _linkchecker_add_node_links($node);

    // Store results for post-processing in the finished callback.
    $context['results'][] = $node->nid;
    $context['message'] = t('Node: @title', array('@title' => $node->title));
}

/**
 * Batch: scan links
 */

function _nstests_batch_checklinks_op(&$context) {
    if (empty($context['sandbox'])) {
        $links = db_query("SELECT * FROM {linkchecker_links} WHERE ((status = 1) and (code = '-1')) ORDER BY lid ASC");
        $context['sandbox']['links'] = array();
        // Iterate through the list, create operations
        while ($link = db_fetch_object($links)) {
            $context['sandbox']['links'][] = $link;
        }
        $context['sandbox']['max'] = count($context['sandbox']['links']);
        //$context['sandbox']['progress'] = 0;
        $context['sandbox']['progress'] = 0;
        if ($context['sandbox']['max'] == 0) {
            $context['finished'] = 1;
            return;
        }
    }
    // Perform operation on link
    _nstests_batch_checklinks($context['sandbox']['links'][$context['sandbox']['progress']], &$context);
    // Show progress
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
    // Deal with another link
    $context['sandbox']['progress']++;
  }

/**
 * Batch operation: check links' targets
 */

function _nstests_batch_checklinks($link, &$context) {
    $headers = array();
    $headers['User-Agent'] = variable_get('linkchecker_check_useragent', 'Drupal (+http://drupal.org/)');
    $uri = @parse_url($link->url);

    // URL contains a fragment.
    if (in_array($link->method, array('HEAD', 'GET')) && !empty($uri['fragment'])) {
        // We need the full content and not only the HEAD.
        $link->method = 'GET';
        // Request text content only (like Firefox/Chrome).
        $headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
    } elseif ($link->method == 'GET') {
        // Range: Only request the first 1024 bytes from remote server. This is
        // required to prevent timeouts on URLs that are large downloads.
        $headers['Range'] = 'bytes=0-1024';
    }

    // Drupal core link request
    $response = drupal_http_request($link->url, $headers, $link->method, NULL, 0);

    // Add 'redirect_code' property to core response object for consistency
    // with HTTPRL object.
    if ($response->code == 301 && !isset($response->redirect_code)) {
        $response->redirect_code = $response->code;
    }
    // Add 'uri' property to core response object for 'fragment' check and
    // consistency with HTTPRL object.
    $response->uri = $uri;

    _linkchecker_status_handling($response, $link);

    // Results for evaluating finished batch
    $context['results'][] = $link->id;
    $context['message'] = t('Link: @uri', array('@uri' => $link->url));
}

/**
 * Batch operation finished: show results
 */

function _nstests_batch_checklinks_finished($success, $results, $operations) {
    if ($success) {
        $message = format_plural(count($results), 'One link has been tested.', '@count links have been tested.');
    } else {
        $message = t('Testing links in Wiki have failed with an error.');
    }
    drupal_set_message($message);
}

/**
 * === FUNCTION "empty" ===
 * Walks through all Wiki nodes and shows these which are empty or with an error
 * message.
 */

function nstests_testspages($limited = false) {
    
    // Create list of nodes
    $result = db_query('SELECT n.nid FROM {node} n WHERE n.status = %d AND n.type IN (' . db_placeholders(array("wiki"), 'varchar') .
                       ') ORDER BY n.nid', array_merge(array(1), array("wiki")));

    // Initialize operations
    $operations = array(array('_nstests_testspages_opinit', array()));

    // Create array of nids
    $nids = array();
    while ($row = db_fetch_array($result)) {
        $nids[] = $row['nid'];
    }
    $no_of_nids = count($nids);
    
    // Return if there aren't NIDs at all
    if (!$no_of_nids) {
        drupal_set_message("No nodes to check.");
        return;
    }
    
    // Get last processed nid
    $recent_nid = variable_get('nstests_last_nid', 0);
    
    // Limit number of nids if requested and possible
    if ($limited && ($no_of_nids > 10)) {
        
        $output = array();
        
        // Find position of next NID
        $next_position = array_search ($recent_nid , $nids) + 1;
        
        // Set the limit
        $nids_limit = 10;
        
        // Do slice
        $output = array_slice($nids, $next_position, $nids_limit);
        
        // Add nodes from beginning if needed
        $actual_size = count($output);
        if ($actual_size < $nids_limit) {
            $output = array_merge($output, array_slice($nids, 0, $nids_limit - $actual_size));
        }
        
        // Return output
        $nids = $output;
    }
    
    // Create operations
    foreach ($nids as $nid) {
        $operations[] = array('_nstests_testspages_op', array($nid));
    }    

    // $operations = array(array('_nstests_testspages_opinit', array()), array('_nstests_testspages_op', array(1)), array('_nstests_testspages_op', array(2778)), array('_nstests_testspages_op', array(3024))); //Deleteafter

    // Set-up batch
    $batch = array(
        'file' => drupal_get_path('module', 'nstests') .'/includes/nstests.pages.inc',
        'finished' => '_nstests_testspages_finished',
        'operations' => $operations,
        'title' => t('Checking Wiki')
    );

    // Reset cache of results
    variable_set("nstests_errors", array("error" => array(), "empty" => array(), 'message' => "Not fully tested."));

    batch_set($batch);
}

function _nstests_testspages_opinit(&$context) {
    // Login to NS HUB
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url(NULL, array('absolute' => TRUE)) . "user/login");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP script');
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/cookie.txt");
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookie.txt');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'name=testeditor&pass=XWRCqFexLJmcE9lN9Zrv|&form_id=user_login');
    curl_exec ($ch);
    $context['results']['ch'] = $ch;
}

function _nstests_testspages_op($nid, &$context) {
    // Read node
    $ch = $context['results']['ch'];
    if (!$ch) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookie.txt');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP script');
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $context['results']['ch'] = $ch;
    }
    $url = url(NULL, array('absolute' => TRUE)) . '?q=node/' . $nid;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($ch, CURLOPT_HTTPGET, 1);
    $content = curl_exec($ch);

    // Add result for further processing
    $context['results']['out'][] = $nid;
    $context['message'] = t('Url: @title', array('@title' => $url));
    // Check is page is empty or faulty
    $errors = variable_get("nstests_errors", array("error" => array(), "empty" => array()));
    if (strpos($content, "messages error") !== false) {
        $context['results']['error'][] = $nid;
        $errors['error'][] = "Page with error: " . l($url, $url, array('query' => drupal_get_destination()));
    }
    if (preg_match('/<div id="content-area.*<div class="content">\s*<\/div>/is', $content)) {
        $context['results']['error'][] = $nid;
        $errors['empty'][] = "Empty page: " . l($url, $url, array('query' => drupal_get_destination()));
    }
    if ((count($errors['error']) + count($errors['empty'])) <= 10) {
        variable_set("nstests_errors", $errors);
    }
    // Write the last NID
    variable_set('nstests_last_nid', $nid);
}

function _nstests_testspages_finished($success, $results, $operations) {
    if ($success) {
        $message = format_plural(count($results['out']), 'One article has been tested.', '@count articles have been tested.');
        $message .= '<br>' . format_plural(count($results['error']), 'One article is faulty.', '@count articles are faulty.');
    } else {
        $message = t('Testing links in Wiki have failed with an error.');
    }
    drupal_set_message($message);
    $changedate = variable_get("nstests_errors", array("error" => array(), "empty" => array(), 'message' => "Not fully tested."));
    $changedate['message'] = date('d/m/Y', time());
    variable_set("nstests_errors", $changedate);
}

/**
 * Funcrionality test
 */

function nstests_func_tests() {
    $q = $_GET['q'];
    // Delete old case if it is set
    $old_case = variable_get("nstests_testtid", array());
    if (isset($old_case['tid'])) {
        _nstests_deletecase($old_case['nid'], $old_case['tid']);
    }
    // Create new document
    $new_document_name = substr(md5(uniqid(rand(), true)), 16);
    // -- Check if document exists
    $names = _nstests_nsdoc_names();
    while (in_array($new_document_name, $names)) {
        $new_document_name = substr(md5(uniqid(rand(), true)), 16);
    }
    // -- Create it
    $form_state = array(
       'values' => array(
           'op' => t('Create'),
           'document' => $new_document_name
       ),
    );
    drupal_execute("admin_ns_user_createform", $form_state);
    // Create content
    $term = taxonomy_get_term_by_name($new_document_name);
    $tid = $term[0]->tid;
    $content = file_get_contents(drupal_get_path('module', 'nstests') .'/content/node-content.txt');
    // -- Create node
    $node = new stdClass();
    $node->title = "First article";
    $node->body = $content;
    $node->type= "wiki";
    $node->created = time();
    $node->changed = $node->created;
    $node->status = 1;
    $node->promote = 0;
    $node->sticky = 0;
    $node->format = 1;
    $node->uid = 1;
    $node->taxonomy = array(
        $tid => $term[0]
    );
    $node->format = 4;
    $node = node_submit($node);
    node_save($node);
    // -- Create failsafe variable
    $failsafe = array('tid' => $tid, 'nid' => $node->nid);
    variable_set("nstests_testtid", $failsafe);
    // -- Copy image files
    $files = array(
        array(
            "source" => realpath(drupal_get_path('module', 'nstests') .'/content/images/Smiley_Face_clip_art.eps.converted.png'),
            "target" => realpath('sites/default/files/Document-' . $tid) . '/Smiley_Face_clip_art.eps.converted.png'
        ),
        array(
            "source" => realpath(drupal_get_path('module', 'nstests') .'/content/images/Smiley_Face_clip_art_medium.png'),
            "target" => realpath('sites/default/files/Document-' . $tid) . '/Smiley_Face_clip_art_medium.png'
        ),
        array(
            "source" => realpath(drupal_get_path('module', 'nstests') .'/content/images/originals/Smiley_Face_clip_art.eps'),
            "target" => realpath('sites/default/files/Document-' . $tid) . '/originals/Smiley_Face_clip_art.eps'
        )
    );
    foreach ($files as $file) {
        if (!copy($file['source'], $file['target'])) {
            drupal_set_message("failed to copy" . $file['source'] . "...\n"); // Folders' permissions are somehow wrong
            $failsafe['error'] = "Permissions";
            variable_set("nstests_testtid", $failsafe);
            return;
        }
    }
    // -- Print node
    module_load_include("inc", "printing", "parser/printing.helpers");
    printing_create_pdf(1, $node->nid);
    // -- Read node

    // Perform checks
    $contains = array(
        '<table style="text-align:center;border-width:0;width:100%">',
        '<ol>',
        '<div class="wiki-fig" style="float:none;">'
    );
    $pdffile = realpath('sites/default/files/private/Document-' . $tid . '/Node-' . $node->nid) . '/Paper-1/First-article-Default-a4.pdf';
    if (!file_exists($pdffile)) {
        drupal_set_message("Printing failed", "error"); // We have no existing PDF, quit
        $failsafe['error'] = "Missing PDF";
        $failsafe['link'] = 'sites/default/files/private/Document-' . $tid . '/Node-' . $node->nid . '/Paper-1/First-article-Default-a4.log';
        variable_set("nstests_testtid", $failsafe);
        return;
    } else {
        $pdfsize = filesize($pdffile);
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, url(NULL, array('absolute' => TRUE)) . "user/login");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, 'PHP script');
    curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/cookie.txt");
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookie.txt');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, 'name=testeditor&pass=XWRCqFexLJmcE9lN9Zrv|&form_id=user_login');
    curl_exec ($ch);
    $url = url(NULL, array('absolute' => TRUE)) . '/' . drupal_get_path_alias('node/' . $node->nid);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    curl_setopt($ch, CURLOPT_HTTPGET, 1);
    $pagecontent = curl_exec($ch);
    // -- Check HTML for content
    foreach ($contains as $contain) {
        if (strpos($pagecontent, $contain) === false) {
            drupal_set_message("WIKI/HTML filter failed", "error"); // We have no correct HTML, quit
            $failsafe['error'] = "HTML incorrect";
            variable_set("nstests_testtid", $failsafe);
            return;
        }
    }
    // -- Check PDF sizes (with 1kB tolerance)
    if (($pdfsize > 131000) || ($pdfsize < 130000)) {
        drupal_set_message("PDF is not correct", "error"); // We have no correct PDF, quit
        $failsafe['error'] = "PDF incorrect";
        $failsafe['link'] = 'sites/default/files/private/Document-' . $tid . '/Node-' . $node->nid . '/Paper-1/First-article-Default-a4.pdf';
        variable_set("nstests_testtid", $failsafe);
        return;
    }
    // All went OK, delete changes
    _nstests_deletecase($node->nid, $tid);
    $_GET['q'] = $q;
    drupal_flush_all_caches();
    global $base_root;
    drupal_goto($base_root . request_uri());
}

/**
 * Get names of NS Documents
 */

function _nstests_nsdoc_names() {
    $terms = taxonomy_get_tree(1);
    $options = array();
    foreach ($terms as $term) {
        $options[$term->tid] = $term->name;
    }
    return $options;
}

/**
 * Reset testing case
 */

function _nstests_deletecase($nid, $tid) {
    // -- Delete node
    node_delete($nid);
    // -- Delete roles
    admin_ns_user_deleterole($tid);
    // -- Delete document
    $term = array(
       'tid' => $tid,
    );
    taxonomy_save_term($term);
    node_access_rebuild();
    // -- Delete temporary variable
    variable_del('nstests_testtid');
}

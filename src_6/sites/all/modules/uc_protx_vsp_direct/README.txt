Drupal Project Page: http://drupal.org/project/uc_protx_vsp_direct

This module implements as far as possible v2.22 of the Protx VSP Direct Protocol
http://www.protx.com/downloads/docs/VSPDirectProtocolandIntegrationGuideline.pdf

You must upgrade to (at least) v1.2 of Ubercart to use all the features of this module; in particular: accepting cards that have start date and issue number (e.g., UK Maestro, Solo), and longer card numbers (e.g., International Maestro).

Suggestions:
You may want to prevent the shopping cart block from displaying on the 'uc_protx_vsp_direct/3DSecure' page.

For HTTPS support, I suggest using Secure Pages on the following pages (at minimum):
admin
admin/*
cart/checkout
cart/checkout/*

Please feel free to contact me (solarian) at http://drupal.org/user/166738 with any problems or suggestions for improvements.

N.B. As of v1.2, Ubercart's credit card validation algorithm has problems.  For better operation, it is reccommended that you disable credit card validation in Store Administration->Configuration->Payment settings->Payment Methods->Credit Card.
<?php
// $Id: uc_protx_vsp_direct.ca.inc,v 1.1.2.5 2009/09/03 13:28:45 hanoii Exp $

define('UC_PROTX_VSP_DIRECT_CATEGORY', t('SagePay Direct'));

function uc_protx_vsp_direct_action_3dsecure_form($form_state, $settings) {
  $options = array();
  $options[0] = t('Default');
  $options[1] = t('Force 3D-Secure checks');
  $options[2] = t('Do not perform 3D-Secure checks');
  $options[3] = t('Force 3D-Secure checks but ALWAYS obtain an auth code');
  $form['uc_protx_vsp_direct_action_3dsecure_flag'] = array(
    '#type' => 'select',
    '#title' => t('3DSecure Check'),
    '#options' => $options,
    '#default_value' => $settings['uc_protx_vsp_direct_action_3dsecure_flag'],
    '#description' => t('
      <p>
        Using this flag you can fine tune the 3D Secure checks and rule set
        you\'ve defined, at a transaction level. This is useful in circumstances
        where direct and trusted customer contact has been established and you
        wish to override the default security checks. NB: If 3D Secure is ON for
        your account this field becomes compulsory.
        <b>This field is ignored for PAYPAL transactions</b>
      </p>
      <p>
        <ul>
          <li>
            <b>%0</b> = If 3D-Secure checks are possible and rules allow,
            perform the checks and apply the authorisation rules (default).
          </li>
          <li>
            <b>%1</b> = Force 3D-Secure checks for this transaction only (if
            your account is 3D-enabled) and apply rules for authorisation.
          </li>
          <li>
            <b>%2</b> = Do not perform 3D-Secure checks for this transaction
            only and always authorise.
          </li>
          <li>
            <b>%3</b> = Force 3D-Secure checks for this transaction (if your
            account is 3D-enabled) but ALWAYS obtain an auth code, irrespective
            of rule base.
          </li>
        </ul>
      </p>
      <p>
      More info at !sagepaylink
      </p>',
      array(
        '%0' => $options[0],
        '%1' => $options[1],
        '%2' => $options[2],
        '%3' => $options[3],
        '!sagepaylink' => l(t('SagePay\'s protocol description'), 'http://www.sagepay.com/developers/integration_manual/direct_protocol.html'),
      )
    ),
  );

  return $form;
}

function uc_protx_vsp_direct_action_3dsecure($txdata, $settings) {
  $txdata['Apply3DSecure'] = $settings['uc_protx_vsp_direct_action_3dsecure_flag'];
  uc_protx_vsp_direct_txdata_save($txdata);
}

function uc_protx_vsp_direct_condition_card_form($form_state, $settings) {
  // TODO: Add validation for method ID
  /*
  foreach (_payment_method_list() as $method) {
    $options[$method['id']] = $method['title'];
  }
  */
  // Code copied from uc_credit.module:939
  $form =  array();
  if (variable_get('uc_credit_type_enabled', FALSE)) {
    $types = variable_get('uc_credit_accepted_types', implode("\r\n", array(t('Visa'), t('Mastercard'), t('Discover'), t('American Express'))));
    if (empty($types)) {
      $types = array(t('N/A'));
    }
    else {
      $types = explode("\r\n", $types);
    }
    foreach ($types as $type) {
      $options[check_plain($type)] = $type;
    }
    $form['uc_protx_vsp_direct_condition_card_type'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Card type'),
      '#options' => $options,
      '#default_value' => $settings['uc_protx_vsp_direct_condition_card_type'] ? $settings['uc_protx_vsp_direct_condition_card_type'] : array(),
    );
  }

  return $form;
}

function uc_protx_vsp_direct_condition_card($order, $settings) {
  $condition = in_array($order->payment_details['cc_type'], $settings['uc_protx_vsp_direct_condition_card_type'], TRUE);
  return $condition;
}

/******************************************************************************
 * Conditional Actions Hooks                                                  *
 ******************************************************************************/

/**
 * Implementation of hook_ca_trigger().
 */
function uc_protx_vsp_direct_ca_trigger() {
  $triggers['uc_protx_vsp_direct_trigger_txsend'] = array(
    '#title' => t('SagePay Direct: Transaction information is about to be sent'),
    '#category' => UC_PROTX_VSP_DIRECT_CATEGORY,
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order')
      ),
      'uc_protx_vsp_direct_txdata' => array(
        '#entity' => 'uc_protx_vsp_direct_txdata',
        '#title' => t('SagePay transaction data')
      ),
    ),
  );

  return $triggers;
}

/**
 * Implementation of hook_ca_entity().
 */
function uc_protx_vsp_direct_ca_entity() {
  $entities['uc_protx_vsp_direct_txdata'] = array(
    '#title' => t('SagePay transaction data'),
    '#type' => 'array',
  );

  return $entities;
}

/**
 * Implementation of hook_ca_condition().
 */
function uc_protx_vsp_direct_ca_condition() {

  $conditions['uc_protx_vsp_direct_condition_card'] = array(
    '#title' => t('Check the credit card type'),
    '#callback' => 'uc_protx_vsp_direct_condition_card',
    '#arguments' => array(
      'order' => array(
        '#entity' => 'uc_order',
        '#title' => t('Order')
      )
    ),
    '#category' => UC_PROTX_VSP_DIRECT_CATEGORY,
  );

  return $conditions;
}

/**
 * Implementation of hook_ca_action().
 */
function uc_protx_vsp_direct_ca_action() {

  $actions['uc_protx_vsp_direct_action_3dsecure'] = array(
    '#title' => t('Set 3D-Secure Checks Flag'),
    '#callback' => 'uc_protx_vsp_direct_action_3dsecure',
    '#arguments' => array(
      'uc_protx_vsp_direct_txdata' => array(
        '#entity' => 'uc_protx_vsp_direct_txdata',
        '#title' => t('SagePay transaction data')
      )
    ),
    '#category' => UC_PROTX_VSP_DIRECT_CATEGORY,
  );

  return $actions;
}

/**
 * Implementation of hook_ca_predicate().
 */
function uc_protx_vsp_direct_ca_predicate() {
  $predicates = array();

  $predicates['uc_protx_vsp_direct_set_3dsecure'] = array(
    '#title' => t('Set 3D-Secure flag'),
    '#description' => t('Control the Apply3DSecure item of the protocol.'),
    '#class' => 'payment',
    '#status' => 0,
    '#trigger' => 'uc_protx_vsp_direct_trigger_txsend',
    '#actions' => array(
      array(
        '#name' => 'uc_protx_vsp_direct_action_3dsecure',
        '#title' => t('Set 3D-Secure Checks Flag'),
        '#argument_map' => array(
          'uc_protx_vsp_direct_txdata' => 'uc_protx_vsp_direct_txdata',
        ),
        '#settings' => array(
          'uc_protx_vsp_direct_action_3dsecure_flag' => 0,
        ),
      ),
    ),
  );

  return $predicates;
}
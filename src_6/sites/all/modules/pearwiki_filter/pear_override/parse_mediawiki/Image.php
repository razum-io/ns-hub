<?php

class Text_Wiki_Parse_Image extends Text_Wiki_Parse {

    var $regex = '/(\[\[image:)(.+?)(\]\])/i';

    function process(&$matches)
    {
        $pos = strpos($matches[2], '|');
        if ($pos === false) {
            $options = array(
                'src' => $matches[2],
                'attr' => array());
        } else {
            list($image, $attrs) = explode('|', $matches[2], 2);
            $options = array('src' => $image, 'attr' => array());
            $parts = explode('|', $attrs);
            foreach($parts as $index => $part) {
			    if(strpos($part, 'print_') === 0){
					$pos = strpos($part, ':');
					$pre = substr($part, 0, $pos);
					$this->parse_attrs($options, 'attr_'.$pre, $index, substr($part, $pos+1));
				}else{
					$this->parse_attrs($options, 'attr', $index, $part);
				}
            }
        }
        return $this->wiki->addToken($this->rule, $options);
    }
	
	function parse_attrs(&$options, $type_pre, $index, $part){
		  if (in_array($part, array('thumbnail', 'thumb', 'frame'))) {
			if ($part == 'thumb' || $part == 'thumbnail') {
			  $options[$type_pre]['align'] = 'right';
			  $options[$type_pre]['width'] = '180';
			}
			else {
			
			}
			// add frame
		  }
		  elseif (in_array($part, array('right', 'left', 'center', 'none'))) {
			if ($part != 'none') {
			  $options[$type_pre]['align'] = $part;
			}
		  }
		  elseif ($pos = strpos($part, 'px')) {
			$part = str_replace('px', '', $part);
			if (strpos($part, 'x')) {
			  list($width, $height) = explode('x', substr($part, 0, $pos));
			  $options[$type_pre]['width'] = $width;
			  $options[$type_pre]['height'] = $height;
			}
			else if(strpos($part, 'x') === 0){
			  $options[$type_pre]['height'] = substr($part, 1, $pos);
			}
			else {
			  $options[$type_pre]['width'] = substr($part, 0, $pos);
			}
		  }
		  else {
			if($index == 2){
				$options[$type_pre]['caption'] = $part;
			}else if($index == 3){
				$options[$type_pre]['border'] = $part ? $part : 0;
			}else if($index == 4){
				$options[$type_pre]['margin'] = $part ? $part : 0;
			}else if($index == 5){
				$options[$type_pre]['inline'] = ($part == 'inline');
			}
		  }
	}
}

<?php
// vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4:
/**
 * Mediawiki: Parses for links to (inter)wiki pages or images.
 *
 * Text_Wiki rule parser to find links, it groups the 3 rules:
 * # Wikilink: links to internal Wiki pages
 * # Interwiki: links to external Wiki pages (sister projects, interlangage)
 * # Image: Images
 * as defined by text surrounded by double brackets [[]]
 * Translated are the link itself, the section (anchor) and alternate text
 *
 * PHP versions 4 and 5
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Bertrand Gugger <bertrand@toggg.com>
 * @author     Paul M. Jones <pmjones@php.net>
 * @copyright  2005 bertrand Gugger
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    CVS: $Id: Wikilink.php 278841 2009-04-16 15:10:51Z ritzmo $
 * @link       http://pear.php.net/package/Text_Wiki
 */

/**
 * Wikilink, Interwiki and Image rules parser class for Mediawiki.
 * This class implements a Text_Wiki_Parse to find links marked
 * in source by text surrounded by 2 opening/closing brackets as 
 * [[Wiki page name#Section|Alternate text]]
 * On parsing, the link is replaced with a token.
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Bertrand Gugger <bertrand@toggg.com>
 * @copyright  2005 bertrand Gugger
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/Text_Wiki
 * @see        Text_Wiki_Parse::Text_Wiki_Parse()
 */
class Text_Wiki_Parse_Backgroundimage extends Text_Wiki_Parse {


    /**
     * The regular expression used to parse the source text and find
     * matches conforming to this rule.  Used by the parse() method.
     *
     * @access public
     * @var string
     * @see Text_Wiki_Parse::parse()
     */
    //var $regex = '/(?<!\[)\[\[(?!\[)\s*(:?)((?:[^:]+:)+)?([^:]+)(?:#(.*))?\s*(?:\|(((?R))|.*))?]]/msU';
    var $regex = '/\[\[BackgroundImage\:(.*?)\|(.*?)\|(.*?)\]\]/s';

	
     /**
     * Constructor.
     * We override the constructor to get Image and Interwiki config
     *
     * @param object &$obj the base conversion handler
     * @return The parser object
     * @access public
     */

	function Text_Wiki_Parse_Backgroundimage(&$obj)
    {
        $default = $this->conf;
        parent::Text_Wiki_Parse($obj);
	}
	
    /**
     * Generates a replacement for the matched text.  Token options are:
     * - 'page' => the name of the target wiki page
     * -'anchor' => the optional section in it
     * - 'text' => the optional alternate link text
     *
     * @access public
     * @param array &$matches The array of matches from parse().
     * @return string token to be used as replacement 
     */
    function process(&$matches)
    {
        
        $start = $this->wiki->addToken(
            $this->rule, 
            array(
                'type' => 'start',
                'image' => trim($matches[1]),
                'align' => trim($matches[2]),
                'valign' => trim($matches[3]),
            )
        );
        
        $end = $this->wiki->addToken(
            $this->rule, 
            array(
                'type' => 'end',
            )
        );
		
        return $start.$end;
    }
}
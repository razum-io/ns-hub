<?php
// vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4:
/**
 * Mediawiki: Parses for links to (inter)wiki pages or images.
 *
 * Text_Wiki rule parser to find links, it groups the 3 rules:
 * # Wikilink: links to internal Wiki pages
 * # Interwiki: links to external Wiki pages (sister projects, interlangage)
 * # Image: Images
 * as defined by text surrounded by double brackets [[]]
 * Translated are the link itself, the section (anchor) and alternate text
 *
 * PHP versions 4 and 5
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Bertrand Gugger <bertrand@toggg.com>
 * @author     Paul M. Jones <pmjones@php.net>
 * @copyright  2005 bertrand Gugger
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    CVS: $Id: Wikilink.php 278841 2009-04-16 15:10:51Z ritzmo $
 * @link       http://pear.php.net/package/Text_Wiki
 */

/**
 * Wikilink, Interwiki and Image rules parser class for Mediawiki.
 * This class implements a Text_Wiki_Parse to find links marked
 * in source by text surrounded by 2 opening/closing brackets as 
 * [[Wiki page name#Section|Alternate text]]
 * On parsing, the link is replaced with a token.
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Bertrand Gugger <bertrand@toggg.com>
 * @copyright  2005 bertrand Gugger
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/Text_Wiki
 * @see        Text_Wiki_Parse::Text_Wiki_Parse()
 */
class Text_Wiki_Parse_Title extends Text_Wiki_Parse {


    /**
     * The regular expression used to parse the source text and find
     * matches conforming to this rule.  Used by the parse() method.
     *
     * @access public
     * @var string
     * @see Text_Wiki_Parse::parse()
     */
    //var $regex = '/(?<!\[)\[\[(?!\[)\s*(:?)((?:[^:]+:)+)?([^:]+)(?:#(.*))?\s*(?:\|(((?R))|.*))?]]/msU';
    var $regex = '/\[\[Title\:(.*?)\]\]/s';

    var $conf = array(
        'id_prefix' => 'title-'
    );
	
     /**
     * Constructor.
     * We override the constructor to get Image and Interwiki config
     *
     * @param object &$obj the base conversion handler
     * @return The parser object
     * @access public
     */
	 
	var $levels;
	var $maxlevel = 6;

	function Text_Wiki_Parse_Title(&$obj)
    {
        $default = $this->conf;
        parent::Text_Wiki_Parse($obj);
		
		$this->levels = array();
		for($i = 1; $i <= $this->maxlevel; $i++){
			$this->levels[$i] = 0;
		}
	}
	
    /**
     * Generates a replacement for the matched text.  Token options are:
     * - 'page' => the name of the target wiki page
     * -'anchor' => the optional section in it
     * - 'text' => the optional alternate link text
     *
     * @access public
     * @param array &$matches The array of matches from parse().
     * @return string token to be used as replacement 
     */
    function process(&$matches)
    {
	//var_dump($matches);die;
	
        static $id;
        if (! isset($id)) {
            $id = 0;
        }
		$id++;
        
		$value = _drillingstandardmod_parse_title($matches[1]);/*explode('|', $matches[1]);
		if(!(isset($value[1]) && $value[1])){
			$value[1]  = 6;
		}
		$prop = array();
		if(isset($value[2]) && $value[2]){
			$prop[] = $value[2];
		}
		if(isset($value[3]) && $value[3]){
			$prop[] = $value[3];
		}
		
		$value[2] = 'numbering-on';
		$value[3] = 'menu';
		
		foreach($prop as $v){
			if($v == 'nomenu' || $v == 'no-menu' || $v == 'menu'){
				$value[3]  = $v;
			}else if($v == 'numbering-off' && $v == 'numbering-on'){
				$value[2]  = $v;
			}
		}*/
		
		
		$level = trim($value[1]);
		for($i = $level+1; $i <= $this->maxlevel; $i++){
			$this->levels[$i] = 0;
		}
		if(trim($value[2]) == 'numbering-on'){
			$this->levels[$level]++;
		}
		$levelarray = array();
		for($i = 1; $i <= $this->maxlevel; $i++){
			if(isset($this->levels[$i]) && $this->levels[$i]){
				$levelarray[] = $this->levels[$i];
			}else{
				break;
			}
		}
		
        $prefix = htmlspecialchars($this->getConf('id_prefix'));
        
        $start = $this->wiki->addToken(
            $this->rule, 
            array(
                'type' => 'start',
                'level' => $level,
                'text' => trim($value[0]),
				'number' => (trim($value[2]) == 'numbering-on'),
				'menu' => (trim($value[3]) == 'menu'),
                'id' => $prefix . $id,
				'levelindex' => $levelarray,
				'levelindexattr' => implode(',', $this->levels),
            )
        );
        
        $end = $this->wiki->addToken(
            $this->rule, 
            array(
                'type' => 'end',
                'level' => $level,
            )
        );
        
		$tag = 'h'.$value[1];
		
        return $start .$value[0] . $end;
	
	/*
        // Starting colon ?
        $colon = !empty($matches[1]);
        $auto = $interlang = $interwiki = $image = $site = '';
        // Prefix ?
        if (!empty($matches[2])) {
            $prefix = explode(':', substr($matches[2], 0, -1));
            $count = count($prefix);
            $i = -1;
            // Autolink
            if (isset($this->conf['project']) &&
                    in_array(trim($prefix[0]), $this->conf['project'])) {
                $auto = trim($prefix[0]);
                unset($prefix[0]);
                $i = 0;
            }
            while (++$i < $count) {
                $prefix[$i] = trim($prefix[$i]);
                // interlangage
                if (!$interlang &&
                    in_array($prefix[$i], $this->interwikiConf['interlangage'])) {
                    $interlang = $prefix[$i];
                    unset($prefix[$i]);
                    continue;
                }
                // image
                if (!$image && in_array($prefix[$i], $this->imageConf['prefix'])) {
                    $image = $prefix[$i];
                    unset($prefix[$i]);
                    break;
                }
                // interwiki
                if (isset($this->interwikiConf['sites'][$prefix[$i]])) {
                    $interwiki = $this->interwikiConf['sites'][$prefix[$i]];
                    $site = $prefix[$i];
                    unset($prefix[$i]);
                }
                break;
            }
            if ($prefix) {
                $matches[3] = implode(':', $prefix) . ':' . $matches[3];
            }
        }
        $text = empty($matches[5]) ? $matches[3] : $matches[5];
        $matches[3] = trim($matches[3]);
        $matches[4] = empty($matches[4]) ? '' : trim($matches[4]);
        if ($this->conf['spaceUnderscore']) {
            $matches[3] = preg_replace('/\s+/', '_', $matches[3]);
            $matches[4] = preg_replace('/\s+/', '_', $matches[4]);
        }
        if ($image) {
            return $this->image($matches[3] . (empty($matches[4]) ? '' : '#' . $matches[4]),
                                $text, $interlang, $colon);
        }
        if (!$interwiki && $interlang && isset($this->conf['url'])) {
            if ($interlang == $this->conf['langage']) {
                $interlang = '';
            } else {
                $interwiki = $this->conf['url'];
                $site = isset($this->conf['project']) ? $this->conf['project'][0] : '';
            }
        }
        if ($interwiki) {
            return $this->interwiki($site, $interwiki,
                $matches[3] . (empty($matches[4]) ? '' : '#' . $matches[4]),
                $text, $interlang, $colon);
        }
        if ($interlang) {
            $matches[3] = $interlang . ':' . $matches[3];
            $text = (empty($matches[5]) ? $interlang . ':' : '') . $text;
        }
        // set the options
        $options = array(
            'page'   => $matches[3],
            'anchor' => (empty($matches[4]) ? '' : '#'.$matches[4]),
            'text'   => $text
        );

        // create and return the replacement token
		*/
        //return $this->wiki->addToken($this->rule, $options);
    }
}
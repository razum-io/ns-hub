<?php
// vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4:
/**
 * Superscript rule end renderer for Xhtml
 *
 * PHP versions 4 and 5
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Paul M. Jones <pmjones@php.net>
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    CVS: $Id: Superscript.php 191862 2005-07-30 08:03:29Z toggg $
 * @link       http://pear.php.net/package/Text_Wiki
 */

/**
 * This class renders superscript text in XHTML.
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Paul M. Jones <pmjones@php.net>
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/Text_Wiki
 */
class Text_Wiki_Render_Xhtml_Divstart extends Text_Wiki_Render {

    var $conf = array(
        'css' => null
    );

    /**
    *
    * Renders a token into text matching the requested format.
    *
    * @access public
    *
    * @param array $options The "options" portion of the token (second
    * element).
    *
    * @return string The text rendered from the token options.
    *
    */

    function token($options)
    {
	
		if($options['type'] == 'start'){
			$style = '';
			if($options['position']){
				$style .= 'position:'.$options['position'].';';
			}
			if($options['width']){
				$style .= 'width:'.$options['width'].';';
			}
			if($options['height']){
				$style .= 'height:'.$options['height'].';';
			}
			if($options['bcolor']){
				$style .= 'background-color:'.$options['bcolor'].';';
			}
			if($options['color']){
				$style .= 'color:'.$options['color'].';';
			}
			if($options['margin']){
				if(is_numeric($options['margin'])){
					$style .= 'margin:'.$options['margin'].'px;';
				}else{
					$style .= 'margin:'.$options['margin'].';';
				}
			}
			if($options['padding']){
				if(is_numeric($options['padding'])){
					$style .= 'padding:'.$options['padding'].'px;';
				}else{
					$style .= 'padding:'.$options['padding'].';';
				}
			}
			if($options['align']){
				if($options['align'] == 'center'){
					$style .= 'margin-left:auto;margin-right:auto;';
				}else{
					$style .= 'float:'.$options['align'].';';
				}
			}
			
			return '<div style="'.$style.'">';
		}
    }
}
?>

<?php
// $Id$

/**
 * @file
 * User pages and forms.
 */

function legal_page() {
  global $language;
  $conditions = legal_get_conditions($language->language);
  $output = '';

  switch (variable_get('legal_display', '0')) {
    case 0: // Scroll Box.
      $output = nl2br(strip_tags($conditions['conditions']));
      break;
    case 1: // CSS Scroll Box with HTML.
    case 2: // HTML.
      $output = filter_xss_admin($conditions['conditions']);
      break;
    case 3: // Page Link.
      $output_node = node_load(trim($conditions['conditions']));
      $output_node = node_build_content($output_node);
      $output = $output_node->body;
      break;
  }

  return $output;
}

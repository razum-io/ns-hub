<?php
/** nid plugin for freelinking
 * Originially by [grayside](http://drupal.org/user/346868)
 * See http://drupal.org/node/486954
 *
 * @file
 * Allows for a link like [[node:<nid>]], [[n:<nid>]], or [[node:<nid>]] to be
 * expanded to a link to the node with the title associated with that nid.
 * A "could not find nid" message is displayed if the nid could not be found.
 */

$freelinking['nid'] = array(
  'indicator' => '/(n(id|ode)?)$/A',
  'callback' => 'freelinking_nid_callback',
  'tip' => t('Link to a local node by nid'),
  'run on view' => TRUE,
);

function freelinking_nid_callback($target, $plugin) { // resolve $target into a link to the node or display failure
  if (is_numeric($target['dest'])) {
    $params = array('nid' => $target['dest']);
    if ($node = node_load($params)) {
      if (node_access('view', $node)) {
        $title = $target['text'] ? $target['text'] : $node->title;
        if (!$target['tooltip']) {
          $target['tooltip'] = freelinking_internal_tooltip('node', $target['dest']);
        }
        return array(
          check_plain($title),
          'node/' . $target['dest'],
          array('attributes' => array('title' => $target['tooltip']))
        );
      }
    }
  }
  return array(
    'failover' => 'error',
    'message' => t('Invalid Node ID "!nid"', array('!nid' => $target['dest'])),
  );
}

// vim:tw=300 nowrap syn=php


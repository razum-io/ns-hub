<?php

/**
 * This function creates the settings page. It is a callback function for drupal_get_form()
 */
function automatic_password_replace_settings($form_state) {
    // Get user roles
    $roles = user_roles(TRUE);
    // Read table for all roles
    $expiry_data = db_query('SELECT rid, expiry, weight, action from {automatic_password_replace_expiry} ORDER BY weight ASC, rid ASC');
    $expiry = array();
    // Fill the table with data from database
    while($data = db_fetch_array($expiry_data)) {
        $expiry[$data['rid']] = array (
            'expiry' => $data['expiry'],
            'weight' => $data['weight'],
            'action' => $data['action']
            );
    }
    $form['expiry_data'] = array (
        '#type' => 'value',
	'#value' => $expiry,
    );
    // Create fieldset
    $form['expiry'] = array (
        '#type' => 'fieldset',
        '#title' => t('Password Expiry'),
        '#collapsible' => FALSE,
    );
    // Describe fieldset
    $form['expiry']['header'] = array (
        '#value' => '<p>' . t('Select the amount of time after which you would like a passwords of users in a role ' .
                    'to be automatically changed. Possible actions which can be taken when time is has elapsed are: Only remind ' .
                'or Change') . '</p>',
    );
    $form['expiry']['table'] = array (
	'#tree' => TRUE,
	'#theme' => 'automatic_password_replace_expiry',
    );
    $time_period = array (
	'hours',
	'days',
	'weeks',
	'years'
    );
    $actions = array (
        'Only remind',
        'Replace'
    );
    $heaviest_weight = 0;
	if(count($expiry)) {
            foreach($expiry as $rid => $data) {
                $form['expiry']['table'][$rid]['role'] = array (
                    '#value' => $roles[$rid],
		);
		if($data['expiry'] != '' && $data['expiry']) {
                    $expires = $data['expiry'];
                    $year = 60 * 60 * 24 * 365;
                    if($expires % $year === 0) {
                        $time_period_default = 3;
			$time_quantity_default = $expires / $year;
                    } else {
			$week = 60 * 60 * 24 * 7;
			if($expires % $week === 0) {
                            $time_period_default = 2;
                            $time_quantity_default = $expires / $week;
			} else {
                            $day = 60 * 60 * 24;
                            if ($expires % $day === 0) {
                                $time_period_default = 1;
                                $time_quantity_default = $expires / $day;
                            } else {
				$hour = 60 * 60;
				$time_period_default = 0;
				if($expires % $hour === 0) {
                                    $time_quantity_default = $expires / $hour;
				} else {
                                    $time_quantity_default = 0;
				}
                            }
			}
                    }
		} else {
                    $time_period_default = 0;
                    $time_quantity_default = 0;
		}
		$form['expiry']['table'][$rid]['time_quantity'] = array (
                    '#type' => 'textfield',
                    '#default_value' => $time_quantity_default,
		);
		$form['expiry']['table'][$rid]['time_period'] = array (
                    '#type' => 'select',
                    '#options' => $time_period,
                    '#default_value' => $time_period_default,
		);
                $form['expiry']['table'][$rid]['actions'] = array (
                    '#type' => 'select',
                    '#options' => $actions,
                    '#default_value' => $data['action'],
		);
		$form['expiry']['table'][$rid]['weight'] = array (
                    '#type' => 'weight',
                    '#delta' => count($roles),
                    '#default_value' => ($data['weight'] != '') ? $data['weight'] : 0,
		);
		$heaviest_weight = ($data['weight'] != '') ? $data['weight'] : 0;
            }
	}
        // Order table
	foreach($roles as $rid => $r) {
            if(!isset($form['expiry']['table'][$rid])) {
		$heaviest_weight++;
		$form['expiry']['table'][$rid]['role'] = array (
                    '#value' => $r,
		);
		$form['expiry']['table'][$rid]['time_quantity'] = array (
                    '#type' => 'textfield',
                    '#default_value' => 0,
		);
		$form['expiry']['table'][$rid]['time_period'] = array (
                    '#type' => 'select',
                    '#options' => $time_period,
		);
                $form['expiry']['table'][$rid]['actions'] = array (
                    '#type' => 'select',
                    '#options' => $actions,
		);
		$form['expiry']['table'][$rid]['weight'] = array (
                    '#type' => 'weight',
                    '#delta' => count($roles),
                    '#default_value' => $heaviest_weight,
		);
            }
	}
	$form['expiry']['footer'] = array (
            '#value' => '<p>' . t('Drag and drop the rows to set the priority for password replacement. The roles with the highest priority' .
                        ' should be placed at the top of the list. If a user is a member of more than one role, then the time after which' .
                        ' their password expires will be determined by whichever of their roles has the highest priority.' .
                        ' Any other roles will be ignored. On this note, placing the authenticated user role above any other role' .
                        ' will effectively nullify the expiry date for those roles, since all members are authenticated users.') . '</p>',
	);
	$form['submit'] = array (
            '#type' => 'submit',
            '#value' => t('Submit'),
	);
	return $form;
}

/**
 * Submit function for the settings form
 */
function automatic_password_replace_settings_submit($form, &$form_state) {
    // Save Table
    $arguments = array();
    foreach($form_state['values']['table'] as $rid => $expiry) {
        $multiplier = array(60 * 60, 60 * 60 * 24, 60 * 60 * 24 * 7, 60 * 60 * 24 * 365);
	$time_period = $expiry['time_quantity'] * $multiplier[$expiry['time_period']];
        if(isset($form_state['values']['expiry_data'][$rid])) {
            db_query('UPDATE {automatic_password_replace_expiry} SET expiry = %d, weight = %d, action = %d WHERE rid = %d', $time_period, $expiry['weight'], $expiry['actions'], $rid);
        } else {
            $add_query = 'INSERT INTO {automatic_password_replace_expiry} (rid, expiry, weight, action) VALUES (%d, %d, %d, %d)';
            $arguments = array($rid, $time_period, $expiry['weight'], $expiry['actions']);
            db_query($add_query, $arguments);
        }
    }
    // Amend users
    $users = array();
    $query = 'SELECT rid, expiry, action, weight ' .
             'FROM {automatic_password_replace_expiry} ' .
             'ORDER BY weight DESC';
    $result = db_query($query);
    while ($roles = db_fetch_array($result)) {
        $addrid = ($roles['rid'] != 2) ? 'WHERE ur.rid = ' . $roles['rid'] : '';
        if ($roles['expiry'] == 0) {
            $query_users = 'UPDATE {automatic_password_replace_users} AS ruser ' .
                           'LEFT JOIN {users_roles} AS ur ' .
                           'ON ur.uid = ruser.uid ' .
                           'SET ruser.will_be_replaced = 0, ' .
                           'ruser.action = ' . $roles['action'] . ', ruser.interval = ' . $roles['expiry'] . ' ' .
                           $addrid;
        } else {
            $query_users = 'UPDATE {automatic_password_replace_users} AS ruser ' .
                           'LEFT JOIN {users_roles} AS ur ' .
                           'ON ur.uid = ruser.uid ' .
                           'SET ruser.will_be_replaced = ruser.last_replaced + ' . $roles['expiry'] . ', ' .
                           'ruser.action = ' . $roles['action'] . ', ruser.interval = ' . $roles['expiry'] . ' ' .
                           $addrid;
        }
        db_query($query_users);
    }
    // Finished message
    drupal_set_message("All changes have been saved.");
}

/**
 * Theme function for password expiry on the settings page. This is used add the tabledrag for the expiry data
 */
function theme_automatic_password_replace_expiry($form)
{
	drupal_add_tabledrag('password_expiry_table', 'order', 'sibling', 'weight-group');
	$header = array(t('Role'), t('Expire password after:'), t('Weight'));
	$rows = array();
	foreach(element_children($form) as $key)
	{
		$element = &$form[$key];
		$element['weight']['#attributes']['class'] = 'weight-group';
		$row = array();
		$row[] = drupal_render($element['role']);
		$row[] = drupal_render($element['time_quantity']) . drupal_render($element['time_period']) . drupal_render($element['actions']);
		$row[] = drupal_render($element['weight']);
		$rows[] = array('data' => $row, 'class' => 'draggable');
	}
	$output = theme('table', $header, $rows, array('id' => 'password_expiry_table'));
	return $output . drupal_render($form);
}
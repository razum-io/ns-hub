<?php

/*
 * Convert single node, save .log & .tex file into appropriate folder, return TRUE if PDF was saved correct
 */

function printing_get_data ($pid, $nid, $book = FALSE, $type = "node", $source = "", $gid = 0) {
    // Load include with clean-up logic
    module_load_include("inc", "printing", "parser/printing.tagstable");

    // Load parser
    module_load_include("php", "printing", "w2l/w2lParser");
    module_load_include("php", "printing", "w2l/functions");

    if ($type != "node") {
        $body = $source;
    } else {
        // Retrieve data from node
        $sql = "SELECT node_revisions.body
                FROM {node} node
                LEFT JOIN {node_revisions} ON node_revisions.vid = node.vid
                WHERE node.nid = $nid";
        $body = db_result(db_query($sql));
    }
    /*
     * Clean wiki language
     */

    // Ingnore TOC for single node
    // TODO: Deal with [[Printing_]] tags (delete unappropriate ones)
    if(!$book) {
        $body = str_replace("[[Toc]]", "", $body);
    }

    // Add a \nsdocno tag
    if ($type == "node") {
        // Retrieve data from node
        $sql = "SELECT content_type_wiki.field_nsdocno_value
                FROM {node} node
                LEFT JOIN {content_type_wiki} ON content_type_wiki.vid = node.vid
                WHERE node.nid = $nid";
        $nsdocno = db_result(db_query($sql));
        $nsdocno = ($nsdocno) ? '<rawtex>\renewcommand{\nsdocno}{</rawtex>' . $nsdocno . '<rawtex>}</rawtex>' . "\n" : '<rawtex>\renewcommand{\nsdocno}{\empty}</rawtex>' . "\n";
        $body = $nsdocno . $body;
    }

    // Replace tags which aren't in parser and they can be simply replaced
    $tags = printing_noparsertags();
    foreach ($tags as $tag => $replacement) {
        $body = str_replace($tag, $replacement, $body);
    }

    // TODO: Deal with additional printing tags, just Div is done now

    $body = printing_newtags($body, $nid, $type, $pid, $gid);

    // Parse wiki

    $parser = new Wiki2LaTeXParser();
    $title = printing_parser_setup();


    $sql = "SELECT printing_geometry.m_right, printing_geometry.m_left, printing_geometry.width
            FROM {printing_paper} printing_paper
            LEFT JOIN {printing_geometry} printing_geometry
                ON printing_geometry.GID = printing_paper.def_GID
            WHERE printing_paper.PID = $pid";

    $width_params = db_fetch_array(db_query($sql));
    $textwidth = (int) $width_params['width'] - ((int) $width_params['m_right'] + (int) $width_params['m_left']);
    $units = substr($width_params['width'], -2);
    $params = array(
        "type" => $type, // Type of parsing can be "node" or "geometry"
        "textwidth" => $textwidth,
        "units" => $units
    );
    $body = $parser->parse($body, $title, 'W2L_STRING', $params);

    // Clean-up raw latex (revert changes between <rawtex> ... </rawtex> tags

    $body = printing_afterparser($body);
    
    // In HTML is allowed to have multiple end of row even while paragraph follows. Not in Latex

    $oldbody = $body;
    $body = str_replace('\\\\' . "\n\n", "\n\n", $body);
    while ($oldbody != $body) {
        $oldbody = $body;
        $body = str_replace('\\\\' . "\n\n", "\n\n", $body);
    }

    // return Latex file
    return $body;
}

function printing_get_header ($pid) {

    $sql = "SELECT printing_paper.TID, printing_paper.fontshape, printing_paper.fontssize, printing_paper.head_1, printing_paper.head_2, printing_paper.head_3,
            printing_paper.head_4, printing_paper.head_5, printing_paper.head_6, printing_paper.orphan, printing_paper.facing, printing_paper.def_GID,
            printing_geometry.width, printing_geometry.height, printing_geometry.m_top, printing_geometry.m_right, printing_geometry.m_bottom,
            printing_geometry.m_left, printing_geometry.hline, printing_geometry.fline, printing_geometry.headsep, printing_geometry.footskip, printing_geometry.headheight,
            printing_geometry.units, header_left.latex as hl, header_center.latex as hc, header_right.latex as hr, footer_left.latex as fl,
            footer_center.latex as fc, footer_right.latex as fr
            FROM {printing_paper} printing_paper
            LEFT JOIN {printing_geometry} printing_geometry
                ON printing_geometry.GID = printing_paper.def_GID
            LEFT JOIN {printing_hf} header_left
                ON header_left.HFID = printing_geometry.def_LH
            LEFT JOIN {printing_hf} header_center
                ON header_center.HFID = printing_geometry.def_CH
            LEFT JOIN {printing_hf} header_right
                ON header_right.HFID = printing_geometry.def_RH
            LEFT JOIN {printing_hf} footer_left
                ON footer_left.HFID = printing_geometry.def_LF
            LEFT JOIN {printing_hf} footer_center
                ON footer_center.HFID = printing_geometry.def_CF
            LEFT JOIN {printing_hf} footer_right
                ON footer_right.HFID = printing_geometry.def_RF
            WHERE printing_paper.PID = $pid";
    $options = db_fetch_array(db_query($sql));

    $paper = '\documentclass[a4paper, ' .  $options['fontssize'] . 'pt';
    if ($options['facing']) {
        $paper .= ', twoside';
    }
    $paper .= ']{article}' . "\n";
    $paper .= '\usepackage[top=' . $options['m_top'] . ', bottom=' . $options['m_bottom'] . ', left=' . $options['m_left'] .
              ', right=' . $options['m_right'] . ', paperwidth=' . $options['width'] . ', paperheight=' . $options['height'] . ']{geometry}' . "\n";
    $paper .= '\usepackage[pdftex]{graphicx}' . "\n" .
              '\usepackage{ulem}' . "\n" .
              '\usepackage{enumerate}' . "\n" .
              '\usepackage{pbox}' . "\n" .
              '\usepackage{xfrac}' . "\n" .
              '\usepackage{mathtools}' . "\n" .
              '\usepackage{alltt}' . "\n" .
              '\usepackage{multirow}' . "\n" .
              '\usepackage{url}' . "\n" .
              '\usepackage{colortbl}' . "\n" .
              '\usepackage{epstopdf}' . "\n" .
              '\newcommand{\urlwofont}[1] { \urlstyle{same}\url{#1} }' . "\n" .
              '\usepackage{tabularx}' . "\n" .
              '\usepackage{fancyhdr}' . "\n" .
              '\usepackage{float}' . "\n" .
              '\usepackage{wrapfig}' . "\n" .
              '\pagestyle{fancy}' . "\n";
    $paper .= '\renewcommand{\headrulewidth}{' . $options['hline'] . '}' . "\n";
    $paper .= '\renewcommand{\footrulewidth}{' . $options['fline'] . '}' . "\n";
    if ($options['m_top']) {
        $paper .= '\widowpenalty=10000' . "\n" .
                  '\clubpenalty=10000' . "\n";
    } else {
        $paper .= '\widowpenalty=1' . "\n" .
                  '\clubpenalty=1' . "\n";
    }
    $watermark = file_directory_path() . '/private/Document-' . $options['TID'] . '/Paper-' . $pid . '/watermark/watermark.pdf';
    if (is_file($watermark)) {
        $watermark = realpath($watermark);
        $paper .= '\usepackage{draftwatermark}' . "\n" .
                  '\SetWatermarkText{\includegraphics{' . $watermark . '}}' . "\n" .
                  '\SetWatermarkAngle{0}' . "\n";
    }
    $paper .= '\renewcommand{\contentsname}{Table Of Contents}' . "\n" .
              '\usepackage{array,booktabs,xcolor}' . "\n" .
              '\usepackage{hhline}' . "\n" .
              '\usepackage{arydshln}' . "\n" .
              '\usepackage[utf8]{inputenc}' . "\n" .
              '\usepackage[T1]{fontenc}' . "\n" .
              '\usepackage[labelformat=empty,font={footnotesize,sf}]{caption}' . "\n" .
              '\setlength{\arrayrulewidth}{1pt}' . "\n" .
              '\newcolumntype{Z}{>{\centering \arraybackslash}X}' . "\n" .
              '\newcolumntype{Y}{>{\raggedright \arraybackslash}X}' . "\n" .
              '\newcolumntype{W}{>{\raggedleft \arraybackslash}X}' . "\n";
    $paper .= '\newcommand\headerone[1]{\vspace{1em} {' . $options['head_1'] . ' #1} \vspace{0.8em}}' . "\n" .
              '\newcommand\headertwo[1]{\vspace{1em} {' . $options['head_2'] . ' #1} \vspace{0.8em}}' . "\n" .
              '\newcommand\headerthree[1]{\vspace{0.8em} {' . $options['head_3'] . ' #1} \vspace{0.5em}}' . "\n" .
              '\newcommand\headerfour[1]{\vspace{0.8em} {' . $options['head_4'] . ' #1} \vspace{0.5em}}' . "\n" .
              '\newcommand\headerfive[1]{\vspace{0.1em} {' . $options['head_5'] . ' #1} \vspace{0.2em}}' . "\n" .
              '\newcommand\headersix[1]{\vspace{0.1em} {' . $options['head_6'] . ' #1} \vspace{0.2em}}' . "\n";
    $paper .= '\aboverulesep0pt' . "\n" .
              '\belowrulesep0pt' . "\n" .
              '\newlength{\oldtabcolsep}' . "\n" .
              '\setlength{\oldtabcolsep}{\tabcolsep}' . "\n" .
	      '\usepackage[ddmmyyyy]{datetime}' . "\n" .
              '\usepackage{fixltx2e}' . "\n" .
              '\makeatletter' . "\n" .
              '\newcommand\noitemerroroff{\let\@noitemerr\relax}' . "\n" .
              '\newcommand\noitemerroron{\let\@noitemerr\@oldnoitemerr}' . "\n" .
              '\newcommand*{\compress}{\@minipagetrue}' . "\n" .
              '\makeatother' . "\n" .
              '\begingroup' . "\n" .
              '\toks0=\expandafter{\multicolumn{#1}{#2}{#3}}' . "\n" .
              '\edef\x{\endgroup' . "\n" .
              '\long\def\noexpand\multicolumn##1##2##3{\the\toks0 }}\x' . "\n";
    $paper .= '\usepackage[pdfborder={0 0 0}]{hyperref}' . "\n" .
              '\hypersetup{pdfborder=0 0 0}' . "\n";
    if ($options['fontshape'] == 'Sans Serif') {
        $paper .= '\renewcommand{\familydefault}{\sfdefault}' . "\n";
    }
    $paper .= '\newcommand{\nsdocno}{\empty}' . "\n";
    $paper .= "\n" . '\begin{document}' . "\n" .
              '\headheight' . $options['headheight'] . "\n" .
              '\headsep' . $options['headsep'] . "\n" .
              '\footskip' . $options['footskip'] . "\n" .
              '\setlength{\parindent}{0in}' . "\n" .
              '\noitemerroroff' . "\n\n";
    $paper .= '\fancyhead[L]{' . $options['hl'] . '}' . "\n" .
              '\fancyhead[C]{' . $options['hc'] . '}' . "\n" .
              '\fancyhead[R]{' . $options['hr'] . '}' . "\n" .
              '\fancyfoot[L]{' . $options['fl'] . '}' . "\n" .
              '\fancyfoot[C]{' . $options['fc'] . '}' . "\n" .
              '\fancyfoot[R]{' . $options['fr'] . '}' . "\n";

    return $paper;
}

function printing_create_pdf($pid, $nid, $whole = false) {

    // Clean-up printing query for this session
    global $user;
    db_query("DELETE FROM {printing_progress} WHERE session = '$user->sid'");

    // Don't be interrupted
    ignore_user_abort(true);
    set_time_limit(0);

    // Get header file
    $file_data = printing_get_header($pid);

    // Define path, TID & Paper name
    $path = 'sites/default/files/private';
    $mynode = node_load($nid);
    $tid = drillingstandardmod_get_booktid($mynode);
    $format_name = db_result(db_query("SELECT name
                                       FROM {printing_paper} printing_paper
                                       WHERE printing_paper.PID = $pid"));

    /*
     *  Create LaTeX files
     */

    if ($whole) {
        $chinese = false;
        // We want whole document
        // So let's figure out name of document
        $document_term = taxonomy_get_term($tid);
        $first = true;

        $sql = "SELECT node.nid
                FROM {node} node
                LEFT JOIN {term_node} term_node on term_node.vid=node.vid
                WHERE (term_node.tid = '$tid') and (node.type = 'wiki')
                ORDER BY term_node.weight_in_tid asc";
        $query = db_query($sql);
        $num_rows = $query->num_rows + 1;
        $node_seq = 0;
        while ($article_nid = db_fetch_array($query)) {
            $node_seq++;
            printing_add_to_query($tid, $article_nid['nid'], $pid, $user->sid, $num_rows, $node_seq);
            $articles[] = $article_nid['nid'];
        }
        // Create folders and file name for binding document, add this file to query
        $node_seq++;
        $bpath = $path;
        $bdirectories = array('/Document-' . $tid, '/Paper-' . $pid);
        foreach ($bdirectories as $bdirectory) {
            $bpath .= $bdirectory;
            if (!is_dir($bpath)) {
                $old = umask(0);
                mkdir($bpath, 0777);
                umask($old);
            }
        }
        $bname_nosuffix = printing_sanitize_filename($document_term->name . "-" . $format_name);
        $bname = $bpath . "/" . $bname_nosuffix . ".tex";
        $body_links = "";
        $header_data = $file_data;
        printing_add_to_query($tid, 0, $pid, $user->sid, $num_rows, $node_seq);
        // Print all nodes and prepare body of binding document
        foreach ($articles as $single_article) {
            set_time_limit(0);
            $result = printing_print_single_node ($path, $single_article, $format_name, $tid, $pid, $whole, $header_data);
            if (!$chinese && $result['chinese']) {
                $chinese = true;
            }
            if ($result['flag']) {
                if ($first) {
                    $file_data .= '\headerone{Documents which was not printed correct}' . "\n\n" . $result['name'] . '\\\\' . "\n";
                    $first = false;
                } else {
                    $file_data .= $result['name'] . '\\\\' . "\n";
                }
            } else {
                $body_links .= '\input{' . $result['file'] . "}\n" . '\clearpage' . "\n";
            }
            db_query("DELETE FROM {printing_progress} WHERE session = '$user->sid' AND NID = '$single_article'");
        }
        $file_data .= $body_links . '\end{document}';
        // Change file if Chinese characters are there
        if($chinese) {
            $pattern = '/^\\\\documentclass\[(.*)\]\{article\}$/mi';
            $replacement = '\documentclass[UTF8, $1]{ctexart}';
            $file_data = preg_replace($pattern, $replacement, $file_data);
        }
        // Write file
        printing_write_latex_file ($bname, $file_data);
        if (printing_latex_shell($bpath, $bname_nosuffix, 0, $tid, $pid)) {
            drupal_set_message(t("An error occured during pdf generation, consult log file."), 'error');
        }
    } else {
        // We want just this node - add printing to query
        printing_add_to_query($tid, $nid, $pid, $user->sid);
        // Print it
        if (printing_print_single_node ($path, $nid, $format_name, $tid, $pid, $whole, $file_data)) {
            drupal_set_message(t("An error occured during pdf generation, consult log file."), 'error');
        }
    }
    // Purge printing query, delete temp files (images)
    db_query("DELETE FROM {printing_progress} WHERE session = '$user->sid'");
    printing_delete_file();
}

function printing_print_single_node ($path, $nid, $format_name, $tid, $pid, $whole, $file_data) {
    $file_nosuffix = printing_get_latex_filename($nid, $format_name);
    $outpath = printing_create_directories($tid, $nid, $pid, $path); // Files needs path without initial slash
    $binder_filename = $outpath . "/" . $file_nosuffix . ".tex";
    $body_filename = $outpath . "/" . $file_nosuffix . "-body.tex";
    // Get latex file
    $file_data_body = printing_get_data($pid, $nid, $whole);
    // Check for Chinese
    preg_match('/\\p{Han}/u', $file_data_body, $matches);
    if (count($matches) > 0) {
        $pattern = '/^\\\\documentclass\[(.*)\]\{article\}$/mi';
        $replacement = '\documentclass[UTF8, $1]{ctexart}';
        $file_data = preg_replace($pattern, $replacement, $file_data);
        $chinese = true;
    } else {
        $chinese = false;
    }
    // Save it
    printing_write_latex_file($body_filename, $file_data_body);
    // Create binding file
    $body_filename = realpath($body_filename);
    $file_data .= '\input{' . $body_filename . '}' . "\n" . '\end{document}';
    // Save it
    printing_write_latex_file($binder_filename, $file_data);
    // Create PDF if possible
    $return_flag = printing_latex_shell($outpath, $file_nosuffix, $nid, $tid, $pid);
    if ($whole) {
        return array("flag" => $return_flag, "file" => $body_filename, "name" => $file_nosuffix . ' - http://www.drillingstandard.com/node/' . $nid, "chinese" => $chinese);
    } else {
        return $return_flag;
    }
}

function printing_create_directories ($tid, $nid, $pid, $path) {
    $directories = array('/Document-' . $tid, '/Node-' . $nid, '/Paper-' . $pid);
    foreach ($directories as $directory) {
        $path .= $directory;
        if (!is_dir($path)) {
            $old = umask(0);
            mkdir($path, 0777);
            umask($old);
        }
    }
    return $path;
}

function printing_get_latex_filename ($nid, $format_name) {
    $mynode = node_load($nid);
    $mynode = node_build_content($mynode);
    if (trim($mynode->field_subtitle[0]['value']) != '') {
        $filename = trim($mynode->field_subtitle[0]['value']) . "-" . $format_name;
    } else {
        $filename = trim($mynode->title) . "-" . $format_name;
    }
    $filename = printing_sanitize_filename($filename);
    return $filename;
}

function printing_write_latex_file ($name, $content) {
    $fh = fopen($name, 'w');
    if(!fwrite($fh, $content)) {
        drupal_set_message("Something went wrong, try again.", "error");
    }
    fclose($fh);
}

function printing_add_to_query($tid, $nid, $pid, $sid, $total = 1, $count = 1) {
    $data = array(
            "TID" => $tid,
            "NID" => $nid,
            "PID" => $pid,
            "session" => $sid,
            "total" => $total,
            "count" => $count
        );
    drupal_write_record("printing_progress", $data);
}

function printing_latex_shell ($outputpath, $filename, $nid, $tid, $pid) {
    $printing_command = "cd ./$outputpath && pdflatex --shell-escape --synctex=1 " . $filename . ".tex"; // command need initial slash
    $logfile = $outputpath . "/" . $filename . ".log";
    $pdffile = $outputpath . "/" . $filename . ".pdf";
    exec($printing_command, $op, $return_var);

    if (!$return_var) {
        exec($printing_command, $op, $return_var);
    }

    if ($return_var) {
        printing_write_file('log', $logfile, $nid, $tid, $pid);
    } else {
        printing_write_file('pdf', $pdffile, $nid, $tid, $pid);
    }
    return $return_var;
}
<?php

/*
 * Sanitize tags not parsed by parser
 */

function printing_noparsertags() {
    $tags = array(
        '[[Print:noscreen]]' => '',
        '[[Print:/noscreen]]' => '',
        '[[Print:sloppy]]' => '<rawtex>\sloppy</rawtex>',
        '[[Print:fussy]]' => '<rawtex>\fussy</rawtex>',
        '[[Print:pagebreak]]' => '<rawtex>\clearpage</rawtex>',
        '[[Print:flushvertical]]' => '<rawtex>\vspace*{\fill}</rawtex>',
        '[[center]]' => '<rawtex>\begin{center}</rawtex>',
        '[[/center]]' => '<rawtex>\end{center}</rawtex>',
        '[[blockquote]]' => '<blockquote>',
        ' ' => '~',
        '[[/blockquote]]' => '</blockquote>',
        '½' => '<rawtex>$\sfrac{1}{2}$</rawtex>',
        '⅓' => '<rawtex>$\sfrac{1}{3}$</rawtex>',
        '¼' => '<rawtex>$\sfrac{1}{4}$</rawtex>',
        '¾' => '<rawtex>$\sfrac{3}{4}$</rawtex>',
        '⅛' => '<rawtex>$\sfrac{1}{8}$</rawtex>',
        '⅜' => '<rawtex>$\sfrac{3}{8}$</rawtex>',
        '⅝' => '<rawtex>$\sfrac{5}{8}$</rawtex>',
        '⁄' => '/',
        '⅞' => '<rawtex>$\sfrac{7}{8}$</rawtex>',
        '¹' => '<rawtex>$^1$</rawtex>',
        '₁' => '<rawtex>$_1$</rawtex>',
        '²' => '<rawtex>$^2$</rawtex>',
        '₂' => '<rawtex>$_2$</rawtex>',
        '³' => '<rawtex>$^3$</rawtex>',
        '₃' => '<rawtex>$_3$</rawtex>',
        '₄' => '<rawtex>$_4$</rawtex>',
        '⁵' => '<rawtex>$^5$</rawtex>',
        '₆' => '<rawtex>$_6$</rawtex>',
        '⁷' => '<rawtex>$^7$</rawtex>',
        '⁹' => '<rawtex>$^9$</rawtex>',
        'θ' => '<rawtex>$\Theta$</rawtex>',
        '≥' => '<rawtex>$\geq $</rawtex>',
        'ᶧ' => '<rawtex>$^\ddagger$</rawtex>',
        'º' => '˚',
        '˚' => '<rawtex>$^\circ$</rawtex>',
        '≤' => '<rawtex>$\leq $</rawtex>',
        '----' => '<rawtex>\hrulefill</rawtex>',
        'ﬂ' => 'fl',
        'ﬁ' => 'fi',
        '<p>' => '',
        '</p>' => '',
        '[[sub]]' => '<sub>',
        '[[/sub]]' => '</sub>',
        '[[sup]]' => '<sup>',
        '[[/sup]]' => '</sup>',
        '<sub>' => '<rawtex>\textsubscript{</rawtex>',
        '</sub>' => '<rawtex>}</rawtex>',
        '<sup>' => '<rawtex>\textsuperscript{</rawtex>',
        '</sup>' => '<rawtex>}</rawtex>',
        '<tex>' => '<rawtex>',
        '</tex>' => '</rawtex>',
        '<ins>' => '<rawtex>\uline{</rawtex>',
        '</ins>' => '<rawtex>}</rawtex>',
        '<del>' => '<rawtex>\sout{</rawtex>',
        '</del>' => '<rawtex>}</rawtex>',
        '<br/>' => '<rawtex>\\\\</rawtex>',
        '<br />' => '<rawtex>\\\\</rawtex>',
        '<br>' => '<rawtex>\\\\</rawtex>',
    );
    return $tags;
}

/*
 * Header for parser
 */

function printing_parser_setup() {
    $title = array(
        'mTextform' => 'Main Page',
        'mUrlform' => 'Main_Page',
        'mDbkeyform' => 'Main_Page',
        'mUserCaseDBKey' => 'Main_Page',
        'mNamespace' => '0',
        'mInterwiki' => '',
        'mFragment' => '',
        'mArticleID' => '1',
        'mLatestID' => '2',
        'mRestrictions' => Array (
            'edit' => Array(),
            'move' => Array()
            ),
        'mOldRestrictions' => '',
        'mCascadeRestriction' => '',
        'mCascadingRestrictions' => '',
        'mRestrictionsExpiry' => Array (
            'edit' => 'infinity',
            'move' => 'infinity'
            ),
        'mHasCascadingRestrictions' => '',
        'mCascadeSources' => '',
        'mRestrictionsLoaded' => '1',
        'mPrefixedText' => 'Main Page',
        'mTitleProtection' => '',
        'mDefaultNamespace' => '0',
        'mWatched' => '',
        'mLength' => '7682',
        'mRedirect' => '',
        'mNotificationTimestamp' => Array (),
        'mBacklinkCache' => ''
        );
    return $title;
}

/*
 * We need pass variable to Img tag callback
 */

class printing_epgi {
    public function __call($method, $args) {
        if (isset($this->$method)) {
            $func = $this->$method;
            return call_user_func_array($func, $args);
        }
    }
}

class MyImgCallback {
    private $key;

    function __construct($key) {
        $this->key = $key;
    }

    public function callback($matches) {
        $matches = str_replace("<rawtex>\_</rawtex>", "_", $matches[1]);
        $img_name_array = explode("|",$matches);
	$file_new = $img_name_array[0];
        if (is_array($this->key)) {
            $path = "sites/default/files/private/Document-" . $this->key['tid'] . "/Paper-" . $this->key['pid'] . "/Geometry-" . $this->key['gid'] . "/images";
        } else {
            $path = 'sites/default/files/Document-' . $this->key;
        }
        if(strpos($matches, "|") === false) {
            $image_remaining = "";
        } else {
            $image_remaining = substr($matches, strpos($matches, "|"));
        }

        // Local or external?
        if(strpos($file_new, "http://", 0)===false) {

            // check if we really have file name
            if (strrpos($file_new, "/")) {
                $temp_name = explode('/',$file_new);
                $file_new = $temp_name[count($temp_name)-1];
            }
            $file_new_name1 = explode(".",$file_new);
            $file_new_name = $path . '/' . $file_new;

            // Check if we have original
            if (count($file_new_name1) > 0) {
                $tempname = $path . '/originals/' . $file_new_name1[0] . '.' .$file_new_name1[1]; // Re-create original name

                // Sanitize tiffs
                if (file_exists($tempname) && (strtolower($file_new_name1[1]) == 'tif')) {
                    $magick_dir = '"convert"';
                    $tmppath = $path . '/temp/';
                    $tempname_new = $tmppath . substr(md5(uniqid(rand(), true)), 16) . ".jpg";
                    while (file_exists($tempname_new)) {
                        $tempname_new = $tmppath . substr(md5(uniqid(rand(), true)), 16) . ".jpg";
                    }
                    $send_cmd = $magick_dir . " " . $tempname . " " . $tempname_new;
                    $result = exec($send_cmd);
                    if (file_exists($tempname_new)) {
                        $file_new_name = $tempname_new;
                        printing_write_file('temp', $file_new_name);
                    }
                } elseif (file_exists($tempname)) {
                    $file_new_name = $tempname;
                }
            }
        } else {
            $temppath = $path . '/temp/';
            $ch = curl_init($file_new);
            $suffix = explode(".", $file_new);
            $suffix = $suffix[count($suffix) - 1];
            $file_new_name = $temppath . substr(md5(uniqid(rand(), true)), 16) . "." . $suffix;
            while (file_exists($file_new_name)) {
                $file_new_name = $temppath . substr(md5(uniqid(rand(), true)), 16) . "." . $suffix;
            }
            $fp = fopen($file_new_name, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            if (!is_array($this->key)) {
                printing_write_file('temp', $file_new_name);
            }
        }
        // Final check
        if (!file_exists($file_new_name)) {
            $file_new_name = $file_new;
        } else {
            // Sanitize GIF & EPS
            $file_type = explode(".", $file_new_name);
            $type = strtolower($file_type[count($file_type) - 1]);
            if (in_array($type, array("gif", "eps"))) {
                $tempname = $file_new_name;
                //Michal: new approach
                $create_temp = new printing_epgi();
                // Create new name of file
                $create_temp->createname = function($args) {
                    $new = $args['path'] . '/temp/' . substr(md5(uniqid(rand(), true)), 16) . '.' . $args['suffix'];
                        while (file_exists($new)) {
                            $new = $args['path'] . '/temp/' . substr(md5(uniqid(rand(), true)), 16) . '.' . $args['suffix'];
                        }
                    return $new;
                };
                switch ($type) {
                    case "gif":
                        $create_temp->createfile = function($args) {
                            exec('"convert"' . ' ' . $args['original'] . ' ' . $args['newfile']);
                        };
                        $newtype = 'jpg';
                        break;
                    case "eps":
                        $create_temp->createfile = function($args) {
                            exec('"epstopdf"' . ' ' . $args['original'] . ' --outfile=' . $args['newfile']);
                        };
                        $newtype = 'pdf';
                        break;
                }
                $tempname_new = $create_temp->createname(array('path' => $path, 'suffix' => $newtype));
                $create_temp->createfile(array('original' => $tempname, 'newfile' => $tempname_new));
                if (file_exists($tempname_new)) {
                    $file_new_name = $tempname_new;
                    if (!is_array($this->key)) {
                        printing_write_file('temp', $file_new_name);
                    }
                }

                /*Michal: The old one
                $magick_dir = '"convert"';
                $tmppath = $path . '/temp/';
                $tempname_new = $tmppath . substr(md5(uniqid(rand(), true)), 16) . ".jpg";
                while (file_exists($tempname_new)) {
                    $tempname_new = $tmppath . substr(md5(uniqid(rand(), true)), 16) . ".jpg";
                }
                $send_cmd = $magick_dir . " " . $tempname . " " . $tempname_new;
                    $result = exec($send_cmd);
                    if (file_exists($tempname_new)) {
                        $file_new_name = $tempname_new;
                        if (!is_array($this->key)) {
                            printing_write_file('temp', $file_new_name);
                        }
                    }
                */
            }
            // Sanitize files with multiple dots in name
            if (count($file_type) > 2) {
                $tmppath = $path . '/temp/';
                $tempname_new = $tmppath . substr(md5(uniqid(rand(), true)), 16) . "." . $file_type[count($file_type) - 1];
                while (file_exists($tempname_new)) {
                    $tempname_new = $tmppath . substr(md5(uniqid(rand(), true)), 16) . "." . $file_type[count($file_type) - 1];
                }
                copy($file_new_name, $tempname_new);
                if (file_exists($tempname_new)) {
                    $file_new_name = $tempname_new;
                    if (!is_array($this->key)) {
                        printing_write_file('temp', $file_new_name);
                    }
                }
            }
            // Create tag
            $file_new_name = '[[Image:' . realpath($file_new_name) . $image_remaining . ']]';
        }
        return $file_new_name;
    }
}

class MyTitleCallback {
    private $key;

    function __construct($key) {
        $this->key = $key;
    }

    public function callback($match) {
        $value = _drillingstandardmod_parse_title($match[1]); // Sanitize input
        $level = trim($value[1]);
	$level = $level ? $level : 6;
        if (($value[2] == 'numbering-on') && ($this->key != "hf")) {
            for($i = $level+1; $i <= 6; $i++) {
                $levels[$i-1] = 0;
            }
            $levels[$level-1]++;
            $titlelevel = $levels;
            $levels = $this->key;
            // reset all levels above
            for($i=0; $i<6;$i++) {
                if($i != ($level-1)) {
                    $titlelevel[$i] = 0;
                }
            }
            // reset all levels below
            for($i=$level;$i<6;$i++) {
                $levels[$i] = 0;
            }
            $templevels = array($levels[0] + ($titlelevel[0]?1:0),
                                $levels[1] + ($titlelevel[1]?1:0),
                                $levels[2] + ($titlelevel[2]?1:0),
                                $levels[3] + ($titlelevel[3]?1:0),
                                $levels[4] + ($titlelevel[4]?1:0),
                                $levels[5] + ($titlelevel[5]?1:0),);
            $finallevel = array();
            for($i=0; $i<6 && $i<$level; $i++) {
                $finallevel[] = $templevels[$i];
            }
            for($i=count($finallevel)-1; $i>=0; $i--) {
                if($finallevel[$i]) {
                    break;
                }
                unset($finallevel[$i]);
            }
        }
        $titleattribute = str_repeat("=", $level);
	if($finallevel) {
            $titlelabel = $titleattribute . '<privatetitle>' . implode('.', $finallevel) . ' ' . trim($value[0]) . $titleattribute;
            $this->key = $templevels;
	} elseif ($this->key != "hf") {
            $titlelabel = $titleattribute . '<privatetitle>' . trim($value[0]) . $titleattribute;
        } else {
            $titlelabel = $titleattribute . trim($value[0]) . $titleattribute;
        }
        
        return "\n" . $titlelabel . "\n";
    }
}

class MyFootnoteCallback {

    function __construct($key) {
        $this->key = $key;
        $this->footarr = array();
    }

    public function callback($matches) {
        $this_footnote = $this->getunique();
        if (preg_match('/value\s*=\s*"(\d+)"/iU', $matches[1], $footnoteno)) {
            if (array_key_exists($footnoteno[1], $this->footarr)) {
                $counter_name = $this->getunique();
                $this->footarr[$this_footnote]['texstring'] = '<rawtex>\footnotemark[\value{' . $counter_name . '}]</rawtex>';
                $this->footarr[$footnoteno[1]]['texstring'] .= '<rawtex>\setcounter{' . $counter_name . '}{\value{footnote}}</rawtex>';
                $this->footarr[$this_footnote]['mask'] = $this_footnote;
                $this->footarr['counters'][] = '<rawtex>\newcounter{' . $counter_name . '}</rawtex>';
            } else {
                $this->footarr[$footnoteno[1]]['texstring'] = '<rawtex>\footnote{</rawtex>' . $matches[2] . '<rawtex>}</rawtex>';
                $this->footarr[$footnoteno[1]]['mask'] = $this_footnote;
            }
        } else {
            $this->footarr[$this_footnote]['texstring'] = '<rawtex>\footnote{</rawtex>' . $matches[2] . '<rawtex>}</rawtex>';
            $this->footarr[$this_footnote]['mask'] = $this_footnote;
        }
        return $this_footnote;
    }

    public function finalize($body) {
        if (isset($this->footarr['counters']) && count($this->footarr['counters']) > 0) {
            $body = implode("\n", $this->footarr['counters']) . "\n" . $body;
        }
        foreach ($this->footarr as $footnote) {
            $body = str_replace($footnote['mask'], $footnote['texstring'], $body);
        }
        return $body;
    }

    private function getunique() {
        return "uni-" . $this->key . '-' . dechex(mt_rand(0, 0x7fffffff));
    }
}

/*
 * Our new defined tags
 */

function printing_newtags($body, $nid, $type = "node", $pid = 0, $gid = 0) {
    $mynode = node_load($nid);
    $tid = drillingstandardmod_get_booktid($mynode);
    if ($type == "node") {
        // Set-up levels of titles
        $sql = "select content_field_wikititles.nid,content_field_wikititles.vid,content_field_wikititles.delta,content_field_wikititles.field_wikititles_value,node.title from {node} node
                left join {content_field_wikititles} content_field_wikititles on node.vid=content_field_wikititles.vid
                left join {term_node} term_node on term_node.vid=node.vid
                where term_node.tid='$tid' and content_field_wikititles.field_wikititles_value is not null
                order by term_node.weight_in_tid asc, content_field_wikititles.delta asc";
        $query = db_query($sql);
        $levels = array(0, 0, 0, 0, 0, 0);
        while($r = db_fetch_array($query)) {
            if($mynode->vid == $r['vid']) {
                break;
            }
            $v = $r['field_wikititles_value'];
            if(strpos($v, 'numbering-off')) {
                continue;
            }
            $parts = explode('|', $v);
            $level = $parts[2];
            $levels[$level-1]++;
            for($i = $level; $i<6; $i++){
                $levels[$i] = 0;
            }
        }
    } else {
        $tid = array(
            "tid" => $tid,
            "pid" => $pid,
            "gid" => $gid
        );
        $levels = "hf";
        preg_replace("/\[fn(.*?)\](.*?)\[\/fn]/is", "", $body);
    }

    /*
     * Div tag
     */
     // @TODO: add nesting
     $body = preg_replace_callback(
        "/\[\[Div:(.*?)\]\](.*?)\[\[\/Div\]\]/is",
        "printing_newtags_div",
        $body
     );

    /*
     * Footnote
     */
    // @TODO: add nesting

    $callback = new MyFootnoteCallback($nid);
    $body = preg_replace_callback(
        "/\[fn(.*?)\](.*?)\[\/fn]/is",
        array($callback, 'callback'),
        $body
    );
    $body = $callback->finalize($body);

    /*
     * Img tag
     */
    $callback = new MyImgCallback($tid);
    $body = preg_replace_callback(
            "/\[\[Image:(.*?)\]\]/is",
            array($callback, 'callback'),
            $body
            );

    /*
     * Title tag
     */
    $callback = new MyTitleCallback($levels);
    $body = preg_replace_callback(
            '/\[\[Title\:(.*?)\]\]/is',
            array($callback, 'callback'),
            $body
            );

    /*
     * Color tag
     */
    // Replace all occurences of pairs of </span> pairs where is color in it

    $body = preg_replace_callback(
        '/\<span\s*style\s*=\s*\"(?:[^"]*?)color:([0-9a-z#]+)(?:;[^"]*?)*"\>(.*?)\<\s*\/span\s*\>/is',
        "printing_intext_colors_replace",
        $body
     );

    /*
     * Print tags
     */
    // @TODO:
    //[[Print:nopagebreak]]...[[Print:/nopagebreak]], [[Print:newgeometry|id]]

    $body = preg_replace_callback(
        "/\[\[Print:textsize\|(-*\d)\]\](.*?)\[\[Print:\/textsize\]\]/is",
        "printing_newtags_textsize",
        $body
     );

    $body = preg_replace_callback(
        '/\[\[Print:putvertical\|(-*\d*[\.|,]?\d+)(.*?)\]\]/is',
        "printing_newtags_putvertical",
        $body
     );

    $body = preg_replace_callback(
        '/\[\[Print:puthorizontal\|(-*\d*[\.|,]?\d+)(.*?)\]\]/is',
        "printing_newtags_puthorizontal",
        $body
     );

     // Pagination
     $body = str_ireplace('[[Print:pagination]]', '<rawtex>\thepage </rawtex>', $body);

     // Date
     $body = str_ireplace('[[Print:today]]', '<rawtex>\today </rawtex>', $body);

     // Variable
     $body = str_ireplace('[[Print:docnumber]]', '<rawtex>\nsdocno </rawtex>', $body);

     // Noprint tags
     $body = preg_replace("/\[\[Print:noprint\]\](.*?)\[\[Print:\/noprint\]\]/is", "", $body);

     // Prevent hyphenation
     $body = preg_replace("/\[\[Print:nohyphen\]\](.*?)\[\[Print:\/nohyphen\]\]/is", '<rawtex>\mbox{$1}</rawtex>', $body);

     // Stay on the same page
     $body = preg_replace("/\[\[Print:nopagebreak\]\](.*?)\[\[Print:\/nopagebreak\]\]/is", '<rawtex>\begin{samepage}' . "\n" . '$1' . "\n" . '\end{samepage}</rawtex>', $body);

     // New geometry
     $body = preg_replace_callback(
        '/\[\[Print:newgeometry\|(\d+)\]\]/is',
        "printing_newtags_newgeometry",
        $body
     );

    return $body;
}

/*
 * Callback function for colors
 */

function printing_intext_colors_replace($matches) {
    if (strpos($matches[1],'#') === false) {
        module_load_include("inc", "printing", "includes/printing.colortable");
        $style = printing_colortable($matches[1]);
    } else {
        $style = $matches[1];
    }
    $color = explode("#", $style);
    $rgb = printing_hextorgb(trim($color[1]));
    foreach ($rgb as $singlecolor) {
        $colors[] = round($singlecolor/255,2);
    }
    $colorname = preg_replace("/[^0-9]+/", "", $colors[0] . $colors[1] . $colors[2]);
    $command = '<rawtex>\providecolor{' . $colorname . '}{rgb}{' . $colors[0] . ',' . $colors[1] . ',' . $colors[2] . '}\textcolor{' . $colorname . '}{</rawtex>';
    return $command . $matches[2] . '<rawtex>}</rawtex>';
}

/*
 * Callback functions for new tags
 */

function printing_newtags_div($matches) {
    $parts = explode('|', $matches[1]);
    $pos = ($parts[0] == 'absolute' || $parts[0] == 'relative') ? $pos : '';
    $width = "";
    if(strpos($parts[1],"%") !== false) {
        $width = str_replace("%","",$parts[1]);
        $width = round($width / 100, 2);
    } elseif(strpos($parts[1],"px") !== false) {
        $width = str_replace("px","",$parts[1]);
        $width = round($width / 706, 2);
    }
    $align = FALSE;
    if (strtolower($parts[3])) {
        $align = substr(strtolower($parts[3]), 0, 1);
    }
    $txtcolor = printing_hextorgb($parts[4]);
    $bgcolor = printing_hextorgb($parts[5]);
    $parts[6] = $parts[6] ? $parts[6] : '0';
    $padding = $parts[7] ? preg_replace('/[^0-9.]/', '', $parts[7]) : '0';
    $text = $matches[2]; // data in Div
    //$text = str_replace("\r", '\\\\', $text); // add line breaks

    // Do we have one of allowed cases?
    if (($pos == "") && $width && $align) {
        $text  = '<rawtex>\makebox[\textwidth][' . $align . ']{' .
                 ' \definecolor{background}{RGB}{'.$bgcolor['r'].','.$bgcolor['g'].','.$bgcolor['b'].'}'.
                 ' \definecolor{text}{RGB}{'.$txtcolor['r'].','.$txtcolor['g'].','.$txtcolor['b'].'}'.
		 ' \setlength\fboxsep{' . $padding . 'mm}' .
                 ' \setlength\fboxrule{0pt}'.
		 ' \fcolorbox{background}{background}{'.
		 ' \begin{minipage}{' . $width . '\textwidth}'.
		 ' \color{text}</rawtex>' . $text . '<rawtex>\end{minipage}}}</rawtex>';
    } elseif (($pos == '') && !$width && !$align) {
        $text = '<rawtex>\begin{minipage}{\textwidth}</rawtex>' . $text . '<rawtex>\end{minipage}</rawtex>';
    }
    return $text;
}

function printing_newtags_putvertical($matches) {
    $output = '<rawtex>\vspace*{';
    $output .= (is_numeric($matches[1])) ? $matches[1] : 0;
    $output .= (in_array($matches[2], array("em", "in", "mm", "cm"))) ? $matches[2] : "mm";
    return $output . '}</rawtex>';
}

function printing_newtags_puthorizontal($matches) {
    $output = '<rawtex>\hspace*{';
    $output .= (is_numeric($matches[1])) ? $matches[1] : 0;
    $output .= (in_array($matches[2], array("em", "in", "mm", "cm"))) ? $matches[2] : "mm";
    return $output . '}</rawtex>';
}

function printing_newtags_pagination($matches) {
    
}

function printing_newtags_textsize($matches) {
    switch ($matches[1]) {
        case -4:
            $output = '<rawtex>{\tiny </rawtex>';
            break;
        case -3:
            $output = '<rawtex>{\scriptsize </rawtex>';
            break;
        case -2:
            $output = '<rawtex>{\footnotesize </rawtex>';
            break;
        case -1:
            $output = '<rawtex>{\small </rawtex>';
            break;
        case 2:
            $output = '<rawtex>{\large </rawtex>';
            break;
        case 3:
            $output = '<rawtex>{\Large </rawtex>';
            break;
        case 4:
            $output = '<rawtex>{\LARGE </rawtex>';
            break;
        case 5:
            $output = '<rawtex>{\huge </rawtex>';
            break;
        case 6:
            $output = '<rawtex>{\Huge </rawtex>';
            break;
        case 1:
        default:
            $output = '<rawtex>{\normalsize </rawtex>';
            break;
    }
    return $output . $matches[2] . '<rawtex>}</rawtex>';
}

function printing_newtags_newgeometry($matches) {
    $sql = "SELECT printing_geometry.width, printing_geometry.height, printing_geometry.hline, printing_geometry.fline, printing_geometry.m_top,
            printing_geometry.m_right, printing_geometry.m_bottom, printing_geometry.m_left,
            printing_geometry.headsep, printing_geometry.footskip, printing_geometry.headheight, header_left.latex as hl,
            header_center.latex as hc, header_right.latex as hr, footer_left.latex as fl, footer_center.latex as fc, footer_right.latex as fr
            FROM {printing_geometry} printing_geometry
            LEFT JOIN {printing_hf} header_left
                ON header_left.HFID = printing_geometry.def_LH
            LEFT JOIN {printing_hf} header_center
                ON header_center.HFID = printing_geometry.def_CH
            LEFT JOIN {printing_hf} header_right
                ON header_right.HFID = printing_geometry.def_RH
            LEFT JOIN {printing_hf} footer_left
                ON footer_left.HFID = printing_geometry.def_LF
            LEFT JOIN {printing_hf} footer_center
                ON footer_center.HFID = printing_geometry.def_CF
            LEFT JOIN {printing_hf} footer_right
                ON footer_right.HFID = printing_geometry.def_RF
            WHERE printing_geometry.GID = " . $matches[1];
    $options = db_fetch_array(db_query($sql));
    $output = '\pdfpagewidth=' . $options['width'] . ' \pdfpageheight=' . $options['height'] . "\n" .
              '\newgeometry{left=' . $options['m_left'] . ', right=' . $options['m_right'] . ', bottom=' . $options['m_bottom'] . ', top=' . $options['m_top'] . '}' . "\n" .
              '\headheight' . $options['headheight'] . "\n" .
              '\headsep' . $options['headsep'] . "\n" .
              '\footskip' . $options['footskip'] . "\n" .
              '\renewcommand{\headrulewidth}{' . $options['hline'] . '}' . "\n" .
              '\renewcommand{\footrulewidth}{' . $options['fline'] . '}' . "\n" .
              '\fancyhead[L]{' . $options['hl'] . '}' . "\n" .
              '\fancyhead[C]{' . $options['hc'] . '}' . "\n" .
              '\fancyhead[R]{' . $options['hr'] . '}' . "\n" .
              '\fancyfoot[L]{' . $options['fl'] . '}' . "\n" .
              '\fancyfoot[C]{' . $options['fc'] . '}' . "\n" .
              '\fancyfoot[R]{' . $options['fr'] . '}' . "\n";
    return '<rawtex>' . $output . '</rawtex>';
}

/*
 * Afterparser (we need to get back characters between <rawtex> tag
 */

function printing_afterparser ($body) {
    $body = preg_replace_callback(
        "/<rawtex>(.*)<\/rawtex>/isU",
            "printing_afterparser_cbk",
            $body
            );
    return $body;
}

function printing_afterparser_cbk($matches) {
    $output = str_replace(array('\textbackslash '), array('\\\\'), $matches[1]);
    return $output;
}
<?php

function printing_install() {
    drupal_install_schema('printing');
    $icon_path = drupal_get_path("module", "printing");
    //Set default values
    $default_values['printing_paper'] = array(
        array(
            'name'      => 'Default a4',
            'TID'       => -1,
            'def_GID'   => 1
        ),
        array(
            'name'      => 'Default a5',
            'TID'       => -1,
            'def_GID'   => 2
        ),
        array(
            'name'      => 'Default Letter',
            'TID'       => -1,
            'def_GID'   => 3
        )
    );
    $default_values['printing_geometry'] = array(
        array(
            'PID' => 1,
            'name' => 'Plain a4',
            'width'     => '210mm',
            'height'    => '296mm',
            'icon'      => "$icon_path/thumbnails/a4icon.png",
        ),
        array(
            'PID' => 2,
            'name' => 'Plain a5',
            'width'     => '148mm',
            'height'    => '210mm',
            'icon'      => "$icon_path/thumbnails/a5icon.png",
        ),
        array(
            'PID' => 3,
            'name' => 'Plain Letter',
            'width'     => '8.5in',
            'height'    => '11in',
            'm_top'     => '0.5in',
            'm_right'   => '0.5in',
            'm_bottom'  => '0.5in',
            'm_left'    => '0.5in',
            'hline'     => '0in',
            'fline'     => '0in',
            'headsep'   => '0.2in',
            'footskip'  => '0.2in',
            'headheight'  => '0in',
            'units'     => 'in',
            'icon'      => "$icon_path/thumbnails/lettericon.png",
        ),
    );
    $default_values['printing_hf'] = array(
        array(
            'body'      => ''
        ),
    );
    $default_values['printing_colormap'] = array(
        array('color'     => '', 'hex'       => ''),
    );
    _printing_default_recordinsert($default_values);
}

function printing_schema() {
    $icon_path = drupal_get_path("module", "printing");
    $schema['printing_paper'] = array(
        'fields' => array(
            'PID'       => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
            'TID'       => array('type' => 'int', 'unsigned' => FALSE, 'not null' => TRUE, 'default' => 0),
            'name'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE),
            'fontshape' => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => 'Roman'),
            'fontssize' => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '11'),
            'head_1'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '\\\\huge \\\\bfseries'),
            'head_2'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '\\\\LARGE \\\\bfseries'),
            'head_3'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '\\\\Large \\\\bfseries'),
            'head_4'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '\\\\large \\\\bfseries'),
            'head_5'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '\\\\large \\\\itshape \\\\bfseries'),
            'head_6'    => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => '\\\\large'),
            'orphan'    => array('type' => 'int', 'size' => 'tiny', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'facing'    => array('type' => 'int', 'size' => 'tiny', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'def_GID'   => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0)
        ),
        'primary key' => array('PID')
    );
    $schema['printing_geometry'] = array(
      'fields' => array(
            'GID'       => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
            'PID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'name'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE),
            'apply_to'  => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => 'Both'),
            'width'     => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE),
            'height'    => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE),
            'm_top'     => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '10mm'),
            'm_right'   => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '10mm'),
            'm_bottom'  => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '10mm'),
            'm_left'    => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '10mm'),
            'def_LH'    => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 1),
            'def_CH'    => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 1),
            'def_RH'    => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 1),
            'def_LF'    => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 1),
            'def_CF'    => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 1),
            'def_RF'    => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 1),
            'hline'     => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '0mm'),
            'fline'     => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '0mm'),
            'headsep'   => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '5mm'),
            'footskip'  => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '5mm'),
            'headheight'  => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => '0mm'),
            'units'     => array('type' => 'varchar', 'length' => 50, 'not null' => TRUE, 'default' => 'mm'),
            'icon'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
        ),
        'primary key' => array('GID')
    );
    $schema['printing_progress'] = array(
      'fields' => array(
            'PRID'      => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
            'TID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'NID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'PID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'session'   => array('type' => 'varchar', 'length' => 64, 'not null' => TRUE, 'default' => ''),
            'total'     => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'count'     => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
        ),
        'primary key' => array('PRID')
    );
    $schema['printing_hf'] = array(
      'fields' => array(
            'HFID'      => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
            'GID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'body'      => array('type' => 'text', 'not null' => TRUE, 'size' => 'big'),
            'latex'     => array('type' => 'text', 'not null' => TRUE, 'size' => 'big'),
            'icon'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => "$icon_path/thumbnails/emptyhf.png"),
        ),
        'primary key' => array('HFID')
    );
    $schema['printing_files'] = array(
      'fields' => array(
            'FID'       => array('type' => 'serial', 'unsigned' => TRUE, 'not null' => TRUE),
            'PID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'TID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'NID'       => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
            'SID'       => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
            'type'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
            'filename'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
            'filepath'      => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
            'timestamp'      => array('type' => 'int', 'unsigned' => TRUE, 'not null' => TRUE, 'default' => 0),
        ),
        'primary key' => array('FID')
    );
    $schema['printing_colormap'] = array(
      'fields' => array(
            'color'     => array('type' => 'varchar', 'length' => 255, 'not null' => TRUE, 'default' => ''),
            'hex'       => array('type' => 'varchar', 'length' => 7, 'not null' => TRUE, 'default' => '')
          )
    );
    return $schema;
}

function printing_uninstall() { 
    drupal_uninstall_schema('printing');
}

function _printing_default_recordinsert($default_values) {
    foreach ($default_values as $table => $rows) {
        foreach ($rows as $row => $values) {
            $names = array();
            $placeholders = array();
            foreach ($values as $name => $value) {
                $names[] = $name;
                if (is_numeric($value)) {
                    $placeholders[] = "%d";
                } else {
                    $placeholders[] = "'%s'";
                }
            }
            $names = (string) implode(',', $names);
            $placeholders = (string) implode(',', $placeholders);
            $sql = "INSERT INTO {$table} ($names) VALUES ($placeholders)";
            db_query($sql, $values);
        }
    }
}






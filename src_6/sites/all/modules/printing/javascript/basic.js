function poolData() {
   $.ajax({
            url: '/printing/progress',
            success: function(data) {
                if (data != "none") {
                    $("#counting").text("Printing article #" + data['count'] + ' of ' + data['total']);
                }
            },
            dataType: "json",
            cache: false,
            timeout: 2000,
            complete: poolData
    })
}

$(document).ready(function() {
    $('#print-page, #print-document').click(function() {
        $('#basic-modal-content').modal();
        poolData();
    });
});


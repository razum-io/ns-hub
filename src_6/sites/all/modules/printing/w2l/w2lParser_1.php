<?php

/*
 * File: w2lParser.php
 *
 * Purpose:
 * Contains the parser, which transforms Mediawiki-articles to LaTeX
 *
 * License:
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 */


define('W2L_UNDEFINED', 'undefined');

define('W2L_FILE', 1);
define('W2L_STRING', 0);

define('W2L_TEMPLATE', 1);
define('W2L_TRANSCLUSION', 2);
define('W2L_COREPARSERFUNCTION', 3);
define('W2L_PARSERFUNCTION', 4);
define('W2L_VARIABLE', 5);


class Wiki2LaTeXParser {
	function __construct() {
		$this->version = '1.1';
		
		$this->initiated = false;
		$this->doProfiling = true;
		$this->ProfileLog  = array();
		$this->parsing     = '';
		$this->config      = array();
		$this->tags        = array();
		$this->fragments   = array();
		$this->elements    = array();
		$this->rawtex_counter = 0;
		$this->marks_counter = 0;
		$this->nowikiMarks = array();
		$this->nowikiCounter = 0;
		$this->rawtex_replace = array();
		// some default settings
		$this->config["headings_toplevel"] = 'section';
		$this->config["use_hyperref"]      = true;
		$this->config["leave_noinclude"]   = false;
		$this->config["babel"] = 'english';
		$this->tag_source  = array();
		$this->tag_replace = array();
		$this->tags_replace = array();
		$this->preReplace   = array();
		$this->replace_search  = array(
                    '″'
                ); // NEVER set one of these values via another way than by addSimpleReplace
		$this->replace_replace = array(
                    '"'
                );
		$this->ireplace_search  = array(); // NEVER set one of these values via another way than by addSimpleReplace
		$this->ireplace_replace = array();
		$this->regexp_search  = array(); // NEVER set one of these values via another way than by addRegExp
		$this->regexp_replace = array();

		$this->error_msg = array();
		$this->is_error = false;
		// takes parser functions

		$this->curlyBraceDebugCounter = 0;
		$this->curlyBraceLength = 0;


		$this->mw_vars = array();
		$this->content_cache = array();
		// Parserfunctions...
		$this->pFunctions = array(); // takes custom ones (#switch)
		$this->cpFunctions = array(); // takes those without #
		$this->mask_chars = array();

		$this->files_used = false;
		$this->files = array();
		$this->required_packages = array();
		$this->latex_headcode = array();
		
		// Mediawiki-Parser-Vars;
		$this->mLastSection = '';
		$this->mInPre = false;

		// Special-chars array...
		$this->sc = array();

                // Tables array
                $this->tabarr = array();
		
		// For sorting and bibtex:
		$this->run_bibtex = false;
		$this->run_sort   = false;
		$this->debug = array();
		
		$this->multirow_sts = false;
		$this->multirow_count = 0;
	}

	/* Public Functions */

	public function setConfig($cArray) {
		foreach ($cArray as $key=>$value) {
			$this->setVal($key, $value);
		}
		return true;
	}
	public function setVal($key, $value) {
		$this->config[$key] = $value;

		return true;
	}

	public function getVal($key) {
		if ( array_key_exists($key, $this->config) ) {
			return $this->config[$key];
		} else {
			return W2L_UNDEFINED;
		}
	}

	public function addSimpleReplace($search, $replace, $case_sensitive = 1) {
		if ($case_sensitive == 0 ) {
			$this->ireplace_search[]  = $search;
			$this->ireplace_replace[] = $replace;
		} else {
			$this->replace_search[]  = $search;
			$this->replace_replace[] = $replace;
		}
	}
	
	private function doSimpleReplace( $str ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// Here we're replacing.
		$str = str_replace($this->replace_search, $this->replace_replace, $str);
		$str = str_ireplace($this->ireplace_search, $this->ireplace_replace, $str);
		$this->profileOut($fName);
		return $str;
	}

	public function addTagCallback($tag, $callback) {
		$this->tags[$tag] = $callback;
		$this->elements[] = $tag;
	}

	public function addParserFunction($tag, $callback) {
		$this->pFunctions[$tag] = $callback;
	}

	public function addCoreParserFunction($tag, $callback) {
		$this->addParserFunction($tag, $callback);
	}

	public function addRegExp($search, $replace) {
		$this->regexp_search[]  = $search;
		$this->regexp_replace[] = $replace;
	}

	public function recursiveTagParse( $str = '' ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$str = $this->internalParse($str);
		$this->profileOut($fName);
		return $str;
	}

        private function documentSettings ($settings) {
            $this->documentSettings = $settings;
        }

	public function parse($text, &$title, $mode = W2L_STRING, $my_params = array()) {
		
		$this->profileIn(__METHOD__);
		
		/* parse a given wiki-string to latex */
		/* if $transclusions is an array, then all transcluded files are in there */
		$time_start = microtime(true);
		

		if ($this->initiated == false ) {
			$this->initParsing();
		}
		
		$this->mTitle =& $title;

                $this->documentSettings = $my_params;

		$text = trim($text);
		
		$text = "\n".$text."\n";

                // Replace \r by \r\\ (do row breaks)
                $text = $this->replaceRowbreaks($text);

		//wfRunHooks('w2lBeginParse', array( &$this, &$text ) );

		//wfRunHooks('w2lBeforeCut', array( &$this, &$text ) );
		$text = $this->preprocessString($text);

		
		// First, strip out all comments...
		//wfRunHooks('w2lBeforeStrip', array( &$this, &$text ) );
		$text = $this->stripComments($text);
		
		//wfRunHooks('w2lBeforeExpansion', array( &$this, &$text ) );

		switch ( $this->getVal('process_curly_braces') ) {
			case '0': // remove everything between curly braces
				$text = preg_replace('/\{\{(.*?)\}\}/sm', '', $text);
			break;
			case '1': // do nothing
			break;
			case '2': // process them
				$text = $this->processCurlyBraces($text);
			break;
			default:
			break;
		}
		
		//$this->reportError($text, __METHOD__);
		//wfRunHooks("w2lBeforeExtractTags", array( &$this, &$text ) );
		$text = $this->extractParserExtensions($text);
		
		$text = $this->extractPre($text);
		//echo $text;
		
		//wfRunHooks("w2lBeforeInternalParse", array( &$this, &$text ) );
		
		$text = $this->internalParse($text);
		//echo "<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
		//echo $text;

		$text = trim($text);
		// Some tidying
		$text = str_replace("\n\n\n", "\n\n", $text);
		$text = trim(str_replace("\n\n\n", "\n\n", $text));
		// replace Extensions

		$text = $this->replacePre($text);
		$text = $this->replaceParserExtensions($text);
		$text = $this->replaceNoWikiMarkers($text);
		
		$text = $this->deMask($text);
		//$text = $this->replacePre($text);
		$text = trim($text);
		$text = str_replace("\n\n\n", "\n\n", $text);
		//wfRunHooks("w2lFinish", array( &$this, &$text ) );

		$time_end = microtime(true);
		$this->parse_time = $time_end - $time_start;
		$this->profileOut(__METHOD__);

		return $text;
	}

	function addChar($html, $latex, $utf_dec = false, $req_package = false) {
		if ($utf_dec === false ) {
			$ent_dec = '';
			$ent_hex = '';
		} else {
			$ent_dec = '&#'.$utf_dec.';';
			$ent_hex = '&#x'.dechex($utf_dec).';';
		}

		$this->htmlEntities[] = array(
			'html'    => $html,
			'utf_hex' => $ent_dec,
			'utf_dec' => $ent_hex,
			'latex'   => $latex,
			'xetex'   => '', // Future
			'required_package' => $req_package
		);
		return true;
	}

	function processHtmlEntities( $str ) {
		foreach($this->htmlEntities as $entity ) {
			$entity['html']    = str_replace('&', $this->Et, $entity['html']);
			$entity['utf_hex'] = str_replace('&', $this->Et, $entity['utf_hex']);
			$entity['utf_dec'] = str_replace('&', $this->Et, $entity['utf_dec']);
			
			if ($entity['required_package'] != false ) {
				if ( strpos($str, $entity['html']) !== false ) {
					$this->addPackageDependency($entity['required_package']);
				}
			}
			
			$str = strtr($str, array($entity['html'] => $entity['latex']));

			if ( $entity['utf_hex'] != '' ) {
				if ($entity['required_package'] != false ) {
					if ( strpos($str, $entity['utf_hex']) !== false ) {
						$this->addPackageDependency($entity['required_package']);
					}
				}
				
				$str = strtr($str, array($entity['utf_hex'] => $entity['latex']));
				$str = strtr($str, array($entity['utf_dec'] => $entity['latex']));
			}

			unset($entity);
		}

		return $str;
	}

        function tempTabReplace($matches) {
            $thistable = $this->getMark('Thistable');
            $this->tabarr[$thistable] = $matches[0];
            return $thistable;
        }

        function rawTexReplace($matches) {
            $thisrawtex = $this->getMark('Rawtex');
            $this->mask($thisrawtex, $matches[1]);
            return $thisrawtex;
        }

        function replacePairTags($body) {

            // Regex - function pairs
            $pairs = array(
            "bold" => array (
                "regex" => "/'''(()|[^'].*)'''/iU",
                "function" => "return '" . '<rawtex>{\bfseries</rawtex>' . " '" . ' . $matches[1] . ' . "'" . '<rawtex>}</rawtex>' . "';",
            ),
            "italics" => array (
                "regex" => "/''(()|[^'].*)''/iU",
                "function" => "return '" . '<rawtex>{\itshape</rawtex>' . " '" . ' . $matches[1] . ' . "'" . '<rawtex>}</rawtex>' . "';",
            ),
            "dolars" => array (
                "regex" => '/([^\\\\]|^|^<rawtex>)\$(()|[^\$].*)([^\\\\])\$(?!<\/rawtex>)/iU',
                "function" => "return " . '$matches[1]' . " . '" . '<rawtex>$' . "'" . ' . $matches[2] . $matches[4] . ' . "'" . '$</rawtex>' . "'" . ';'
            ),
            "dolarsanity" => array (
                "regex" => '/(.)(?<!\\\\|\<rawtex\>)\$(?!\<\/rawtex\>)/iU',
                "function" => 'return $matches[1]' . " . '" . '\$' . "';"
            )
        );
        foreach ($pairs as $pairname => $data) {
            $body = preg_replace_callback(
                $data["regex"],
                create_function(
                    '$matches',
                    $data["function"]
                ),
                $body
            );
        }
        return $body;
        }

        function startNewrowReplace ($matches) {
            $slashs = preg_replace('#\s#', '', strip_tags($matches[1]));
            $spaces = strspn($slashs, '\\\\' );
            if ($spaces >= 1) {
                 $spaces = round($spaces/2);
                 return '<rawtex>\vspace{' . $spaces . ' em}</rawtex>' . "\n";
            } else {
                 return '';
            }
        }

        /*
         * Create paragraphs, new rows and sanity new rows on beginning of row
         */

        function replaceRowbreaks($str) {
            // avoid end of row on begginning of row
            $str = preg_replace_callback(
                '#^\s*((\s*<rawtex>(\\\\\\\\)<\/rawtex>\s*)+)#msi',
                array($this, "startNewrowReplace"),
                $str
            );

            // First sane already done math environments and TeX, then sane dollars and do it again for new math
            $str = preg_replace_callback(
                '/<rawtex>(.*)<\/rawtex>/isU',
                array($this, "rawTexReplace"),
                $str
            );

            $str = $this->replacePairTags($str);

            $str = preg_replace_callback(
                '/<rawtex>(.*)<\/rawtex>/isU',
                array($this, "rawTexReplace"),
                $str
            );
            
            // avoid tables
            $str = preg_replace_callback (
                '#^\{\|(.*?)(?:^\|\+(.*?))?(^(?:((?R))|.)*?)^\|}#msi',
                array($this, "tempTabReplace"),
                $str
            );

            $rows = explode("\n", $str);
            $str = "";
            $endofrow = $this->getMark('LF');
            $this->sc['endofrow'] = $endofrow;
            $this->mask($endofrow, '\\\\');
            $previous = FALSE;
            for ($i = 0; $i < count($rows); $i++) {
                $rows[$i] = trim($rows[$i]);
                $addrow = FALSE;
                if (($rows[$i] != "") && // If row is not empty
                   (substr($rows[$i], 0, 1) != '*') && // If row is not in unordered list
                   (substr($rows[$i], 0, 1) != '#') && // If row is not in ordered list
                   (substr($rows[$i], 0, 1) != ';') && // If row is not in ordered list
                   (substr($rows[$i], 0, 1) != ':') && // If row is not in ordered list
                   ((substr($rows[$i], -1) != ']') || (strpos($rows[$i], '[http:') !== false)) && // If row is not tag, except link
                   ((substr($rows[$i], 0, 1) != '=') && (substr($rows[$i], -1) != '=')) && // If row is not title
                   (substr($rows[$i], -6) != 'QINU))')) { // If row isn't placeholder
                    $addrow = TRUE;
                }
                if ($previous && $addrow) {
                    $str .= $rows[$i - 1] . $endofrow . "\n";
                } elseif ($i > 0) {
                    $str .= $rows[$i - 1] . "\n";
                }
                $previous = $addrow;
            }
            if ($rows[$i] != "") {
                $str .= $rows[$i] . "\n";
            }
            unset($rows);
            unset($mymarks);
            $str = str_replace(array_keys($this->tabarr), array_values($this->tabarr), $str);
            $this->tabarr = array();
            return "\n" . trim($str) . "\n";
        }

	function internalParse($str) {
		
		$this->profileIn(__METHOD__);

		// Used for parsing the string as is, without comments, extension-tags, etc.
		
		$str = $this->doSimpleReplace($str);
		//code to open
		//$str = $this->doInternalLinks($str);//orignal position but moved after doTableStuff($str)
		$str = $this->doExternalLinks($str);
		
		//wfRunHooks('w2lBeforeMask', array( &$this, &$str ) );
		$str = $this->maskLatexCommandChars($str);
		
		// Now we can begin parsing. We parse as close as possible the way mediawiki parses a string.
		// So, start with tables
		
		//wfRunHooks('w2lBeforeTables', array( &$this, &$str ) );
		$str = $this->doTableStuff($str);
		
		//shifted from above
		$str = $this->doInternalLinks($str);

		// Next come these Blocklevel elments
		// Now go on with headings

		$str = $this->doHeadings($str);

		$str = $this->doQuotes($str);

		$str = $this->doHTML($str);
		$str = $this->doQuotationMarks($str);

		$str = $this->maskLatexSpecialChars($str);
		$str = $this->doSpecialChars($str);
		$str = $this->processHtmlEntities($str);
		$str = $this->maskLaTeX($str);
		$str = $this->doBlocklevels($str);
		$str = $this->maskMwSpecialChars($str);
		
		$str = $this->doDivAndSpan($str, 'span');
		$str = $this->doDivAndSpan($str, 'div');
		
		$str = $this->doSimpleReplace($str);


		//wfRunHooks('w2lInternalFinish', array( &$this, &$str ) );

		$this->profileOut(__METHOD__);
		return $str;
	}

	public function getPerformanceProfile($export_as = 'xml') {
		if ( !$this->doProfiling ) {
			return false;
		}
		switch ($export_as) {
			case 'array':
				return $this->ProfileLog;
			break;
			case 'xml':
				$xml_return = "";
				foreach($this->ProfileLog AS $func_call) {
					$xml_return .= '<'.$func_call['type'].' fname="'.$func_call['function'].'" time="'.$func_call['time'].'" />'."\n";
				}
				return $xml_return;
			break;
			default:
				return false;
			break;
		}
	}

	public function getParseTime() {
		return $this->parse_time;
	}

	private function doQuotationMarks($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);

		switch ( $this->getVal('babel') ) {
			case 'english':			
				$quotes = array(
					'"' => '"', // "
					"'" => "'", // '
				);
			break;
			case 'german': // using switch-fallthough here...
			case 'ngerman':
				$quotes = array(
					'"' => '\dq{}', // "
					"'" => '\rq{}', // '
				);
			break;
			default:
				$quotes = array(
					'"' => '"', // "
					"'" => "'", // '
				);
			break;		
		}

		$str = strtr($str, $quotes);

		$this->profileOut($fName);
		return $str;
	}
	/* Internal parsing functions */
	public function initParsing() {
		$fName = __METHOD__;
		$this->profileIn($fName);
		global $w2lTags;
		global $w2lParserFunctions;
		global $w2lConfig;

		if ($this->initiated == true ) {
			return;
		}
		
		//wfRunHooks('w2lInitParser', array(&$this));
		
		$this->unique = $this->uniqueString();
		
		/*foreach($w2lTags as $key => $value) {
			$this->addTagCallback($key, $value);
		}

		foreach($w2lParserFunctions as $key => $value) {
			$this->addParserFunction($key, $value);
		}

		foreach($w2lConfig as $key => $value) {
			$this->setVal($key, $value);
		}*/

		//$this->addCoreParserFunction();
		/*$this->addCoreParserFunction( 'int', array( 'CoreParserFunctions', 'intFunction' ) );
		$this->addCoreParserFunction( 'ns', array( 'CoreParserFunctions', 'ns' )  );
		$this->addCoreParserFunction( 'urlencode', array( 'CoreParserFunctions', 'urlencode' )  );
		$this->addCoreParserFunction( 'lcfirst', array( 'CoreParserFunctions', 'lcfirst' )  );
		$this->addCoreParserFunction( 'ucfirst', array( 'CoreParserFunctions', 'ucfirst' )  );
		$this->addCoreParserFunction( 'lc', array( 'CoreParserFunctions', 'lc' )  );
		$this->addCoreParserFunction( 'uc', array( 'CoreParserFunctions', 'uc' )  );
		$this->addCoreParserFunction( 'localurl', array( 'CoreParserFunctions', 'localurl' )  );
		$this->addCoreParserFunction( 'localurle', array( 'CoreParserFunctions', 'localurle' )  );
		$this->addCoreParserFunction( 'fullurl', array( 'CoreParserFunctions', 'fullurl' )  );
		$this->addCoreParserFunction( 'fullurle', array( 'CoreParserFunctions', 'fullurle' )  );
		//$this->addCoreParserFunction( 'formatnum', array( 'CoreParserFunctions', 'formatnum' )  );
		//$this->addCoreParserFunction( 'grammar', array( 'CoreParserFunctions', 'grammar' )  );
		//$this->addCoreParserFunction( 'plural', array( 'CoreParserFunctions', 'plural' )  );
		$this->addCoreParserFunction( 'numberofpages', array( 'CoreParserFunctions', 'numberofpages' )  );
		$this->addCoreParserFunction( 'numberofusers', array( 'CoreParserFunctions', 'numberofusers' )  );
		$this->addCoreParserFunction( 'numberofarticles', array( 'CoreParserFunctions', 'numberofarticles' )  );
		$this->addCoreParserFunction( 'numberoffiles', array( 'CoreParserFunctions', 'numberoffiles' )  );
		$this->addCoreParserFunction( 'numberofadmins', array( 'CoreParserFunctions', 'numberofadmins' )  );
		$this->addCoreParserFunction( 'language', array( 'CoreParserFunctions', 'language' )  );
		$this->addCoreParserFunction( 'padleft', array( 'CoreParserFunctions', 'padleft' )  );
		$this->addCoreParserFunction( 'padright', array( 'CoreParserFunctions', 'padright' )  );
		$this->addCoreParserFunction( 'anchorencode', array( 'CoreParserFunctions', 'anchorencode' )  );
		$this->addCoreParserFunction( 'special', array( 'CoreParserFunctions', 'special' ) );
		//$this->addCoreParserFunction( 'defaultsort', array( 'CoreParserFunctions', 'defaultsort' )  );
		$this->addCoreParserFunction( 'pagesinnamespace', array( 'CoreParserFunctions', 'pagesinnamespace' ) );*/

		// And here we add some replace-rules
		$this->addSimpleReplace(" - "," -- ");
		$this->addSimpleReplace(" -\n"," --\n");
		$this->addSimpleReplace("\n- ", "\n-- ");
		
		include('w2lChars.php');
		include('w2lQuotes.php');
		
		//wfRunHooks('w2lInitParserFinish', array(&$this));

		$this->initiated = true;
		$this->profileOut($fName);
		return;
	}
	
	function doSpecialChars($str) {

		$chars = array(
			"…"=>"{\dots}",
			"…"=>"{\dots}",
			'~'=> '\textasciitilde',
			'€'=> '{\euro}',
			'&'=> '{\&}',
		);

		if ( strpos($str, '€') !== false ) {
			$this->addPackageDependency('eurosym');
		}

		$str = strtr($str, $chars);
		return $str;
	}

	function getFirstChar($str) {
		if ( strlen($str) == 0 ) {
			return '';
		} else {
			return $str{0};
		}
	}

	function extractPre($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$work_str = explode("\n", $str);
		$final_str = '';
		$debug = '';
		$pre_line = false;
		$block_counter = 0;

		$rplBlock = array();
		$preBlock = array();

		foreach($work_str as $line) {
			//wfVarDump($line);
			// every line is here, now check for a blank at first position
			// old code gives a notice on empty line:
			
			$first_char = $this->getFirstChar($line);

			$last_line = $pre_line;
			if ( ' ' == $first_char ) {
				if ($last_line == true) {

				} else {
					++$block_counter;
					$preBlock[$block_counter] = "";
				}

				$rpl_line = substr($line, 1);
				$preBlock[$block_counter] .= $rpl_line."\n";
				/*if ($this->getVal('debug') == true) {
					$preBlock[$block_counter] .= $rpl_line."\n"; // original-line
				} else {
					@$preBlock[$block_counter] .= $rpl_line."\n"; // The @ was added to suppress a notice...
				}*/
				$pre_line = true;
				$debug .= '1';
				$work_line = '';
			} else {
				$work_line = $line."\n";
				// check, wether last line was true, so we can create a block
				if ($last_line == true) {
					if ( trim($preBlock[$block_counter]) == "" ) {
						$work_line = $preBlock[$block_counter].$work_line;
					} else {
						$preBlockX = "\begin{verbatim}\n".$preBlock[$block_counter]."\end{verbatim}\n";
					
						//$work_line = $preBlock[$block_counter];
						//
						// originale Zeilen, latex-zeilen, marker,
						//
					
						//if ( $preBlock[$block_counter] ==  )
						$marker = $this->getMark('pre', $block_counter);
						$work_line = $marker.$work_line;
						//wfVarDump($str);
						//$str = str_replace($rplBlock[$block_counter], $marker, $str);
						//wfVarDump($str);
						//echo $preBlockX."<br />";
						$this->preReplace[$marker] = $preBlockX;
					}
				}

				$pre_line = false;
				$debug .= '0';
			}
			//$debug .= $pre_line;
			//wfVarDump($work_line);
			$final_str .= $work_line;
		}
		
		//wfVarDump($preBlock);
		//print "<pre>";
		//print_r($this->preReplace);
		//echo "<br /><br /><br /><br /><br /><br /><br />";
		$this->profileOut($fName);
		return $final_str;
	}

	function replacePre($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$str = str_replace(array_keys($this->preReplace), array_values($this->preReplace), $str);
		$this->profileOut($fName);
		return $str;
	}

	function matchNoWiki($str) {
		//
		$str = preg_replace_callback('/<nowiki>(.*)<\/nowiki>/smU', array($this,'noWikiMarker'), $str);
		return $str;
	}

	function noWikiMarker($match) {
		//
		++$this->nowikiCounter;
		$marker = $this->getMark('nowiki', $this->nowikiCounter);
		$str = $this->maskLatexCommandChars($match[1]);
		$str = $this->maskLatexSpecialChars($str);
		$str = $this->maskMwSpecialChars($str);
		$this->nowikiMarks[$marker] = $str;
		return $marker;
	}

	function replaceNoWikiMarkers($str) {
		//
		$str = strtr($str, $this->nowikiMarks);
		return $str;
	}

	public function preprocessString($str) {
		//$this->reportError(strlen($str), __METHOD__);
		$str = $this->matchNoWiki($str);
		$str = $this->stripComments($str);
		//$this->reportError(strlen($str), __METHOD__);
		if ( $this->getVal('leave_noinclude') ) {
			$str = preg_replace('/<noinclude>(.*)<\/noinclude>/smU', "$1", $str);
			$this->setVal('leave_noinclude', false);
		} else {
			$str = preg_replace('/<noinclude>.*<\/noinclude>/smU', '', $str);
		}

		if ( $this->getVal('insert_includeonly') ) {
			$str = preg_replace('/<includeonly>(.*)<\/includeonly>/smU', "$1", $str);
		} else {
			$str = preg_replace('/<includeonly>(.*)<\/includeonly>/smU', '', $str);
			$this->setVal('insert_includeonly', true);
		}

		//$this->reportError(strlen($str), __METHOD__);

		//wfRunHooks('w2lPreProcess', array( &$this, &$str ) );
		//$this->reportError(strlen($str), __METHOD__);
		return $str;
	}

	private function doBlockLevels( $str = '' ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$fname=__METHOD__;
		$text = $str;
		$linestart = true;
		# Parsing through the text line by line.  The main thing
		# happening here is handling of block-level elements p, pre,
		# and making lists from lines starting with * # : etc.
		#
		$textLines = explode( "\n", $text );

		$lastPrefix = $output = '';
		$this->mDTopen = $inBlockElem = false;
		$prefixLength = 0;
		$paragraphStack = false;

		if ( !$linestart ) 
		{
			$output .= array_shift( $textLines );
		}
		//echo "<br/><br/>".$output;
		foreach ( $textLines as $oLine ) 
		{			
			//echo "<br/><br/>".$oLine;
			$lastPrefixLength = strlen( $lastPrefix );
			$preCloseMatch = preg_match('/<\\/pre/i', $oLine );
			$preOpenMatch = preg_match('/<pre/i', $oLine );
			if ( !$this->mInPre ) 
			{
				//echo "<br/><br/>inside if ";
				# Multiple prefixes may abut each other for nested lists.
				$prefixLength = strspn( $oLine, '*#:;' );
				$pref = substr( $oLine, 0, $prefixLength ); // Prefix

				# eh?
				$pref2 = str_replace( ';', ':', $pref );
				$t = substr( $oLine, $prefixLength ); // Rest of row
				$this->mInPre = !empty($preOpenMatch);
			} 
			else 
			{
				# Don't interpret any other prefixes in preformatted text
				$prefixLength = 0;
				$pref = $pref2 = '';
				$t = $oLine;
			}
			//echo "<br /> loop ".$oLine;

			# List generation
			if( $prefixLength && 0 == strcmp( $lastPrefix, $pref2 ) ) 
			{
				# Same as the last item, so no need to deal with nesting or opening stuff
				//echo "<br />inside if ".substr( $pref, -1 );
				$output .= $this->nextItem( substr( $pref, -1 ) );
				$paragraphStack = false;

				if ( substr( $pref, -1 ) == ';') 
				{
					# The one nasty exception: definition lists work like this:
					# ; title : definition text
					# So we check for : in the remainder text to split up the
					# title and definition, without b0rking links.
					$term = $t2 = '';
					if ($this->findColonNoLinks($t, $term, $t2) !== false) 
					{
						$t = $t2;
						$output .= $term . $this->nextItem( ':' );
					}
				}
			} 
			elseif( $prefixLength || $lastPrefixLength ) 
			{
				# Either open or close a level...
				$commonPrefixLength = $this->getCommon( $pref, $lastPrefix );
				$paragraphStack = false;
				
				while( $commonPrefixLength < $lastPrefixLength ) 
				{
					//echo "<br />inside elseif closing ";
					$output .= $this->closeList( $lastPrefix{$lastPrefixLength-1} );
					--$lastPrefixLength;
				}
				
				if ( $prefixLength <= $commonPrefixLength && $commonPrefixLength > 0 ) 
				{
					//echo "<br />inside elseif ".$pref{$commonPrefixLength-1};
					$output .= $this->nextItem( $pref{$commonPrefixLength-1} );
				}
				//mansoor
				if(($prefixLength-$commonPrefixLength)>1)
				{
					while ( $prefixLength > $commonPrefixLength ) 
					{
						$char = substr( $pref, $commonPrefixLength, 1 );
						$tmp = str_replace("\\item ","",$this->openList( $char ));
						$output .= $tmp;
						//echo "<br />inside elseif WHILE ".$char;
						//echo "<br />inside elseif WHILE prefixLength - commonPrefixLength : ".$prefixLength."-".$commonPrefixLength;
						if ( ';' == $char ) 
						{
							# FIXME: This is dupe of code above
							if ($this->findColonNoLinks($t, $term, $t2) !== false) 
							{
								$t = $t2;
								//echo "<br />inside elseif WHILE ifif ".$char;
								$output .= $term . $this->nextItem( ':' );
							}
						}
						++$commonPrefixLength;
					}
					$output .= "\\item ";

				}
				else
				{
					while ( $prefixLength > $commonPrefixLength ) 
					{
						$char = substr( $pref, $commonPrefixLength, 1 );
						$tmp = $this->openList( $char );//str_replace("\\item","",$this->openList( $char ));
						$output .= $tmp;
						//echo "<br />inside elseif WHILE ".$char;
						//echo "<br />inside elseif WHILE prefixLength - commonPrefixLength : ".$prefixLength."-".$commonPrefixLength;
						if ( ';' == $char ) 
						{
							# FIXME: This is dupe of code above
							if ($this->findColonNoLinks($t, $term, $t2) !== false) 
							{
								$t = $t2;
								//echo "<br />inside elseif WHILE ifif ".$char;
								$output .= $term . $this->nextItem( ':' );
							}
						}
						++$commonPrefixLength;
					}
				}
				//mansoor
				$lastPrefix = $pref2;
			}
			// somewhere above we forget to get out of pre block (bug 785)
			if($preCloseMatch && $this->mInPre) {
				$this->mInPre = false;
			}
			if ($paragraphStack === false) {
				$output .= $t."\n";
			}
			//echo "<br /><br />";
		}
		while ( $prefixLength ) {
			$output .= $this->closeList( $pref2{$prefixLength-1} );
			--$prefixLength;
		}
		if ( '' != $this->mLastSection ) {
			$output .= '</' . $this->mLastSection . '>';
			$this->mLastSection = '';
		}
		$this->profileOut($fName);
		//echo "<br />output ".$output;
		return $output;

	}

	/* private */ function nextItem( $char ) 
	{
		//echo "<br /> nextItem ".$char;
		if ( '*' == $char || '#' == $char ) 
		{ 
			//mansoor
			//return '\vspace{-3mm}\item ';
			return '\item ';
			//mansoor
		}
		else if ( ':' == $char || ';' == $char ) 
		{
			$close = '\\\\';
			if ( $this->mDTopen ) 
			{ 
				$close = ']  \hfill \\\\';
				//echo "<br/><br/>close<br/>";
			}
			if ( ';' == $char ) 
			{
				$this->mDTopen = true;
				return '\item[ ';
				//return '\item ';
				//echo "<br/><br/>;<br/>";
			} 
			elseif ( ':' == $char ) 
			{
				$this->mDTopen = false;
				return $close;//.'\item ';
				//echo "<br/><br/>:<br/>";
			} 
			else 
			{
				$this->mDTopen = false;
				return $close . '';
				//echo "<br/><br/>empty<br/>";
			}
		}
		return '<!-- ERR 2 -->';
	}
	/* private */ function closeParagraph() {
		$result = '';
		if ( '' != $this->mLastSection ) {
			$result = '</' . $this->mLastSection  . ">\n";
		}
		$this->mInPre = false;
		$this->mLastSection = '';
		return $result;
	}
	/* private */ function openList( $char ) {
		$list_ul_env = 'itemize';
		$list_ol_env = 'enumerate';	 
		//wfRunHooks('w2lParseLists', array(&$this, &$list_ul_env, &$list_ol_env) );
	   
		$result = $this->closeParagraph();

		if ( '*' == $char ) 
		{ 
			$result .= '\begin{'.$list_ul_env.'} \itemsep 0pt \parskip 0pt'."\n".'\item ';
		}
		else if ( '#' == $char ) 
		{ 
			$result .= '\begin{'.$list_ol_env.'} \itemsep 0pt \parskip 0pt'."\n".'\item ';
		}
		else if ( ':' == $char ) 
		{ 
			$result .= "\begin{description}\n\item ";
		}
		else if ( ';' == $char ) 
		{
			//$result .= "\begin{description}\n\item[";
			$result .= "\begin{description}\n\item ";
			$this->mDTopen = true;
		}
		else 
		{ 
			$result = '<!-- ERR 1 -->'; 
		}

		return $result;
	}
	/* private */ function closeList( $char ) {
		$list_ul_env = 'itemize';
		$list_ol_env = 'enumerate';	 
		//wfRunHooks('w2lParseLists', array(&$this, &$list_ul_env, &$list_ol_env) );

		if ( '*' == $char ) { $text = '\end{'.$list_ul_env.'}'; }
		else if ( '#' == $char ) { $text = '\end{'.$list_ol_env.'}'; }
		else if ( ':' == $char ) {
			if ( $this->mDTopen ) {
				$this->mDTopen = false;
				$text = "]\n\end{description}";
			} else {
				$text = "\n\end{description}";
			}
		}
		else {	return '<!-- ERR 3 -->'; }
		return $text."\n";
	}
	/* private */ function getCommon( $st1, $st2 ) {
		$fl = strlen( $st1 );
		$shorter = strlen( $st2 );
		if ( $fl < $shorter ) { $shorter = $fl; }

		for ( $i = 0; $i < $shorter; ++$i ) {
			if ( $st1{$i} != $st2{$i} ) { break; }
		}
		return $i;
	}

	/**
	 * Split up a string on ':', ignoring any occurences inside tags
	 * to prevent illegal overlapping.
	 * @param string $str the string to split
	 * @param string &$before set to everything before the ':'
	 * @param string &$after set to everything after the ':'
	 * return string the position of the ':', or false if none found
	 */
	function findColonNoLinks($str, &$before, &$after) {
		$fname = 'Parser::findColonNoLinks';
		//wfProfileIn( $fname );

		$pos = strpos( $str, ':' );
		if( $pos === false ) {
			// Nothing to find!
			//wfProfileOut( $fname );
			return false;
		}

		$lt = strpos( $str, '<' );
		if( $lt === false || $lt > $pos ) {
			// Easy; no tag nesting to worry about
			$before = substr( $str, 0, $pos );
			$after = substr( $str, $pos+1 );
			//wfProfileOut( $fname );
			return $pos;
		}
	}

	private function doHeadings( $str = '' ) {
		$this->profileIn(__METHOD__);
		// Here we're going to parse headings
		// Without support for \part. Needs to be implemented seperately...
		// Method from mediawiki
		for ( $i = 6; $i >= 1; --$i ) {
			$h = str_repeat( '=', $i );
			$str = preg_replace( "/^{$h}(.+){$h}\\s*$/m", "<h{$i}>\\1</h{$i}>\\2", $str );
		}

		//$pr_match = ;
		$str = preg_replace_callback('^\<h([1-6])\>(.+)\</h([1-6])\>^', array($this, 'processHeadings'), $str);
		//$str = str_ireplace($headings_html, $headings_latex, $str);

		$this->profileOut(__METHOD__);
		return $str;
	}

	private function processHeadings($matches) {
		$text = trim($matches[2]);
                $toc = FALSE;
                if (substr($text, 0, 14) == '<privatetitle>') {
                    $text = substr($text, 14);
                    $toc = TRUE;
                }
		$level = trim($matches[1]);
		switch ($level) {
                    case 1: $heading = '\headerone{' . $text . '}';
                            if ($toc) {
                                $heading .= ' \addcontentsline{toc}{part}{' . $text . '}';
                            }
                    break;

                    case 2: $heading = '\headertwo{' . $text . '}';
                            if ($toc) {
                                $heading .= ' \addcontentsline{toc}{section}{' . $text . '}';
                            }
                    break;

                    case 3: $heading = '\headerthree{' . $text . '}';
                            if ($toc) {
                                $heading .= ' \addcontentsline{toc}{subsection}{' . $text . '}';
                            }
                    break;

                    case 4: $heading = '\headerfour{' . $text . '}';
                            if ($toc) {
                                $heading .= ' \addcontentsline{toc}{subsubsection}{' . $text . '}';
                            }
                    break;

                    case 5: $heading = '\headerfive{' . $text . '}';
                            if ($toc) {
                                $heading .= ' \addcontentsline{toc}{paragraph}{' . $text . '}';
                            }
                    break;

                    case 6: $heading = '\headersix{' . $text . '}';
                            if ($toc) {
                                $heading .= ' \addcontentsline{toc}{subparagraph}{' . $text . '}';
                            }
                    break;
                }
		return $heading . "\n";
	}

	function mask($key, $value) {
		$this->mask_chars[$key] = $value;
	}

	function deMask($str) {
		/*echo "<pre>";
		print_r($this->mask_chars);*/
		$str = str_replace(array_keys($this->mask_chars), array_values($this->mask_chars), $str);
		return $str;
	}

	private function doInternalLinks( $str = '' ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$str = preg_replace_callback('/\[\[(.*?)\]\]/', array($this, 'internalLinkHelper'), $str);
		$this->profileOut($fName);
		return $str;
	}

	function doImageLinks($str='')
	{
		$fName = __METHOD__;
		$this->profileIn($fName);
		$str = $this->tableImageHelper($str);
		$this->profileOut($fName);
		return $str;
	}
	function tableImageHelper($image)
	{
		$image = trim($image);
		$link_tmp = explode("|", $image, 2);
		$link_int  = $link_tmp[0];
		$link_text = ( isset($link_tmp[1]) ) ? $link_tmp[1] : $link_tmp[0];
		
		$title = $image;
		
		$parts = explode("|", $image);
		$imagename = array_shift($parts);
		$case_imagename = $imagename;
		// still need to remove the Namespace:
		$tmp_name = explode(':', $imagename, 2);
		$imagename = $tmp_name[1];
		$caption = "";
		$imgwidth = "10cm";
		foreach ($parts as $part) 
		{
				if (preg_match("/\d+px/", $part)) continue;

				if (preg_match("/(\d+cm)/", $part, $widthmatch)) 
				{
					$imgwidth = $widthmatch[1];
					continue;
				}

				if (preg_match("/thumb|thumbnail|frame/", $part)) continue;
				if (preg_match("/left|right|center|none/", $part)) continue;

				$caption = trim($part);
		}
		
		$title = str_replace("|[[Image:", "", $title);
		$title = str_replace("|[[image:", "", $title);
		$res = explode("|",$title);
		$img =  $res[0];
		
		$resize_img = $res[6];
		$res_txt = '';
		if(!empty($resize_img))
		{
			$resize_img = preg_replace('/[^0-9]/', '',$resize_img);
			if(strlen($resize_img)>0)
			{
				$resize_img = $resize_img*0.026458333;
				$res_txt = ' \\resizebox{'.$resize_img.'cm}{!}';
			}
		}
		
		$img1 = explode(" ",$img);
		$imagepath = $img1[0];
		//echo " img path ".$imagepath;
		$title1 = explode("|",$image);//$file->getTitle()->getText();
		/*print('<pre>');
		print_r($title1);*/
		$caption = $title1[1];
		$graphic_package = 'graphicx';
		//$graphic_command = "\\begin{center} \\resizebox{".$imgwidth."}{!}{\includegraphics{{$imagepath}}} \\textit{{$caption}}\end{center}\n";
		//$graphic_command = " \\resizebox{".$imgwidth."}{!}{\includegraphics{{$imagepath}}} \n";
		$graphic_command = $res_txt."{\includegraphics{{$imagepath}}} \n";
		//$graphic_command = "\\resizebox{".$imgwidth."}{!}{\includegraphics{{$imagepath}}} \n";
		//wfRunHooks('w2lImage', array(&$this, &$file, &$graphic_package, &$graphic_command, &$imagepath, &$imagename, &$imgwith, &$caption));
	
		$this->addPackageDependency($graphic_package);
		$masked_command = $this->getMark($graphic_package);
		$this->mask($masked_command, $graphic_command);
		return $masked_command;
	}

	private function internalLinkHelper($matches) { //Images and links
            $matches[1] = trim($matches[1]);
            $link_tmp = explode("|", $matches[1], 2);
            $link_int  = $link_tmp[0];
            $link_text = (isset($link_tmp[1])) ? $link_tmp[1] : $link_tmp[0]; // Description of image
            $title = $matches[1]; //Link tag itself
            $tmp = explode("Image:",$title);
            if ( count($tmp) <= 1) { // We have no image, while internal links are forbidden
                return $matches[1];  // just return what you have got and quit
            }
            $align_st = "";
            $align_nd = "";
			$parts = explode("|", $matches[1]);
			$border = 0;
			if(isset($parts[4]) && !empty($parts[4]))
			{
				$border = $parts[4];
				//echo "Inside If =".$parts[4];
			}
			$align = $parts[2];
			$img_flt = '';
			if( strpos('center', $align )!== false )
			{
				$align_st = "\begin{center}";
				$align_nd = "\end{center}";
				$img_flt = 'c';
			}
			elseif( strpos('left', $align )!== false )
			{
				//$align_st = "\begin{flushleft}";
				//$align_nd = "\end{flushleft}";
				$img_flt = 'l';
			}
			elseif( strpos('right', $align )!== false )
			{
				//$align_st = "\begin{flushright}";
				//$align_nd = "\end{flushright}";
				$img_flt = 'r';
			}
			elseif( strpos('none', $align )!== false )
			{
				$align_st = "";
				$align_nd = "";
				$img_flt = 'c';
			}
			
			//mansoor
			$imagename = array_shift($parts);
			$case_imagename = $imagename;
			// still need to remove the Namespace:
			$tmp_name = explode(':', $imagename, 2);
			$imagename = $tmp_name[1];
			$caption = "";
			$imgwidth = 1;
			$imgheight = '';
			/*foreach ($parts as $part) 
			{
					if (preg_match("/\d+px/", $part)) continue;

					if (preg_match("/(\d+cm)/", $part, $widthmatch)) 
					{
						$imgwidth = $widthmatch[1];
						continue;
					}

					if (preg_match("/thumb|thumbnail|frame/", $part)) continue;
					if (preg_match("/left|right|center|none/", $part)) continue;

					$caption = trim($part);
			}*/
			if(isset($parts[5]) && !empty($parts[5]))
			{
				$imgwidth = $parts[5];
				$tmp_h = substr($imgwidth, 0, 1);
				if($tmp_h == 'x')
				{
					$imgwidth = 1;
					$imgheight = $parts[5];
					/*if(count(explode('%',$imgheight))>1)
					{
						$imgheight = preg_replace('/[^0-9.]/', '',$imgheight);
						$imgheight = $imgheight/100;
					}*/
					if(count(explode('px',$imgheight))>1)
					{
						//$imgheight = preg_replace('/[^0-9.]/', '',$imgheight);
						//$imgheight = $imgheight/706;
					}
					if(count(explode('mm',$imgheight))>1)
					{
						$imgheight = preg_replace('/[^0-9.]/', '',$imgheight);
						//$imgheight = $imgheight/706;
						$imgheight = '[height='.$imgheight.'mm]';
					}
				}
				else
				{
					if(count(explode('%',$imgwidth))>1)
					{
						$imgwidth = preg_replace('/[^0-9.]/', '',$imgwidth);
						$imgwidth = $imgwidth/100;
					}
					elseif(count(explode('px',$imgwidth))>1)
					{
						$imgwidth = preg_replace('/[^0-9.]/', '',$imgwidth);
						$imgwidth = $imgwidth/706;
					}
				}
				/*$imgwidth = preg_replace('/[^0-9.]/', '',$imgwidth);
				$imgwidth = $imgwidth/706;*/
				//echo "Inside If =".$parts[4];
			}
			else
			{
				//echo "<br/><br/>imgpath ".$imagename."<br/><br/>";
				//$img_size = getimagesize($imagename);
				//print_r($img_size);
				//$imgwidth = $img_size[0];
				//$imgwidth = $imgwidth/706;
				//$imgwidth = round($imgwidth,2);
				
				$img_nam = explode("/",$imagename);
				
				$img_nam = $img_nam[count($img_nam)-1];
				
				$magick_dir = 'identify -format "%w" ' ; 
				//$magick_dir = '"E:/Program Files/ImageMagick-6.7.6-Q16/identify -format "%w" ' ; 
				//$tmppath = printing_get_org_img_path().$tid."/originals/".$img_nam;
				$send_cmd= $magick_dir." ".$imagename;
				$result = exec($send_cmd);
				
				$imgwidth = $result/706;
				$imgwidth = round($imgwidth,2);
			}
			//echo "<br/>".$imgwidth."<br/>";
			$title = str_replace("Image:", "", $title);
			$title = str_replace("image:", "", $title);
			$res = explode("|",$title);
			//echo "<pre>";
			//print_r($res);
			$img =  $res[0];
			$resize_img = '';//$res[6];
			$res_txt = '';
			/*if(!empty($resize_img))
			{
				$resize_img = preg_replace('/[^0-9]/', '',$resize_img);
				if(strlen($resize_img)>0)
				{
					$resize_img = $resize_img*0.026458333;
					$res_txt = ' \\resizebox{'.$resize_img.'cm}{!}';
				}
			}*/
			$img1 = explode(" ",$img);
			$imagepath = $img1[0];
			//echo " img path ".$imagepath;
			$title1 = explode("|",$matches[1]);//$file->getTitle()->getText();
			/*print('<pre>');
			print_r($title1);*/
			//$caption = $title1[1];
			$hspace = '0';
			if(isset($parts[4]) && !empty($parts[4]))
			{
				$hspace = preg_replace('/[A-Za-z]*/','',$parts[4]);
			}			
			
			if(isset($parts[2]) && !empty($parts[2]))
			{
				$caption = $parts[2];
			}
			
			$graphic_package = 'graphicx';
			//$graphic_command = "\\begin{center} \\resizebox{".$imgwidth."}{!}{\includegraphics{{$imagepath}}} \\textit{{$caption}}\end{center}\n";
			//$graphic_command = "\\begin{center} \\resizebox{".$imgwidth."}{!}{\includegraphics{{$imagepath}}} \end{center}\n";
			//mansoor
			$print_ = '';
			$vtop = '0mm';
			$vbotom = '0mm';
			if(isset($parts[6]) && !empty($parts[6]))
			{
				$print_ = $parts[6];
				$print_ = str_replace("Print_=","",$print_);
				$print_ = str_replace("\"","",$print_);
				$print_ = explode(";",$print_);
				foreach($print_ as $tag)
				{
					$tmp = explode(":",$tag);
					if($tmp[0]=='vertical-top')
					{
						$vtop = $tmp[1];
					}
					if($tmp[0]=='vertical-bottom')
					{
						$vbotom = $tmp[1];
					}
					if($tmp[0]=='width')
					{
						$imgwidth = $tmp[1];
						$imgwidth = $imgwidth/706;
					}
					if($tmp[0]=='border')
					{
						$border = $tmp[1];
					}
				}
			}
			if($imgwidth == 0)
			{
				$imgwidth = 1;
			}
			if($my_params["type"] == "geometry")
			{
				$graphic_command = '\\includegraphics'.$imgheight.'[width='.$imgwidth.'\textwidth]{'.$imagepath.'}';
			}
			else
			{
				if($img_flt == 'c')//$img_flt != 'l' && $img_flt != 'r' )
				{
					$graphic_command = '\begin{figure}[h!]' .
                                            '\centering \hspace{'.$hspace.'mm}\setlength\\fboxsep{0pt} '.
						'\setlength\\fboxrule{'.$border.'pt} '.
						'\\fbox{ \includegraphics[width='.$imgwidth.'\textwidth]'.
						' {'.$imagepath.'}} \captionsetup{margin='.($hspace/2).'mm} \caption{'.$caption.'} '.
						'\hspace{'.$hspace.'mm} \end{figure}';
					//$graphic_command = $align_st." \setlength\\fboxsep{0pt} \setlength\\fboxrule{".$border."pt} \\fbox{".$res_txt."{\includegraphics{{$imagepath}}}} \n ".$align_nd;
					//echo "<br /> if ".$img_flt;
				}
				elseif($img_flt == 'n')//$img_flt != 'l' && $img_flt != 'r' )
				{
					$graphic_command = '\begin{figure}[H] \hspace{'.$hspace.'mm}\setlength\\fboxsep{0pt} '.
						'\setlength\\fboxrule{'.$border.'pt} '.
						'\\fbox{ \includegraphics[width='.$imgwidth.'\textwidth]'.
						' {'.$imagepath.'}} \captionsetup{margin='.($hspace/2).'mm} \caption{'.$caption.'} '.
						'\hspace{'.$hspace.'mm} \end{figure}';
					//$graphic_command = $align_st." \setlength\\fboxsep{0pt} \setlength\\fboxrule{".$border."pt} \\fbox{".$res_txt."{\includegraphics{{$imagepath}}}} \n ".$align_nd;
					//echo "<br /> if ".$img_flt;
				}
				else
				{

					$graphic_command = '\begin{wrapfigure}{'.$img_flt.'}{'.$imgwidth.'\textwidth}'.
						'\vspace{'.$vtop.'} \centering '.
						'\setlength\fboxsep{0pt} \setlength\fboxrule{'.$border.'pt}'.
						'\fbox{ \includegraphics[width='.$imgwidth.'\textwidth]'.
						'{'.$imagepath.'}} \captionsetup{margin='.($hspace/2).'mm}\caption{'.$caption.'} '.
						'\vspace{'.$vbotom.'} \end{wrapfigure}';
					
					//$graphic_command = " \begin{wrapfigure}{".$img_flt."}{".$wrapsize."\\textwidth} \begin{center} \setlength\\fboxsep{0pt} \setlength\\fboxrule{".$border."pt} \\fbox{".$res_txt."{\includegraphics[width=".$orgwrp."cm]{{$imagepath}}}}\end{center} \end{wrapfigure} ";//\\vspace{-90pt}
					
					
					
					//old
					/*echo "<pre>";
					print_r($img_size);*/
					//$graphic_command = " \begin{wrapfigure}[".$img_size."pt]{".$img_flt."}{2.5in} \setlength\\fboxsep{0pt} \setlength\\fboxrule{".$border."pt} \\fbox{\\resizebox{".$imgwidth."}{!}{\includegraphics[width=2in]{{$imagepath}}}}\end{wrapfigure} \n ";
					//$graphic_command = " \begin{wrapfigure}{".$img_flt."}{0.5\\textwidth} \begin{center} \setlength\\fboxsep{0pt} \setlength\\fboxrule{".$border."pt} \\fbox{".$res_txt."{\includegraphics[width=0.48\\textwidth]{{$imagepath}}}}\end{center} \end{wrapfigure} ";
					
					//echo "<br /> else ".$img_flt;
					//$graphic_command = " \setlength\\fboxsep{0pt} \setlength\\fboxrule{".$border."pt} \\fbox{".$res_txt."{\includegraphics[width=2in]{{$imagepath}}}} \n ";
				}
			}
			$this->addPackageDependency($graphic_package);
			$masked_command = $this->getMark($graphic_package);
			$this->mask($masked_command, $graphic_command);
			return $masked_command;
	}

	private function doExternalLinks( $str ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// Match everything within [...]
		$str = preg_replace_callback('/\[(.*?)\]/', array($this, 'externalLinkHelper'), $str);
		
		// Now check for plain external links:
		$str = $this->replaceFreeExternalLinks($str);
		
		$this->profileOut($fName);
		return $str;
	}
	
	/**
	 * Replace anything that looks like a URL with a link
	 * @private
	 */
	 
	function replaceFreeExternalLinks( $text ) {
		//global $wgContLang;
		//$fname = 'Parser::replaceFreeExternalLinks';
		//wfProfileIn( $fname );

		$bits = preg_split( '/(\b(?:' . wfUrlProtocols() . '))/S', $text, -1, PREG_SPLIT_DELIM_CAPTURE );
		$s = array_shift( $bits );
		$i = 0;
		//$sk = $this->mOptions->getSkin();

		while ( $i < count( $bits ) ){
			$protocol = $bits[$i++];
			$remainder = $bits[$i++];
			
			$m = array();

			if ( preg_match( '/^([^][<>"\\x00-\\x20\\x7F]+)(.*)$/s', $remainder, $m ) ) {
				# Found some characters after the protocol that look promising
				$url = $protocol . $m[1];
				$trail = $m[2];
				
				# special case: handle urls as url args:
				# http://www.example.com/foo?=http://www.example.com/bar
				if(strlen($trail) == 0 &&
					isset($bits[$i]) &&
					preg_match('/^'. wfUrlProtocols() . '$/S', $bits[$i]) &&
					preg_match( '/^([^][<>"\\x00-\\x20\\x7F]+)(.*)$/s', $bits[$i + 1], $m ))
				{
					# add protocol, arg
					$url .= $bits[$i] . $m[1]; # protocol, url as arg to previous link
					$i += 2;
					$trail = $m[2];
				}

				# The characters '<' and '>' (which were escaped by
				# removeHTMLtags()) should not be included in
				# URLs, per RFC 2396.
				$m2 = array();
				if (preg_match('/&(lt|gt);/', $url, $m2, PREG_OFFSET_CAPTURE)) {
					$trail = substr($url, $m2[0][1]) . $trail;
					$url = substr($url, 0, $m2[0][1]);
				}

				# Move trailing punctuation to $trail
				$sep = ',;\.:!?';
				# If there is no left bracket, then consider right brackets fair game too
				if ( strpos( $url, '(' ) === false ) {
					$sep .= ')';
				}

				$numSepChars = strspn( strrev( $url ), $sep );
				if ( $numSepChars ) {
					$trail = substr( $url, -$numSepChars ) . $trail;
					$url = substr( $url, 0, -$numSepChars );
				}

				//$url = Sanitizer::cleanUrl( $url );

				# Is this an external image?
				$text = false;// $this->maybeMakeExternalImage( $url );
				if ( $text === false ) {

					$text = $this->externalLinkHelper(array("[$url]", $url));
					# Not an image, make a link
					//$text = $sk->makeExternalLink( $url, $wgContLang->markNoConversion($url), true, 'free', $this->mTitle->getNamespace() );
					# Register it in the output object...
					# Replace unnecessary URL escape codes with their equivalent characters
					//$pasteurized = Parser::replaceUnusualEscapes( $url );
					//$this->mOutput->addExternalLink( $pasteurized );
				}
				$s .= $text . $trail;
			} else {
				$s .= $protocol . $remainder;
			}
		}
		//wfProfileOut( $fname );
		return $s;
	}
	
	private function externalLinkHelper($matches) {
		$match = trim($matches[1]);
		// check link for ...
		$pattern = '/(http|https|ftp|ftps):(.*?)/';
		if ( !preg_match($pattern, $match) ) {
			return "[".$match."]";
		}
		
		$hr_options = 'pdfborder={0 0 0}, breaklinks=true, pdftex=true, raiselinks=true';
		//wfRunHooks('w2lNeedHyperref', array(&$this, &$hr_options) );
		
		$this->addPackageDependency('hyperref', $hr_options);
		//$this->addPackageDependency('breakurl');
		
		if ( strstr($match, ' ') !== false ) {
			// mit Text!
			$link = explode(' ', $match, 2); // in $link[0] ist die URL!
			$linkCom = $this->maskURL($link[0], $link[1]);
			// (Befehl)(Klammerauf)(Link_masked)(Klammerzu)(Klammerauf)LinkText(Klammerzu)
		} else {
			// nur URL!
			$linkCom = $this->maskURL($match);
		}

		return $linkCom;
	}

	function maskURL($url, $text = '') {
		// (Befehl)(Klammerauf)(Link_masked)(Klammerzu)(Klammerauf)LinkText(Klammerzu)
		$mask_open  = $this->getMark('CurlyOpen');
		$mask_close = $this->getMark('CurlyClose');
		$mask_url = $this->getMark('EXTERNAL-URL');
		$mask_com = $this->getMark('LinkCommand');
                // Unmask my double backslash and put
                $mywildcard = "";
                if (strpos($url, $this->sc['endofrow']) !== false) {
                    $url = str_replace($this->sc['endofrow'], '', $url);
                    $mywildcard = $this->sc['endofrow'];
                }
		if ( '' == $text ) {
			$link = $mask_com.$mask_open.$mask_url.$mask_close.$mywildcard;
			$this->mask($mask_open,  '{');
			$this->mask($mask_close, '}');
			$this->mask($mask_url,   $url);
			$this->mask($mask_com,   '\url');
		} else {
			$link = $mask_com.$mask_open.$mask_url.$mask_close.$mask_open.$text.$mask_close.$mywildcard;
			$this->mask($mask_open,  '{');
			$this->mask($mask_close, '}');
			$this->mask($mask_url,   $url);
			$this->mask($mask_com,   '\href');
		}
		return $link;
	}
	private function extractParserExtensions( $str = '' ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$matches = array();
		$unique  = 'W2l-'.$this->uniqueString();
		//$unique .=

		$str = $this->extractTagsAndParams($this->elements, $str, $matches, $unique);

		// second: Some other aspects...
		// Now call all the registered Callback-function with their contents.
		foreach($matches as $key => $match) {
			$input = $match[1];
			$tag = $match[0];
			$argv = array();
			$argv = $match[2];
			// Submitting false as $frame for now :(
			$rpl = call_user_func($this->tags[$tag], $input, $argv, $this, false, 'latex');
			$this->tag_replace["$key"] = $rpl;
		}
		$this->profileOut($fName);

		return $str;
	}

	private function replaceParserExtensions( $str ) {
		$fName = __METHOD__;
		$this->profileIn($fName);

		$str = str_replace(array_keys($this->tag_replace), array_values($this->tag_replace), $str);
		$this->profileOut($fName);
		return $str;
	}

	private function extractTagsAndParams($elements, $text, &$matches, $uniq_prefix = ''){
		static $n = 1;
		$stripped = '';
		$matches = array();

		$taglist = implode( '|', $elements );
		$start = "/<($taglist)(\\s+[^>]*?|\\s*?)(\/?>)|<(!--)/i";

		while ( '' != $text ) {
			$p = preg_split( $start, $text, 2, PREG_SPLIT_DELIM_CAPTURE );
			$stripped .= $p[0];
			if( count( $p ) < 5 ) {
				break;
			}
			if( count( $p ) > 5 ) {
				// comment
				$element    = $p[4];
				$attributes = '';
				$close      = '';
				$inside     = $p[5];
			} else {
				// tag
				$element    = $p[1];
				$attributes = $p[2];
				$close      = $p[3];
				$inside     = $p[4];
			}

			//$marker = "($uniq_prefix-$element-" . sprintf('%08X', $n++) . '-QINU)';
			$marker = $this->getMark($element, $n++);
			$stripped .= $marker;

			if ( $close === '/>' ) {
				// Empty element tag, <tag />
				$content = null;
				$text = $inside;
				$tail = null;
			} else {
				if( $element == '!--' ) {
					$end = '/(-->)/';
				} else {
					$end = "/(<\\/$element\\s*>)/i";
				}
				$q = preg_split( $end, $inside, 2, PREG_SPLIT_DELIM_CAPTURE );
				$content = $q[0];
				if( count( $q ) < 3 ) {
					# No end tag -- let it run out to the end of the text.
					$tail = '';
					$text = '';
				} else {
					$tail = $q[1];
					$text = $q[2];
				}
			}

			$matches[$marker] = array( $element,
				$content,
				//Sanitizer::decodeTagAttributes( $attributes ),
				"<$element$attributes$close$content$tail" );
		}
		return $stripped;
	}

	private function doQuotes( $text ) {
		$fName = __METHOD__;
		$this->profileIn($fName);

		$arr = preg_split( "/(''+)/", $text, -1, PREG_SPLIT_DELIM_CAPTURE );
		if ( count( $arr ) == 1 ) {
			// No char. return;
			$this->profileOut($fName);

			return $text;
    	} else {
			# First, do some preliminary work. This may shift some apostrophes from
			# being mark-up to being text. It also counts the number of occurrences
			# of bold and italics mark-ups.
			$i = 0;
			$numbold = 0;
			$numitalics = 0;
			foreach ( $arr as $r )
			{
				if ( ( $i % 2 ) == 1 )
				{
					# If there are ever four apostrophes, assume the first is supposed to
					# be text, and the remaining three constitute mark-up for bold text.
					if ( strlen( $arr[$i] ) == 4 )
					{
						$arr[$i-1] .= "'";
						$arr[$i] = "'''";
					}
					# If there are more than 5 apostrophes in a row, assume they're all
					# text except for the last 5.
					else if ( strlen( $arr[$i] ) > 5 )
					{
						$arr[$i-1] .= str_repeat( "'", strlen( $arr[$i] ) - 5 );
						$arr[$i] = "'''''";
					}
					# Count the number of occurrences of bold and italics mark-ups.
					# We are not counting sequences of five apostrophes.
					if ( strlen( $arr[$i] ) == 2 ) $numitalics++;  else
					if ( strlen( $arr[$i] ) == 3 ) $numbold++;     else
					if ( strlen( $arr[$i] ) == 5 ) { $numitalics++; $numbold++; }
				}
				$i++;
			}

			# If there is an odd number of both bold and italics, it is likely
			# that one of the bold ones was meant to be an apostrophe followed
			# by italics. Which one we cannot know for certain, but it is more
			# likely to be one that has a single-letter word before it.
			if ( ( $numbold % 2 == 1 ) && ( $numitalics % 2 == 1 ) )
			{
				$i = 0;
				$firstsingleletterword = -1;
				$firstmultiletterword = -1;
				$firstspace = -1;
				foreach ( $arr as $r )
				{
					if ( ( $i % 2 == 1 ) and ( strlen( $r ) == 3 ) )
					{
						$x1 = substr ($arr[$i-1], -1);
						$x2 = substr ($arr[$i-1], -2, 1);
						if ($x1 == ' ') {
							if ($firstspace == -1) $firstspace = $i;
						} else if ($x2 == ' ') {
							if ($firstsingleletterword == -1) $firstsingleletterword = $i;
						} else {
							if ($firstmultiletterword == -1) $firstmultiletterword = $i;
						}
					}
					$i++;
				}

				# If there is a single-letter word, use it!
				if ($firstsingleletterword > -1)
				{
					$arr [ $firstsingleletterword ] = "''";
					$arr [ $firstsingleletterword-1 ] .= "'";
				}
				# If not, but there's a multi-letter word, use that one.
				else if ($firstmultiletterword > -1)
				{
					$arr [ $firstmultiletterword ] = "''";
					$arr [ $firstmultiletterword-1 ] .= "'";
				}
				# ... otherwise use the first one that has neither.
				# (notice that it is possible for all three to be -1 if, for example,
				# there is only one pentuple-apostrophe in the line)
				else if ($firstspace > -1)
				{
					$arr [ $firstspace ] = "''";
					$arr [ $firstspace-1 ] .= "'";
				}
			}

			# Now let's actually convert our apostrophic mush to HTML!
			$output = '';
			$buffer = '';
			$state = '';
			$i = 0;
			foreach ($arr as $r)
			{
				if (($i % 2) == 0)
				{
					if ($state == 'both')
						$buffer .= $r;
					else
						$output .= $r;
				}
				else
				{
					if (strlen ($r) == 2)
					{
						if ($state == 'i')
						{ $output .= '</i>'; $state = ''; }
						else if ($state == 'bi')
						{ $output .= '</i>'; $state = 'b'; }
						else if ($state == 'ib')
						{ $output .= '</b></i><b>'; $state = 'b'; }
						else if ($state == 'both')
						{ $output .= '<b><i>'.$buffer.'</i>'; $state = 'b'; }
						else # $state can be 'b' or ''
						{ $output .= '<i>'; $state .= 'i'; }
					}
					else if (strlen ($r) == 3)
					{
						if ($state == 'b')
						{ $output .= '</b>'; $state = ''; }
						else if ($state == 'bi')
						{ $output .= '</i></b><i>'; $state = 'i'; }
						else if ($state == 'ib')
						{ $output .= '</b>'; $state = 'i'; }
						else if ($state == 'both')
						{ $output .= '<i><b>'.$buffer.'</b>'; $state = 'i'; }
						else # $state can be 'i' or ''
						{ $output .= '<b>'; $state .= 'b'; }
					}
					else if (strlen ($r) == 5)
					{
						if ($state == 'b')
						{ $output .= '</b><i>'; $state = 'i'; }
						else if ($state == 'i')
						{ $output .= '</i><b>'; $state = 'b'; }
						else if ($state == 'bi')
						{ $output .= '</i></b>'; $state = ''; }
						else if ($state == 'ib')
						{ $output .= '</b></i>'; $state = ''; }
						else if ($state == 'both')
						{ $output .= '<i><b>'.$buffer.'</b></i>'; $state = ''; }
						else # ($state == '')
						{ $buffer = ''; $state = 'both'; }
					}
				}
				$i++;
			}
			# Now close all remaining tags. Notice that the order is important.
			if ($state == 'b' || $state == 'ib')
				$output .= '</b>';
			if ($state == 'i' || $state == 'bi' || $state == 'ib')
				$output .= '</i>';
			if ($state == 'bi')
				$output .= '</b>';
			if ($state == 'both')
				$output .= '<b><i>'.$buffer.'</i></b>';
		}
		$this->profileOut($fName);
		return $output;
	}

	private function doRegExp( $str ) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// Here we're going to run all these regexps
		$str = preg_replace($this->regexp_search, $this->regexp_replace, $str);
		$this->profileOut($fName);
		return $str;
	}

	private function doTableStuff( $str ) {
		$this->profileIn(__METHOD__);
		
		//if ( preg_match('/\\{\|/', $str) ) {
			$correct = array("\n\{|" => "\n{|", "|\}\n"=> "|}\n");
			$str = str_replace(array_keys($correct), array_values($correct), $str);
		
			//wfRunHooks("w2lTables", array( &$this, &$str ) );

			$str = $this->externalTableHelper($str);
		//}

		$this->profileOut(__METHOD__);
		return $str;
	}

        /*
       	 * Restores pre, math, and other extensions removed by strip()
	 *
	 * always call unstripNoWiki() after this one
	 * @private
	 */
	private function unstrip( $text, &$state ) {
		if ( !isset( $state['general'] ) ) {
			return $text;
		}

		//wfProfileIn( __METHOD__ );
		# TODO: good candidate for FSS
		$text = strtr( $text, $state['general'] );
		wfProfileOut( __METHOD__ );
		return $text;
	}

	/**
	 * Always call this after unstrip() to preserve the order
	 *
	 * @private
	 */
	private function unstripNoWiki( $text, &$state ) {
		if ( !isset( $state['nowiki'] ) ) {
			return $text;
		}

		//wfProfileIn( __METHOD__ );
		# TODO: good candidate for FSS
		$text = strtr( $text, $state['nowiki'] );
		//wfProfileOut( __METHOD__ );

		return $text;
	}

	private function unstripForHTML( $text ) {
		$text = $this->unstrip( $text, $this->mStripState );
		$text = $this->unstripNoWiki( $text, $this->mStripState );
		$this->addLatexHeadCode('\\newcolumntype{Y}{>{\\raggedright}X}');
		return $text;
	}
        
        private function tableAttributes ($attributes, $header = FALSE) {

            if (trim($attributes) != "") {
                if (preg_match('/style\s*=\s*"([^"].*)"/iU', $attributes, $styles)) {
                    $styles = explode(";", $styles[1]);
                    foreach ($styles as $style) {
                        $style = trim($style);

                        // Border width
                        if (strpos($style,'border-width') !== false) {
                            $border_width = explode(":", $style);
                            $border_width = explode(" ", trim($border_width[1]));
                            switch (count($border_width)) {
                                case 1:
                                    $number = (int) $border_width[0] * 0.25;
                                    $return['border-width'] =  array(
                                        "top" => $number,
                                        "right" => $number,
                                        "bottom" => $number,
                                        "left" => $number
                                    );
                                break;
                                case 2:
                                    $number_vertical = $number_horizontal = 0;
                                    if ((int) $border_width[0] != 0) {
                                        $number_horizontal = (int) $border_width[0] * 0.25;
                                    }
                                    if ((int) $border_width[1] != 0) {
                                        $number_vertical = (int) $border_width[1] * 0.25;
                                    }
                                    $return['border-width'] =  array(
                                        "top" => $number_horizontal,
                                        "right" => $number_vertical,
                                        "bottom" => $number_horizontal,
                                        "left" => $number_vertical
                                    );
                                break;
                                case 4:
                                    foreach ($border_width as $label => $border) {
                                        if ((int) $border != 0) {
                                            $borders[$label] = (int) $border * 0.25;
                                        } else {
                                            $borders[$label] = 0;
                                        }
                                    }
                                    $return['border-width'] =  array(
                                        "top" => $borders[0],
                                        "right" => $borders[1],
                                        "bottom" => $borders[2],
                                        "left" => $borders[3]
                                    );
                                break;
                            }
                            
                        }

                        // Background color
                        if (strpos($style,'background-color') !== false) {
                            $color = explode("#", $style);
                            $rgb = printing_hextorgb(trim($color[1]));
                            foreach ($rgb as $singlecolor) {
                                $colors[] = round($singlecolor/255,2);
                            }
                            $return['background-color'] = '\cellcolor[rgb]{' . implode(",", $colors) . '}';
                        }

                        // Text Align
                        if (strpos($style,'text-align') !== false) {
                            $align = explode(":", $style);
                            switch (trim($align[1])) {
                                case "left": $return['align'] = '>{\raggedright \arraybackslash\hsize@}';
                                break;
                                case "center": $return['align'] = '>{\centering \arraybackslash\hsize@}';
                                break;
                                case "right": $return['align'] = '>{\raggedleft \arraybackslash\hsize@}';
                                break;
                            }
                        }

                        // Width
                        if ((strpos($style,'width') !== false) && (strpos($style,'border-width') === false)) {
                            $width = explode(":", $style);
                            $width = (int) trim($width[1]);
                            $percent = (substr(trim($style), -1) == '%') ? TRUE : FALSE;
                            if ($header) {
                                if ($percent && ($width != 100)) {
                                    $return['width'] = round($width * 0.01, 2) . '\textwidth';
                                } elseif (!$percent) {
                                    $return['width'] = round($width / 706, 2) . '\textwidth';
                                }
                            } else {
                                if ($percent) {
                                    $percentage = $this->tabarr[count($this->tabarr) - 1]['attributes']['width'];
                                    $percentage = substr($percentage, 0, strlen($percentage) - 10);
                                    $percentage = floatval($percentage);
                                    if ($percentage != 0) {
                                        $tablewidth = $this->documentSettings['textwidth'] * $percentage;
                                    } else {
                                        $tablewidth = $this->documentSettings['textwidth'];
                                    }
                                    $return['width'] = 0.01 * $width * $tablewidth;
                                } else {
                                    $width = round($width/706,2);
                                    if ($width >= 1) {
                                        $width = 1;
                                    }
                                    $return['width'] = $this->documentSettings['textwidth'] * $width;
                                }
                            }
                        }
                    }
                }

                // Add border from table if do exists and no border is set in styles
                if ($header) {
                    if (preg_match('/border\s*=\s*"*(\d+)"*/iU', $attributes, $tableborder)) {
                        if (!isset($return['border-width']) && is_numeric($tableborder[1])) {
                            $number = $tableborder[1] * 0.25;
                            $return['border-width'] =  array(
                                "top" => $number,
                                "right" => $number,
                                "bottom" => $number,
                                "left" => $number);
                            $return['global-border'] = $number;
                        }
                    }
                } elseif (isset($this->tabarr[count($this->tabarr) - 1]['attributes']['global-border']) && (!isset($return['border-width']))) {
                    $return['border-width'] =  array(
                        "top" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border'],
                        "right" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border'],
                        "bottom" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border'],
                        "left" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border']);
                }

                if (preg_match('/rowspan\s*=\s*(\d+)/iU', $attributes, $rowspan)) {
                    $return['rowspan'] = $rowspan[1];
                }

                if (preg_match('/colspan\s*=\s*(\d+)/iU', $attributes, $colspan)) {
                    $return['colspan'] = $colspan[1];
                }

                if (preg_match('/class="([^"].*)"/iU', $attributes, $classes)) {
                    if (strpos($classes[1],'noleftmargin') !== false) {
                        $return['noleftmargin'] = TRUE;
                    }
                }
            }
            if ($header) {
                if (!isset($return['width'])) {
                    $return['width'] = '\textwidth';
                }
                if (!isset($return['align'])) {
                    $return['align'] = '>{\raggedright \arraybackslash\hsize@}';
                }
            } elseif (isset($this->tabarr[count($this->tabarr) - 1]['attributes']['global-border']) && !isset($return['border-width'])) {
                $return['border-width'] =  array(
                        "top" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border'],
                        "right" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border'],
                        "bottom" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border'],
                        "left" => $this->tabarr[count($this->tabarr) - 1]['attributes']['global-border']);
            }
            if (is_array($return)) {
                return $return;
            } else {
                return FALSE;
            }
        }
         
        private function processTables ($matches) {
            $table = $this->getMark('Table');
            $table_number = 0;
            if (is_array($this->tabarr) && count($this->tabarr) > 0) {
                $table_number = count($this->tabarr);
            }
            preg_match_all('#(?:^([|!])-|\G)(.*?)^(.+?)(?=^[|!]-|\z)#msi', $matches[3], $rows, PREG_SET_ORDER);
            $table_content = array();
            $cell_num = 0;
            $table_attributes = $this->tableAttributes($matches[1], TRUE);
            $this->tabarr[$table_number] = array("attributes" => $table_attributes);
            foreach ($rows as $row) {
                preg_match_all('#((?:^\||^!|\|\||!!|\G))(?:([^|\n]*?)\|(?!\|))?(?:\n*)(.+?)(?:\n*)(?=^\||^!|\|\||!!|\z)#msi', $row[3], $cells, PREG_SET_ORDER);
                $cell_content = array();
                foreach ($cells as $cell) {
                    $cell_content[] = array(
                        "content" => $cell[3],
                        "attributes" => $this->tableAttributes($cell[2])
                    );
                }
                if (count($cell_content) > $cell_num) {
                    $cell_num = count($cell_content);
                }
                $table_content[] = $cell_content;
            }

            $this->tabarr[$table_number] = array(
                "mark" => $table,
                "table" => $matches[0],
                "attributes" => $table_attributes,
                "content" => $table_content,
                "columns" => $cell_num
            );
            return $table;
        }

	private function externalTableHelper($text) {

            // Process all tables
            $text = preg_replace_callback (
                '#^\{\|(.*?)(?:^\|\+(.*?))?(^(?:((?R))|.)*?)^\|}#msi',
                array($this, "processTables"),
                $text
            );

            if (isset($this->sc['asteriks'])) {
                $asterisk = $this->sc['asteriks'];
            } else {
                $asterisk = $this->getMark('Asteriks');
		$this->mask($asterisk, '*');
            }

            $doubleslash = $this->getMark('Doubleslash');
            $this->mask($doubleslash, '\\\\');

            $myet = $this->getMark('Et');
            $this->mask($myet, '&');

            foreach ($this->tabarr as $tablekey => $table) {
                /*
                 * Create matrix
                 */
                $column_width = array_fill(0, $table['columns'], 0);
                $vertical_lines = array_fill(0, count($table['content']), array_fill(0, $table['columns'] + 1, 0));
                $horizontal_lines = array_fill(0, count($table['content']) + 1, $column_width);
                $matrix = array_fill(0, count($table['content']), $column_width);
                $rowiterator = 0;
                $previous_horizontal = array();

                // Set lines for whole table
                                        if (isset($table['attributes']['border-width'])) {
                            foreach ($table['attributes']['border-width'] as $position => $bwidth) {
                                switch ($position) {
                                    case "top":
                                        for ($itr = 0; $itr < $table['columns']; $itr++) {
                                            $horizontal_lines[0][$itr] = $bwidth;
                                        }
                                    break;
                                    case "bottom":
                                        for ($itr = 0; $itr < $table['columns']; $itr++) {
                                            $horizontal_lines[count($table['content'])][$itr] = $bwidth;
                                        }
                                    break;
                                    case "left":
                                        for ($itr = 0; $itr < count($table['content']); $itr++) {
                                            $vertical_lines[$itr][0] = $bwidth;
                                        }
                                    break;
                                    case "right":
                                        for ($itr = 0; $itr < count($table['content']); $itr++) {
                                            $vertical_lines[$itr][$table['columns']] = $bwidth;
                                        }
                                    break;
                                }
                            }
                        }

                // for all rows
                foreach ($table['content'] as $rowdata) {
                    // and each column
                    $row = $rowdata;
                    for ($i = 0; $i < $table['columns']; $i++) {
                        // and for all columns

                        if ($matrix[$rowiterator][$i] == 1) { // this column is taken
                            continue;
                        }

                        // We don't know which column number will assigned to this one (because there can be multicolumn)
                        $column = array_shift($row);

                        // first we will deal with colspan & rowspan (or just siply fill a cell)
                        $columnspan = array($i);
                        $rowspan = array($rowiterator);
                        if (isset($column['attributes']['colspan'])) {
                            $columnspan = range($i, $i + $column['attributes']['colspan'] - 1);
                        }

                        if (isset($column['attributes']['rowspan'])) {
                            $rowspan = range($rowiterator, $rowiterator + $column['attributes']['rowspan'] - 1);
                        }

                        

                        
                        $real_rowspan = (isset($column['attributes']['rowspan'])) ? $column['attributes']['rowspan'] : 1;
                        $real_colspan = (isset($column['attributes']['colspan'])) ? $column['attributes']['colspan'] : 1;
                        foreach ($columnspan as $single_column) {
                            $horizontal_lines[$rowiterator][$single_column] = (isset($column['attributes']['border-width']['top']) && ($column['attributes']['border-width']['top'] > $horizontal_lines[$rowiterator][$single_column])) ? $column['attributes']['border-width']['bottom'] : $horizontal_lines[$rowiterator][$single_column];
                            $horizontal_lines[$rowiterator + $real_rowspan][$single_column] = (isset($column['attributes']['border-width']['bottom']) && ($column['attributes']['border-width']['bottom'] > $horizontal_lines[$rowiterator + $real_rowspan][$single_column])) ? $column['attributes']['border-width']['bottom'] : $horizontal_lines[$rowiterator + $real_rowspan][$single_column];
                            foreach ($rowspan as $single_row) {
                                $vertical_lines[$single_row][$i] = (isset($column['attributes']['border-width']['left']) && ($column['attributes']['border-width']['left'] > $vertical_lines[$single_row][$i])) ? $column['attributes']['border-width']['left'] : $vertical_lines[$single_row][$i];
                                $vertical_lines[$single_row][$i + $real_colspan] = (isset($column['attributes']['border-width']['right']) && ($column['attributes']['border-width']['right'] > $vertical_lines[$single_row][$i + $real_colspan])) ? $column['attributes']['border-width']['right'] : $vertical_lines[$single_row][$i + $real_colspan];
                                $matrix[$single_row][$single_column] = 1;
                            }
                        }
                        // Set column width if defined
                        if (($column_width[$i] == 0) && isset($column['attributes']['width']) && !isset($column['attributes']['colspan'])) {
                            $column_width[$i] = $column['attributes']['width'];
                        }
                        $new_row[$i] = $column; // create new array of columns with proper column numbers at cells
                    }
                    $newcontent[$rowiterator] = $new_row;
                    unset($new_row);
                    $rowiterator++;
                    if ($single_column > count($table['content'])) {
                        $table['columns'] = $single_column;
                    }
                    if (count($row) > 0) {
                        $table['columns']++;
                    }
                }
                $this->tabarr[$tablekey]['content'] = $newcontent;
                $this->tabarr[$tablekey]['verticals'] = $vertical_lines;
                $this->tabarr[$tablekey]['horizontals'] = $horizontal_lines;

                // Get physical table width
                $percentage = $this->tabarr[$tablekey]['attributes']['width'];
                $percentage = substr($percentage, 0, strlen($percentage) - 10);
                $percentage = floatval($percentage);
                if ($percentage != 0) {
                    $tablewidth = $this->documentSettings['textwidth'] * $percentage;
                } else {
                    $tablewidth = $this->documentSettings['textwidth'];
                }

                // Amend column widths
                if ($this->documentSettings['units'] == 'mm') {
                    $tabcolsep = 2.12;
                } else {
                    $tabcolsep = 0.083;
                }
                $overall = 0;
                $unknown_widths = array();
                foreach ($column_width as $keycolumn => $widths) {
                    $temp_colwidth[$keycolumn] = $widths - (2 * $tabcolsep);
                    if ($widths == 0) {
                        $unknown_widths[$keycolumn] = 0;
                    } else {
                        $overall += $widths;
                    }
                }
                $column_width = $temp_colwidth;
                unset($temp_colwidth);
                $how_many_missing = count($unknown_widths);

                // Apply some logic
                if (($overall + (2 * $how_many_missing * $tabcolsep)) > $this->documentSettings['textwidth']) {
                    
                    // Table is wider than page
                    if ($how_many_missing != 0) {
                        // And we even have some columns with empty width
                        $average = round($overall/$table['columns'], 2);
                        foreach ($unknown_widths as $ukey => $val) {
                            $column_width[$ukey] = $average;
                        }
                        $overall = array_sum($column_width);
                    }
                    $shrink_of = ($overall - $this->documentSettings['textwidth']) / $this->documentSettings['textwidth'];
                    foreach ($column_width as $keycolumn => $widths) {
                        $column_width[$keycolumn] = round($widths * $shrink_of, 2);
                    }
                } elseif (($how_many_missing != 0) && (($overall + (2 * $how_many_missing * $tabcolsep)) < $this->documentSettings['textwidth'])) {
                    
                    // Some columns haven't set width, but there is space in table to fix it
                    $rest_of_place = round(($this->documentSettings['textwidth'] - $overall) / $how_many_missing, 1);
                    foreach ($unknown_widths as $ukey => $val) {
                        $column_width[$ukey] = $rest_of_place;
                    }
                }

                $this->tabarr[$tablekey]['attributes']['width'] = round($overall / $this->documentSettings['textwidth'], 2) . '\textwidth';

                /*
                 * Finally create the table and exchange it for placeholder
                 */

                $row = array();
                $rowiterator = 0;

                // Header
                $final_table = '\begin{tabularx}{' . $this->tabarr[$tablekey]['attributes']['width'] . '}{';
                foreach ($column_width as $cwidth) {
                    $final_table .= '>{\raggedright \arraybackslash\hsize' . $cwidth . $this->documentSettings['units'] . '}X';
                }
                $final_table .= "}\n";

                // Cells
                foreach ($this->tabarr[$tablekey]['content'] as $row) {
                    
                    // Add horizontal lines
                    $previousline = 0;
                    $verticalline = "";
                    foreach ($this->tabarr[$tablekey]['horizontals'][$rowiterator] as $key => $line) {

                        /*
                        if (!isset($tempvertical['start']) && ($line > 0)) {
                            $tempvertical['start'] = $key;
                        } elseif (($line > 0) && ($previousline == $line)) {
                            $tempvertical['end'] = $key;
                        } elseif (isset($tempvertical['start'])) {
                            $tempvertical['end'] = (isset($tempvertical['end'])) ? $tempvertical['end'] + 1 : $tempvertical['start'] + 1;
                            $tempvertical['start']++;
                            $final_table .= '\cmidrule[' . $previousline . 'pt]{' . $tempvertical['start'] . '-' . $tempvertical['end'] . '}';
                            unset($tempvertical);
                        }
                         * 
                         */

                            if (($previousline == $line) && isset($tempvertical['start'])) {
                                $tempvertical['end'] = $key;
                            } elseif (isset($tempvertical['start'])) {
                                $tempvertical['end'] = (isset($tempvertical['end'])) ? $tempvertical['end'] + 1 : $tempvertical['start'] + 1;
                                $tempvertical['start']++;
                                $final_table .= $newrowadd . '\cmidrule[' . $previousline . 'pt]{' . $tempvertical['start'] . '-' . $tempvertical['end'] . '}';
                                $newrowadd = "";
                                $endofrow = "\n";
                                unset($tempvertical);
                            }
                            if (!isset($tempvertical['start']) && ($line > 0)) {
                                $tempvertical['start'] = $key;
                            }

                        $previousline = $line;
                    }
                    if (isset($tempvertical['start'])) {
                        $tempvertical['end'] = (isset($tempvertical['end'])) ? $tempvertical['end'] + 1 : $tempvertical['start'] + 1;
                        $tempvertical['start']++;
                        $final_table .= '\cmidrule[' . $previousline . 'pt]{' . $tempvertical['start'] . '-' . $tempvertical['end'] . '}' . "\n";
                        unset($tempvertical);
                    } else {
                        if($rowiterator != 0) {
                            $final_table .= "\n";
                        }
                    }

                    // print column
                    $columncount = 0;
                    $column = array();
                    $first = "";
                    foreach ($row as $key => $column) {
                        $final_column_width = 0;
                        if (($first == "") && ($key != 0)) {
                            $first = $myet . " ";
                        }
                        $leftrule = $rightrule = "";
                        $color = (isset($column['attributes']['background-color'])) ? ' ' . $column['attributes']['background-color'] : '';
                        $leftrule = ($this->tabarr[$tablekey]['verticals'][$rowiterator][$key] > 0) ? '!{\vrule width ' . $this->tabarr[$tablekey]['verticals'][$rowiterator][$key] . 'pt}' : '';
                        if (isset($column['attributes']['colspan'])) {
                            $colspan = $column['attributes']['colspan'];
                            for ($i = $key; $i < ($colspan + $key); $i++) {
                                $final_column_width += $column_width[$i];
                            }
                            // We need to know line thickness
                            $all_lines = 0;
                            foreach ($this->tabarr[$tablekey]['verticals'] as $linerows) {
                                $my_lines = 0;
                                for ($itthi = $key; ($itthi < ($colspan + $key)); $itthi++) {
                                    $my_lines += $linerows[$itthi];
                                }
                                if ($my_lines > $all_lines) {
                                    $all_lines = $my_lines;
                                }
                            }
                            // convert pt into given units
                            if ($this->documentSettings['units'] == "mm") {
                                $final_column_width += $all_lines * 0.3546;
                                $final_column_width += ($colspan - 1) * ((2 * $tabcolsep) + 0.3);
                            } else {
                                //$final_column_width += $all_lines * 0.013888888888889;
                            }
                            // Correct column gap

                            $final_column_width = round($final_column_width, 2);

                        } else {
                            $colspan = 1;
                            $final_column_width = $column_width[$key];
                        }
                        if (($key + $colspan) == $table['columns']) {
                            $rightrule = ($this->tabarr[$tablekey]['verticals'][$rowiterator][$key + $colspan] > 0) ? '!{\vrule width ' . $this->tabarr[$tablekey]['verticals'][$rowiterator][$key + $colspan] . 'pt}' : '';
                        }
                        $align = (isset($column['attributes']['align'])) ? $column['attributes']['align'] : $table['attributes']['align'];
                        $align = str_replace("@", $final_column_width . $this->documentSettings['units'], $align);
                        if (isset($column['attributes']['rowspan'])) {
                            $column['content'] = trim(str_replace("\n", $doubleslash . "\n", $column['content']));
                            $column['content'] = '\multirow{' . $column['attributes']['rowspan'] . '}{' . $asterisk . '}{' . "\n" .
                                       '\setlength{\tabcolsep}{-\tabcolsep}' . "\n" .
                                       '\begin{tabular}[c]{' . $align . 'X}' . "\n" .
                                       $column['content'] . "\n" .
                                       '\end{tabular}' . "\n" .
                                       '\setlength{\tabcolsep}{\oldtabcolsep}}';
                        } else {
                            $column['content'] = trim(str_replace("\n", "\n\n", $column['content']));
                        }
                        $final_table .= $first . '\multicolumn{' . $colspan . '}{' . $leftrule . $align . 'X' . $rightrule . '}{'. trim($column['content']) . $color . '}' . "\n";
                        $sequence = $key;
                        $first = $myet . " ";
                    }
                    $rowiterator++;

                    // Add horizontal line if this is the last row
                    $endofrow = "";
                    if ($rowiterator == count($this->tabarr[$tablekey]['content'])) {
                        $newrowadd = '\tabularnewline';
                        $previousline = 0;
                        $verticalline = "";
                        foreach ($this->tabarr[$tablekey]['horizontals'][$rowiterator] as $key => $line) {
                            if (($previousline == $line) && isset($tempvertical['start'])) {
                                $tempvertical['end'] = $key;
                            } elseif (isset($tempvertical['start'])) {
                                $tempvertical['end'] = (isset($tempvertical['end'])) ? $tempvertical['end'] + 1 : $tempvertical['start'] + 1;
                                $tempvertical['start']++;
                                $final_table .= $newrowadd . '\cmidrule[' . $previousline . 'pt]{' . $tempvertical['start'] . '-' . $tempvertical['end'] . '}';
                                $newrowadd = "";
                                $endofrow = "\n";
                                unset($tempvertical);
                            }
                            if (!isset($tempvertical['start']) && ($line > 0)) {
                                $tempvertical['start'] = $key;
                            }

                            $previousline = $line;
                        }
                        if (isset($tempvertical['start'])) {
                            $tempvertical['end'] = (isset($tempvertical['end'])) ? $tempvertical['end'] + 1 : $tempvertical['start'] + 1;
                            $tempvertical['start']++;
                            $final_table .= $newrowadd . '\cmidrule[' . $previousline . 'pt]{' . $tempvertical['start'] . '-' . $tempvertical['end'] . '}' . "\n";
                            unset($tempvertical);
                        } else {
                            $final_table .= $endofrow;
                        }
                    } else {
                        $final_table .= '\tabularnewline';
                    }
                }
                $final_table .= '\end{tabularx}' . "\n";
                $text = str_replace($this->tabarr[$tablekey]['mark'], $final_table, $text);
                unset($newcontent);
            }
            return $text;
	}

	private function stripComments( $text = '' ) {
		$fName = __METHOD__;
		$this->profileIn(__METHOD__);
		/* strips out Mediawiki-comments, which are in fact HTML comments */
		$mode = '';
    		// This approach is from mediawiki
		while ( ($start = strpos($text, '<!--')) !== false ) {
			$end = strpos($text, '-->', $start + 4);
			if ($end === false) {
				# Unterminated comment; bail out
				break;
			}

			$end += 3;

			# Trim space and newline if the comment is both
			# preceded and followed by a newline
			$spaceStart = max($start - 1, 0);
			$spaceLen = $end - $spaceStart;
			while (substr($text, $spaceStart, 1) === ' ' && $spaceStart > 0) {
				$spaceStart--;
				$spaceLen++;
			}
			while (substr($text, $spaceStart + $spaceLen, 1) === ' ')
				$spaceLen++;
			if (substr($text, $spaceStart, 1) === "\n" and substr($text, $spaceStart + $spaceLen, 1) === "\n") {
				# Remove the comment, leading and trailing
				# spaces, and leave only one newline.
				$text = substr_replace($text, "\n", $spaceStart, $spaceLen + 1);
			}
			else {
				# Remove just the comment.
				$text = substr_replace($text, '', $start, $end - $start);
			}
		} // bis hierher
		$this->profileOut($fName);
		return $text;
	}

	private function doHTML($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// First step only. Needs to be far more complex!!!
		// For some HTML-Tag-support

		$replacing = array(
			'<center>'      => '\begin{center}',
			'</center>'     => '\end{center}',
			"<i>"           => '\textit{',
			"</i>"          => '}',
			"<b>"           => '\textbf{',
			"</b>"          => '}',
			"<strong>"      => '\textbf{',
			"</strong>"     => '}',
			'<em>'          => '\textit{',
			'</em>'         => '}',
			"<tt>"          => '\texttt{',
			"</tt>"         => '}',
			'<br/>'         => '\\\\',
			'<small>'       => '{\small ',
			'</small>'      => '}',
			'<big>'         => '{\large ',
			'</big>'        => '}',
			'<blockquote>'  => '\begin{quotation}',
			'</blockquote>' => '\end{quotation}'
		);
		//wfRunHooks('w2lHTMLReplace', array(&$this, &$replacing, &$str));
		$str = str_ireplace(array_keys($replacing), array_values($replacing), $str);
		
		$this->profileOut($fName);
		return $str;
	}
	
	function doDivAndSpan($str, $t = 'span') {
		/*
		$span_data['w2l-remove'] = array (
			'before' => '',
			'after'  => '',
			'filter' => 'w2lRemove',
			'callback' => '',
			'string' => '',
			'environment' => '',
		); array($this, 'doSpanAndDivReplace')
		*/

		$this->DS_tag    = $t;
		

		//foreach ( $tag_data as $class => $values ) {
		//	$this->DS_class = $class;
		//	$this->DS_values = $values;
			$str = preg_replace_callback('/(<'.$t.'(.*)>(.*)<\/'.$t.'>)/sU', array($this, 'doSpanAndDivReplace'), $str);
		//}
		//$str = preg_replace('/<div class="w2l-remove(.*)>(.*)<\/div>/smU','', $str);
		//$str = strtr($str, $abbr);
		
		return $str;
	}
	
	function doSpanAndDivReplace($matches) {
		//$str = preg_match('/<'.$t.'(.*)class=\\\\dq\{\}(.*)'.$class.'(.*)\\\\dq\{\}(.*)>(.*)<\/'.$t.'>/sU', array($this, 'doSpanAndDivReplace'), $str);
		$t = $this->DS_tag;
		$tag_data = $this->getVal($t);
		
		
		$full_block = trim(str_replace('\\dq{}','"', $matches[0]));
		$attributes = trim(str_replace('\\dq{}','"', $matches[2]));
		$content    = $matches[3];
		
		if ( !is_array($tag_data) ) {
			return $content;
		}
		
		foreach ($tag_data as $class => $values) {
			$match = array();
			if ( strpos($attributes, $class ) ) {
				// class is in here :)
				
				preg_match('/<'.$t.'(.*)class="(.*)'.$class.'(.*)"(.*)>(.*)<\/'.$t.'>/sU', $full_block, $match);

				//$content = $match[5];
				
				$other_classes = trim($match[2].' '.$match[3]);
				$otther_attr = trim($match[1].' '.$match[4]);
				$result = '';
				if ( isset($values['callback']) && $values['callback'] != '' ) {
					// Callback
					if ( is_callable($values['callback']) ) {
						$result = call_user_func_array($values['callback'], array(&$this, $content, $t, $other_classes, $match[0]));
					} else {
						return $content;
					}
			
				} elseif ( isset($values['string']) ) {
					// String
			
					if ( isset( $values['filter']) && is_callable($values['filter']) ) {
						$content = call_user_func_array($values['filter'], array(&$this, $content, $t, $other_classes));
					}
			
					$result = str_replace('%content%', $content, $values['string']);
			
				} elseif ( isset($values['environment']) && $values['environment'] != '') {
					//environment
					$result = '\begin{'.$values['environment'].'}';
					if ( isset( $values['filter']) && is_callable($values['filter']) ) {
						$result .= call_user_func_array($values['filter'], array(&$this, $content, $t, $other_classes));
					} else {
						$result .= $content;
					}
			
					$result .= '\end{'.$values['environment'].'}';
				} else { 
					// before/after/filter
					if ( isset( $values['before']) ) {
						$result .= $values['before'];
					}
			
					if ( isset( $values['filter']) && function_exists($values['filter']) ) {
						$result .= call_user_func_array($values['filter'], array(&$this, $content, $t, $other_classes));
					} else {
						$result .= $content;
					}
			
					if ( isset( $values['after']) ) {
						$result .= $values['after'];
					}
				}
				//echo "<br />result ".$result;
				return $result;
			}
		}
		
		// This would be where we would find a span or div with just a style or nothing 
		// or an unknown class; We don't transform them for now :(
		//echo "<br />contents ".$content;
		return $content;
	}
	
	/* Toolkit functions */
	private function uniqueString() {
		return dechex(mt_rand(0, 0x7fffffff)) . dechex(mt_rand(0, 0x7fffffff));
	}
	/* Profiling and debugging functions */
	private function profileIn($fName) {
		if ($this->doProfiling) {
			$time = microtime();
			$this->ProfileLog[] = array("function"=>$fName, "time"=>$time, "type" => "in");
		}
		return;
	}
	private function profileOut($fName) {
		if ($this->doProfiling) {
			$time = microtime();
			$this->ProfileLog[] = array("function"=>$fName, "time"=>$time, "type" => "out");
		}
		return;
	}
	private function profileMsg($msg) {
		if ($this->doProfiling) {
			$time = microtime();
			$this->ProfileLog[] = array("function"=>$msg, "time"=>$time, "action" => "msg");
		}
		return;
	}

	public function maskLaTeX($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$latex = array(
			'LaTeX'    => '\LaTeX{}',
			'TeX'      => '\TeX{}',
			'LaTeX 2e' => '\LaTeXe{}'
		);
		$str = strtr($str, $latex);
		$this->profileOut($fName);
		return $str;
	}

	public function maskLatexCommandChars($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// Chars, which are important for latex commands:
		// {,},\,&
		$this->Et = $this->getMark("Et");
		$this->sc['backslash'] = $this->getMark('backslash');
		
		$this->mask($this->Et, '\&');
		$this->mask($this->sc['backslash'], '\\textbackslash ');
		
		/*$chars = array(
			'\\' => $this->sc['backslash'],
			"{" => "\{",
			"}" => "\}",
			'&' => $this->Et,
		);*/
		$chars = array(
			'\\' => $this->sc['backslash'],
			"{" => "{",
			"}" => "}",
			'&' => $this->Et,
		);
		$str = strtr($str, $chars);
		$this->profileOut($fName);
		return $str;
	}

	public function maskMwSpecialChars($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// Special chars from mediawiki:
		// #,*,[,],{,},|
		$chars = array(
			'#' => "\#",
			"*" => "\(\ast{}\)",
		);
		$str = strtr($str, $chars);
		$this->profileOut($fName);
		return $str;
	}

	public function maskLatexSpecialChars($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		// _,%,§,$,&,#,€,
		$chars = array(
			'_' => '\_',
			'%' => '\%',
			'$' => '\$',
		);
		
		$str = strtr($str, $chars);
		
		$this->profileOut($fName);
		return $str;
	}

	public function getMark($tag, $number = -1) {
		// This function takes strings, which are to be inserted in verabtimenv,
		// like links, f.e.
		// returns a marker
		$fName = __METHOD__;
		$this->profileIn($fName);
		++$this->marks_counter;
		if ($number == -1) {
			$number = $this->marks_counter;
		}
		$marker = '((UNIQ-W2L-'.$this->unique.'-'.$tag.'-'.sprintf('%08X', $number).'-QINU))';
		$this->profileOut($fName);
		return $marker;
	}

	public function processCurlyBraces($str) {
		$fName = __METHOD__;
		$this->profileIn($fName);
		$new_str = '';
		if ($this->initiated == false ) {
			$this->initParsing();
		}

		++$this->curlyBraceDebugCounter;
		$this->curlyBraceLength = $this->curlyBraceLength + strlen($str);
		//$this->reportError($str, __METHOD__);
		// This function processes all templates, variables and parserfunctions
		$marker = $this->getMark('pipe');// $this->uniqueString();
		//$str = preg_replace('/\[\[(.*)\|(.*)\]\]/U', '[[$1'.$marker.'$2]]', $str);
		$test = $this->split_str($str);

		foreach($test as $part) {
			// if first
			if (substr($part, 0,2 ) == '{{' ) {
				//$part = preg_replace('/\[\[(.*)\|(.*)\]\]/U', '[[$1'.$marker.'$2]]', $part);

				$match[0] = $part;
				$match[1] = substr($part, 2, -2);
				//$this->reportError($match[0], __METHOD__);#
				//$this->reportError($match[1], __METHOD__);#

				$part = $this->doCurlyBraces($match);
				//$part = str_replace($marker, '|', $part);
			}
			$new_str .= $part;
		}

		//$str = preg_replace_callback('/\{\{(.*?)\}\}/sm', array($this, 'doCurlyBraces'), $str);

		//$new_str = str_replace($marker, '|', $new_str);
		$chars = array('\{\{\{' => '{{{', '\}\}\}' => '}}}');
		$new_str = strtr($new_str, $chars);
		$this->profileOut($fName);
		
		return $new_str;
	}

	private function doCurlyBraces($matches) {
		$orig  = $matches[0];
		$match = $matches[1];
		//$this->reportError($match, __METHOD__);
		$args = array();
		//$match = strtr($match, array("\n"=>""));
		$match = trim($match);


		// new
		if ( substr_count($match, '|') !== 0 ) {
			$tmp = explode('|', $match, 2);
			$identifier = $tmp[0];
			$args = $tmp[1];
		} else {
			$identifier = $match;
			$args = '';
		}
		$tmp = '';
		$type = $this->checkIdentifier($identifier);
		//$this->reportError($identifier."->".$type, __METHOD__);
		switch ($type) {
			case W2L_TEMPLATE:
				if ( '' == $args ) {
					// no arguments
					$args = array();
				}
				$args = $this->processArgString($args);
				// check the name

				$tmp = $this->getContentByTitle($identifier, NS_TEMPLATE);
				//$this->reportError(strlen($tmp), __METHOD__);
				$tmp = $this->preprocessString($tmp);
				//$this->reportError(strlen($tmp), __METHOD__);
				$tmp = $this->processTemplateVariables($tmp, $args);
				//$this->reportError(strlen($tmp), __METHOD__);
				$tmp = $this->processCurlyBraces($tmp);
			break;
			case W2L_PARSERFUNCTION:
				$identifier = substr($identifier, 1);
				// Now falling through, as the code ist the same now:
			case W2L_COREPARSERFUNCTION:
				$fnc = explode(':', $identifier, 2);
				$expr = $fnc[1];
				$function = $fnc[0];
				$mark = $this->getMark('pipe');

				$args = preg_replace('/\{\{\{(.*)\|(.*)\}\}\}/U', '{{{$1'.$mark.'$2}}}', $args);
				$args = $this->processCurlyBraces($args);
				$args = preg_replace('/\[\[(.*)\|(.*)\]\]/U', '[[$1'.$mark.'$2]]', $args);
				$args = explode('|',$args);// ((>)|(<))
				$new_args = array();
				foreach ($args as $value) {
					$value = str_replace($mark, '|', $value);
					$new_args[] = $value;
				}
				$tmp = $this->processParserFunction($function, $expr, $new_args);
			break;
			case W2L_TRANSCLUSION:
				if ( '' == $args ) {
					// Not sure, why this has been introduced. Commenting out the arrayx-fication, for this causes a warning...
					// no arguments
					$args = array();
				}
				$title = substr($identifier, 1);
				$args = $this->processArgString($args);
				$tmp = $this->getContentByTitle($title);
				$tmp = $this->preprocessString($tmp);
				$tmp = $this->processTemplateVariables($tmp, $args);
				$tmp = $this->processCurlyBraces($tmp);
			break;
			case W2L_VARIABLE:
				$tmp = $this->mw_vars[$identifier];
			break;
			default:
				$tmp = $orig;
			break;
		}
		return trim($tmp);
	}

	private function processArgString($str) {
		$args = array();
		$tmp = array();
		if (is_array($str) ) {
			return $str;
		}
		$tmp = explode('|', $str);
		
		$current_arg = 0;
		foreach($tmp as $keyvaluepair) {
			++$current_arg;

			if (substr_count($keyvaluepair, '=')) {
				$keyvaluepair = explode('=', $keyvaluepair, 2);
				$key = trim($keyvaluepair[0]);
				$value = trim($keyvaluepair[1]);

				$args[$key] = $value;
			} else {
				$args[$current_arg] = $keyvaluepair;
			}
		}


		return $args;
	}

	private function processTemplateVariables($str, $args = array()) {
		// replace the content by the args...
		$this->templateVars = array();
		$this->templateVars = $args;
		$str = preg_replace_callback('/\{\{\{(.*?)\}\}\}/sm', array($this, 'doTemplateVariables'), $str);
		$chars = array('{{{'=>'\{\{\{', '}}}' => '\}\}\}');
		$str = strtr($str, $chars);
		unset($this->templateVars);
		return $str;
	}
	private function doTemplateVariables($match) {
		// replace the content by the args...

		if ( substr_count($match[1],'|') ) {
			$with_default = explode('|', $match[1], 2);

			$content = $this->templateVars[$with_default[0]];

			if ( empty($content) ) {
				return $with_default[1];
			} else {
				return $content;
			}
		} else {
			$content = $this->templateVars[$match[1]];

			if ( empty($content) ) {
				return $match[0];
			} else {
				return $content;
			}
		}

	}

	private function processParserFunction($fnc, $expr, $args) {

		$params = array($this, trim($expr));
		foreach($args as $value) {
			$params[] = trim($value);
		}

		if ( array_key_exists($fnc , $this->pFunctions) ) {
			$content = call_user_func_array($this->pFunctions[$fnc], $params);
			if ( is_array($content) ) {
				return '';
			}
			return $content;
		} else {
			return '{{#'.$fnc.':'.$expr.'|'.implode('|', $args).'}}';
		}

	}

	private function split_str($str) {
		//
		$table_open_mark  = $this->getMark('table-open');
		$table_close_mark =  $this->getMark('table-close');

		$str = str_replace("\n{|", $table_open_mark, $str);
		$str = str_replace("|}\n", $table_close_mark, $str);

		$before_last_char = '';
		$last_char = '';
		$cur_char = '';
		$cb_counter = 0;
		$char_counter = 0;
		$split_array = array();
		$block = 0;
		$split_array[$block] = '';
		$in_block = false;

		$tmp_char = str_split($str);

		foreach($tmp_char as $cur_char) {
			//
			//$cur_char = $str{$char_counter};

			switch ($cur_char) {
				case '{':
					++$cb_counter;
					if ($cb_counter == 1) {
						++$block;
						$split_array[$block] = '';
						$split_array[$block] .= $cur_char;

					} else {
						$split_array[$block] .= $cur_char;
					}
				break;
				case '}':
					--$cb_counter;
					if ($cb_counter == 0) {
						$split_array[$block] .= $cur_char;

						++$block;
						$split_array[$block] = '';


					} else {
						$split_array[$block] .= $cur_char;
					}
				break;
				default:
					$split_array[$block] .= $cur_char;
				break;
			}

			$before_last_char = $last_char;
			$last_char = $cur_char;
			++$char_counter;
			//if ( !isset($str{$char_counter}) ) {


		//		break;
			//}
		}

		foreach ($split_array as $key => $value) {
	  		$value = str_replace( $table_open_mark,"\n{|", $value);
	  		$value = str_replace( $table_close_mark, "|}\n", $value);
	  		$new_split[$key] = $value;
		}

		return $new_split;
	}

	public function getContentByTitle( $title_str , $namespace = NS_MAIN) {
		$title_str =  trim($title_str);

		$title = Title::newFromText( $title_str , $namespace);

		if ( !is_a($title, 'Title') ) {
			$text = $title_str;
			$this->reportError("title_str=".$title_str, __METHOD__);
			return $text;
		}
		
		if ( !$title->UserCanRead() ) {
			return '';
		}
		
		if ( $title->exists() ) {
			$rev  = new Article( $title, 0 );
			$text = $rev->getContent();
		} else {
			$text = $title_str;
		}
		
		return $text;
	}
	
	public function checkIdentifier($str) {
		$str = trim($str);
		if ( array_key_exists($str, $this->mw_vars) )
			return W2L_VARIABLE;

		if ( '#' == $str{0} ) {
			$pf = explode(':', $str, 2);
			$pf = substr($pf[0], 1);
			if ( array_key_exists($pf, $this->pFunctions) == true) {
				return W2L_PARSERFUNCTION;
			} else {
				return false;
			}
			
		}
		if ( ':' == $str{0} )
			return W2L_TRANSCLUSION;
		
		$test = explode(':', $str, 2);
		//$this->reportError($test[0], __METHOD__);
		//$this->reportError(array_key_exists($test[0], $this->pFunctions), __METHOD__);
		if ( array_key_exists($test[0], $this->pFunctions) == true)
			return W2L_COREPARSERFUNCTION;

		return W2L_TEMPLATE;
	}

	public function reportError( $msg, $fnc ) {
		$this->error_msg[] = $fnc.': '.$msg."\n";
		$this->is_error = true;
	}

	public function getErrorMessages() {
		if ( $this->is_error == true) {
			$errors  = wfMsg('w2l_parser_protocol')."\n";
			$errors .= '<pre style="overflow:auto;">';
			foreach ($this->error_msg as $error_line) {
				$errors .= $error_line;
			}
			$errors .= '</pre>'."\n";
			return $errors;
		} else {
			return '';
		}
	}

	public function setMwVariables($vars) {
		$this->mw_vars = $vars;
		return true;
	}
	
	public function addPackageDependency($package, $options = '') {
		$this->required_packages[$package] = $options;
		return true;
	}
	
	public function addLatexHeadCode($code) {
		$this->latex_headcode[] = $code;
	}
	public function getLatexHeadCode() {
		$code = array_unique($this->latex_headcode);
		return trim(implode("\n", $code));
	}
	public function getUsePackageBlock() {
		$packages = '';
		foreach($this->required_packages as $package => $options) {
			$packages .= '\usepackage';
			if ( $options != '' ) {
				$packages .= '['.$options.']';
			}
			$packages .= '{'.$package.'}'."\n";
		}
		return trim($packages)."\n";
	}

	function parseAttrString($str) {
		$result = array();
		$con = true;
		$i = 1;
		while ($con == true) {
			$search_char = ' =';
			$str = trim($str);
			if ( empty($str) ) {
				$con = false;
				continue;
			}
			if ($i>10000) {
				$con = false;
				continue;
			}
			$str = $str.' ';
			// search for attributename...
			$howmany = strcspn($str, $search_char);
			$attr = substr($str, 0, $howmany);
			$str = substr($str, $howmany);
			
			// get value
					$attr_value = '';
			$fChar = $str{0};
			if ( $fChar == '=' ) 
				$str = substr($str, 1);
			$fChar=$str{0};
			
			if ( $fChar == '"' ) {
				// next to search for is "
						$search_char = '"';
				$str = substr($str, 1);
				$howmany = strcspn($str, $search_char);
				$attr_value = substr($str, 0, $howmany);
				$str = substr($str, ++$howmany);
			} elseif ( $fChar == "'" ) {
				$search_char = "'";
				$str = substr($str, 1);
				$howmany = strcspn($str, $search_char);
				$attr_value = substr($str, 0, $howmany);
				$str = substr($str, ++$howmany);
			} elseif ($fChar== ' ') {
				$attr_value = '';
			} else {
				$search_char = ' ';
				//$str = substr($str, 1);
				$howmany = strcspn($str, $search_char);
				$attr_value = substr($str, 0, $howmany);
				$str = substr($str, ++$howmany);
			}
			// save it to the array
			$result[$attr] = $attr_value;
			$i++;
			}
		return $result;
	}
	
	function debugMessage($caller, $message) {
		$this->debug[] = array('caller' => $caller, 'msg' => $message);
		return true;
	}
	
	function getDebugMessages() {
		$messages = '';
		
		foreach ($this->debug as $msg ) {
			$error = print_r($msg['msg'], true);
			$messages .= '<div>'.$msg['caller'].' says: <pre>'.htmlspecialchars($error).'</pre></div>';
		}
		
		if ( '' != $messages ) {
			return '<div class="w2l-debug">'.$messages.'</div>';
		} else {
			return '';
		}
	}
	
	// Functions regarding sorting and Bibtex
	function requireBibtex()  { $this->run_bibtex = true; }
	function requireSorting() { 
		$this->run_sort = true;
		$this->addPackageDependency('makeidx');
		return true;
	}
	function getBibtexState() { return $this->run_bibtex; }
	function getSortState()   { return $this->run_sort;   }

	// Wiki-Parser functions
	function &getTitle() { return $this->mTitle; }
	function disableCache() {
		return true;
	}
}


<?php

/*
 * There are managed Headers & Footers
 */

function printing_hf_show ($pid, $nid, $gid, $hfid) {
    bueditor_settle(5);
    drupal_add_js('$(document).ready(function() {BUE.processTextarea("#edit-hfcontent", "e5")});', 'inline');
    return drupal_get_form('printing_hf_form', $pid, $nid, $gid, $hfid);
}

function printing_hf_form ($form, $pid, $nid, $gid, $hfid) {
    $sql = "SELECT body, GID
            FROM {printing_hf} printing_hf
            WHERE HFID = " . $hfid;
    $result = db_fetch_array(db_query($sql));
    $node = node_load($nid);
    $tid = drillingstandardmod_get_booktid($node);
    $documentname = taxonomy_get_term($tid);
    $node = node_build_content($node);
    if (trim($node->field_subtitle[0]['value']) != '') {
        $articlename = trim($node->field_subtitle[0]['value']);
    } else {
        $articlename = trim($node->title);
    }
    $sql = "SELECT name
            FROM {printing_paper} printing_paper
            WHERE PID = " . $pid;
    $papername = db_result(db_query($sql));
    $sql = "SELECT def_LH, def_CH, def_RH, def_LF, def_CF, def_RF
            FROM {printing_geometry} printing_geometry
            WHERE GID = " . $result['GID'];
    $hfss = db_fetch_array(db_query($sql));
    foreach (array('def_LH' => 'Left header', 'def_CH' => 'Middle header', 'def_RH' => 'Right header', 'def_LF' => 'Left footer', 'def_CF' => 'Middle footer', 'def_RF' => 'Right footer') as $key => $description) {
        if ($hfss[$key] == $hfid) {
            $hfname = $description;
        }
    }
    $form = array();
    $form['markup'] = array(
        '#type' => 'markup',
        '#value' => '<h3 class="printing-subtitle"><strong>Selected document:</strong> ' . $documentname->name . "<br /> <strong>Selected article:</strong> " .
                    $articlename . "<br /> <strong>Selected paper:</strong> " . $papername . "</h3>"
        );
    $form['hfcontent'] = array(
        '#type' => 'textarea',
	'#title' => $hfname,
        '#default_value' => $result['body'],
	);
    $form['hidden'] = array(
        '#type' => 'hidden',
        '#value' => $gid
        );
    $form['hiddenpid'] = array(
        '#type' => 'hidden',
        '#value' => $pid
        );
    $form['hiddentid'] = array(
        '#type' => 'hidden',
        '#value' => $tid
        );
    $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
    $form['use'] = array('#type' => 'submit', '#value' => t('Use this'));
    $form['variables'] = array(
        '#type' => 'value',
        '#value' => array(
            'tid' => $tid,
            'pid' => $pid,
            'nid' => $nid,
            'gid' => $result['GID'],
            'hfid' => $hfid,
            'previous' => $result['body']
            )
        );
    return $form;
}

function theme_printing_hf_form ($form) {

}

function printing_hf_form_validate ($form, &$form_state) {
    if (($form_state['values']['op'] == $form_state['values']['cancel']) || ($form_state['values']['variables']['previous'] == $form_state['values']['hfcontent'])) {
        return;
    }
    module_load_include("inc", "printing", "parser/printing.helpers");
    $latex = printing_get_data($form_state['values']['variables']['pid'], $form_state['values']['variables']['nid'], FALSE, "geometry", $form_state['values']['hfcontent'], $form_state['values']['variables']['gid']);
    $record = array(
        'HFID' => $form_state['values']['variables']['hfid'],
        'latex' => $latex,
        'body' => $form_state['values']['hfcontent']
    );
    drupal_write_record('printing_hf', $record, 'HFID');
    $file = printing_geticon ("", $form_state['values']['variables']['pid'], $form_state['values']['variables']['gid'], $form_state['values']['variables']['tid']);
    $file = explode("/", $file);
    if ($file[count($file) - 1] == "default.png") {
        form_set_error('hfcontent', "Syntax error in header/footer.");
    }
}

function printing_hf_form_submit ($form, &$form_state) {
    drupal_goto("printing/geometry/" . $form_state['values']['variables']['pid'] . '/' . $form_state['values']['variables']['nid'] . '/' . $form_state['values']['variables']['gid']);
}
<?php

/*
 * There are deleted PDF files
 */

function printing_deletepdf_form ($form, $fid) {
    $form = array();
    $result = db_fetch_array(db_query("select filepath, filename from {printing_files} printing_files WHERE FID = '$fid'"));
    if (!$result) {
        $form['markup'] = array(
            '#type' => 'markup',
            '#value' => 'Nothing to delete!'
            );
        $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
        return $form;
    }
    $form['markup'] = array(
        '#type' => 'markup',
        '#value' => '<h3 class="printing-subtitle"><strong>Deleting file:</strong> ' . $result['filename'] .
                    "<br /><strong>This operation cannot be taken back!</strong></h3>"
        );
    $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
    $form['fid'] = array('#type' => 'value', '#value' => $fid);
    $form['path'] = array('#type' => 'value', '#value' => $result['filepath']);
    return $form;
}

function printing_deletepdf_form_submit ($form, &$form_state) {
    if ($form_state['values']['op'] == $form_state['values']['delete']) {
        $realfile = realpath($form_state['values']['path']);
        unlink($realfile);
        db_query("DELETE FROM {printing_files} WHERE FID = '" . $form_state['values']['fid'] . "'");
    }
    drupal_goto($_REQUEST['destination']);
}
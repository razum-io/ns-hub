<?php

/*
 * There are handled Geometry requests
 */

function printing_geometry_form ($form, $pid, $nid, $gid) {
    $form = array();
    $hfss = printing_hfnames();
    // Get geometry data
    $sql = "SELECT printing_geometry.GID, printing_geometry.name, printing_geometry.apply_to, printing_geometry.width, printing_geometry.height, printing_geometry.m_top,
            printing_geometry.m_right, printing_geometry.m_bottom, printing_geometry.m_left, printing_geometry.def_LH, printing_geometry.def_CH,
            printing_geometry.def_RH, printing_geometry.def_LF, printing_geometry.def_CF, printing_geometry.def_RF, printing_geometry.hline,
            printing_geometry.fline, printing_geometry.headsep, printing_geometry.footskip, printing_geometry.units, printing_geometry.icon,
            printing_geometry.PID, printing_geometry.headheight
            FROM {printing_geometry} printing_geometry
            WHERE printing_geometry.GID = " . $gid;
    $result = db_fetch_array(db_query($sql));
    // Get paper data
    $sql = "SELECT printing_paper.name, printing_paper.TID
            FROM {printing_paper} printing_paper
            WHERE printing_paper.PID = " . $pid;
    $paper = db_fetch_array(db_query($sql));
    $mynode = node_load($nid);
    $tid = drillingstandardmod_get_booktid($mynode);
    $documentname = taxonomy_get_term($tid);
    $mynode = node_build_content($mynode);
    if (trim($mynode->field_subtitle[0]['value']) != '') {
        $articlename = trim($mynode->field_subtitle[0]['value']);
    } else {
        $articlename = trim($mynode->title);
    }
    $disabled_form = $disabled_edit = false;
    foreach ($hfss as $buttons => $names) {
        $form[$buttons] = array(
                '#type' => 'markup',
                '#value' => '<strong><label>' . $names . ':</label></strong><div class="footbutton">Disabled</div>'
            );
    }
    if ($paper['TID'] == -1) { // Default paper
        $disabled_form = true;
        $hmessage = '<span class="warning">This geometry belongs to default paper format and cannot be amended.</span>';
    } elseif ($pid != $result['PID']) { // Different paper
        $disabled_edit = true;
        $hmessage = '<span class="warning">Only geometries belongs to current paper format can be edited, new geometry will be created if amended.</span>';
    } else { // Let's rock
        $geometries = array();
        $hmessage = "";
        foreach ($hfss as $defaults => $names) {
            if ($result[$defaults] == 1) {
                $record = array(
                    'body' => '',
                    'GID' => $gid
                );
                drupal_write_record('printing_hf', $record);
                $geometries[$defaults] = $record['HFID'];
                $form[$defaults]['#value'] = '<strong><label for="footbutton">' . $names . '</label></strong><a href="/printing/hf/' .
                          $pid . '/' . $nid . '/' . $gid . '/' . $geometries[$defaults] . '" class="footbutton">Edit</a>';
            } else {
                $form[$defaults]['#value'] = '<strong><label for="footbutton">' . $names . '</label></strong><a href="/printing/hf/' .
                          $pid . '/' . $nid . '/' . $gid . '/' . $result[$defaults] . '" class="footbutton">Edit</a>';
            }
        }
        if (count($geometries) > 0) {
            foreach($geometries as $key => $value) {
                $result[$key] = $value;
            }
            drupal_write_record('printing_geometry', $result, 'GID');
        }
    }
    $form['name'] = array(
        '#type' => 'textfield',
	'#title' => t('Geometry name'),
        '#default_value' => $result['name'],
        '#disabled' => $disabled_form
	);
    $form['amend'] = array(
        '#type' => 'radios',
	'#title' => t('Behaviour'),
        '#options' => array(
            'Amend' => 'Amend',
            'New' => 'New'
            )
	);
    if ($disabled_form) {
        $form['amend']['#disabled'] = true;
        $form['amend']['#default_value'] = 'Amend';
    } elseif ($disabled_edit) {
        $form['amend']['#disabled'] = true;
        $form['amend']['#default_value'] = 'New';
    } else {
        $form['amend']['#disabled'] = false;
        $form['amend']['#default_value'] = 'Amend';
    }
    $form['apply_to'] = array(
        '#type' => 'radios',
	'#title' => t('Apply to'),
        '#options' => array(
            'Both' => 'Both',
            'Left' => 'Left',
            'Right' => 'Right'
            ),
        '#default_value' => $result['apply_to'],
        '#disabled' => $disabled_form
	);
    $form['width'] = array(
        '#type' => 'textfield',
	'#title' => t('Width'),
        '#default_value' => $result['width'],
        '#disabled' => $disabled_form
        );
    $form['height'] = array(
        '#type' => 'textfield',
	'#title' => t('Height'),
        '#default_value' => $result['height'],
        '#disabled' => $disabled_form
        );
    $form['m_top'] = array(
        '#type' => 'textfield',
	'#title' => t('Top margin'),
        '#default_value' => $result['m_top'],
        '#disabled' => $disabled_form
        );
    $form['m_right'] = array(
        '#type' => 'textfield',
	'#title' => t('Right margin'),
        '#default_value' => $result['m_right'],
        '#disabled' => $disabled_form
        );
    $form['m_bottom'] = array(
        '#type' => 'textfield',
	'#title' => t('Bottom margin'),
        '#default_value' => $result['m_bottom'],
        '#disabled' => $disabled_form
        );
    $form['m_left'] = array(
        '#type' => 'textfield',
	'#title' => t('Left margin'),
        '#default_value' => $result['m_left'],
        '#disabled' => $disabled_form
        );
    $form['hline'] = array(
        '#type' => 'textfield',
	'#title' => t('Header line'),
        '#default_value' => $result['hline'],
        '#disabled' => $disabled_form
        );
    $form['fline'] = array(
        '#type' => 'textfield',
	'#title' => t('Footer line'),
        '#default_value' => $result['fline'],
        '#disabled' => $disabled_form
        );
    $form['headsep'] = array(
        '#type' => 'textfield',
	'#title' => t('Header space'),
        '#default_value' => $result['headsep'],
        '#disabled' => $disabled_form
        );
    $form['headheight'] = array(
        '#type' => 'textfield',
	'#title' => t('Header height'),
        '#default_value' => $result['headheight'],
        '#disabled' => $disabled_form
        );
    $form['footskip'] = array(
        '#type' => 'textfield',
	'#title' => t('Footer space'),
        '#default_value' => $result['footskip'],
        '#disabled' => $disabled_form
        );
    $form['units'] = array('#type' => 'radios',
	'#title' => t('Units'),
        '#options' => array(
            'mm' => 'mm',
            'in' => 'in'
            ),
        '#default_value' => $result['units'],
        '#disabled' => $disabled_form);

    // We can cancel action and go back to paper; use geometry, which will save it as well; or refresh preview
    $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
    $form['use'] = array('#type' => 'submit', '#value' => t('Use this'));
    $form['preview'] = array('#type' => 'submit', '#value' => t('Save/preview'), '#disabled' => $disabled_form);
    // Push some data
    $icon = $result['icon'];
    $unsetarray = array_merge(array_keys(printing_hfnames()), array('PID', 'icon', 'GID'));
    foreach($unsetarray as $unsetted) {
        unset($result[$unsetted]);
    }
    $form['hidden'] = array('#type' => 'value', '#value' => array(
        "article_name" => $articlename,
        "document_name" => $documentname->name,
        "paper_name" => $paper['name'],
        "hmessage" => $hmessage,
        "pid" => $pid,
        "nid" => $nid,
        "gid" => $gid,
        "tid" => $tid,
        "ptid" => $paper['TID'],
        "icon" => $icon,
        "database" => $result));
    return $form;
}

function theme_printing_geometry_form ($form) {
    $hfss = printing_hfnames();
    $sql = "SELECT name, GID, PID, icon
            FROM {printing_geometry} printing_geometry
            ORDER BY name ASC";
    $query = db_query($sql);
    $output = '<div class="clear-block">';
        $documentname = $form['hidden']['#value']['document_name'];
        $articlename = $form['hidden']['#value']['article_name'];
        $papername = $form['hidden']['#value']['paper_name'];
        $output .= '<h3 class="printing-subtitle"><strong>Selected document:</strong> ' . $documentname . "<br /> <strong>Selected article:</strong> " . $articlename . "<br /> <strong>Selected paper:</strong> " . $papername . "</h3>";
        $output .= '<div class="print-left-float">';
            $output .= '<fieldset><legend class="main-legend">Select geometry</legend>';
            $output .= '<div class="print-left-float-top">';
                $lis = array();
                while($row = db_fetch_array($query)) {
                    $icon = printing_geticon ($row['icon'], $form['hidden']['#value']['pid'], $row['GID'], $form['hidden']['#value']['ptid']);
                    if ($row['PID'] == $form['hidden']['#value']['pid']) {
                        $this_paper = " (this)";
                    } else {
                        $this_paper = "";
                    }
                    if ($form['hidden']['#value']['gid'] != $row['GID']) {
                        $lis[] = '<a href="/printing/geometry/' . $form['hidden']['#value']['pid'] . '/' . $form['hidden']['#value']['nid'] . '/' . $row['GID'] . '"><span id="selgem"><img src="/' . $icon . '?' .  time() . '" /><br />' . $row['name'] . $this_paper . '</span></a>';
                    } else {
                        $output .= '<div id="selgem"><img src="/' . $icon . '?' . time() . '" /><br />' . $row['name'] . $this_paper . '</div>';
                    }
                }
            $output .= '<ul>';
            $doubles = true;
            foreach ($lis as $lii) {
                if ($doubles) {
                    $output .= '<li class="gemfl clear-block">';
                }
                $output .= $lii;
                if (!$doubles) {
                    $doubles = true;
                    $output .= '</li>';
                } else {
                    $doubles = false;
                }
            }
            if (!$doubles) {
                $output .= '</li>';
            }
            $output .= '</ul></div>';
            $output .= '<div class="print-left-float-bottom">';
                $output .= '<div class="print-form-button">' . drupal_render($form['cancel']) . '</div>';
                $output .= '<div class="print-form-button">' . drupal_render($form['use']) . '</div>';
                $output .= '<div class="print-form-button">' . drupal_render($form['preview']) . '</div>';
            $output .= '</div>';
            $output .= '</fieldset>';
        $output .= '</div>';
        $output .= '<div class="print-right-float">';
            $output .= '<fieldset><legend class="main-legend">Geometry values</legend>';
            $output .= $form['hidden']['#value']['hmessage'];
                $output .= '<div class="clear-block nameselect">';
                    $output .= drupal_render($form['name']);
                    $output .= drupal_render($form['amend']);
                $output .= '</div>';
                $output .= '<div class="clear-block">';
                    // Left column
                    $output .= '<div class="basicselect-right">';
                        $output .= '<fieldset class="pagesizes"><legend>Page sizes</legend>';
                            foreach (array('width', 'height', 'm_top', 'm_bottom', 'm_left', 'm_right', 'hline', 'headsep', 'headheight',
                                            'fline', 'footskip') as $field) {
                                $output .= drupal_render($form[$field]);
                            }
                        $output .= '</fieldset>';
                    $output .= '</div>';
                    // Middle column
                    $output .= '<div class="basicselect">';
                        $output .= '<fieldset><legend>Headers &amp; footers</legend>';
                            foreach ($hfss as $piece => $pieceone) {
                                $output .= drupal_render($form[$piece]);
                            }
                        $output .= '</fieldset>';
                    $output .= '</div>';
                    // Right column
                    $output .= '<div class="basicselect-middle">';
                        $output .= '<fieldset><legend>General</legend>';
                            $output .= drupal_render($form['apply_to']);
                            $output .= drupal_render($form['units']);
                        $output .= '</fieldset>';
                    $output .= '</div>';
                $output .= '</div>';
            $output .= drupal_render($form);
            $output .= '</fieldset>';
        $output .= '</div>';
    $output .= '</div>';
  return $output;
}

function printing_geometry_form_validate ($form, &$form_state) {
    if ($form_state['values']['op'] == $form_state['values']['cancel']) {
        return;
    }
    $save = false;
    foreach ($form_state['values']['hidden']['database'] as $key => $value) {
        if ($value != $form_state['values'][$key]) {
            $save = true;
            break;
        }
    }
    if ($save) {
        $record = array();
        $pattern = '/\d*[\.|,]?\d+/';
        foreach ($form_state['values']['hidden']['database'] as $key => $value) {
            if (in_array($key,array('footskip', 'headsep', 'fline', 'hline', 'headheight', 'm_left', 'm_bottom', 'm_right', 'm_top', 'height', 'width'))) {
                preg_match($pattern, $form_state['values'][$key], $matches);
                $result = str_replace(',', '.', $matches[0]);
                $record[$key] = $result . $form_state['values']["units"];
            } else {
                $record[$key] = $form_state['values'][$key];
            }
        }
        $record['PID'] = $form_state['values']['hidden']['pid'];
        if ($form_state['values']['amend'] == "New") {
            $newname = $origname = $form_state['values']['name'];
            $sql = "SELECT name
                    FROM {printing_geometry} printing_geometry
                    ORDER BY name ASC";
            $query = db_query($sql);
            while ($name = db_fetch_array($query)) {
                $names[] = $name['name'];
            }
            $sequencer = 1;
            while (in_array($newname, $names)) {
                $newname = $origname . $sequencer;
                $sequencer++;
            }
            $record['name'] = $newname;
            $return_value = drupal_write_record('printing_geometry', $record);
        } else {
            $pieces = explode("/", $form_state['values']['hidden']['icon']);
            $pieces = $pieces[count($pieces) - 1];
            $pieces = explode(".", $pieces);
            $pieces = $pieces[0];
            if (is_file($form_state['values']['hidden']['icon']) && ($pieces == "icon")) {
                $record['icon'] = "alt";
            } else {
                $record['icon'] = "";
            }
            $record['GID'] = $form_state['values']['hidden']['gid'];
            $return_value = drupal_write_record('printing_geometry', $record, 'GID');
        }
        $form_state['values']['hidden']['gid'] = $record['GID'];
    }
}

function printing_geometry_form_submit ($form, &$form_state) {
    if ($form_state['values']['op'] == $form_state['values']['use']) {
        $record = array(
            "PID" => $form_state['values']['hidden']['pid'],
            "def_GID" => $form_state['values']['hidden']['gid']
        );
        drupal_write_record('printing_paper', $record, 'PID');
    } elseif ($form_state['values']['op'] == $form_state['values']['preview']) {
        drupal_goto("printing/geometry/" . $form_state['values']['hidden']['pid'] . '/' . $form_state['values']['hidden']['nid'] . '/' . $form_state['values']['hidden']['gid']);
    }

    drupal_goto("printing/content/" . $form_state['values']['hidden']['pid'] . '/' . $form_state['values']['hidden']['nid']);
}

function printing_hfnames() {
    return array(
        "def_LH" => "Left header",
        "def_CH" => "Middle header",
        "def_RH" => "Right header",
        "def_LF" => "Left footer",
        "def_CF" => "Middle footer",
        "def_RF" => "Right footer"
    );
}
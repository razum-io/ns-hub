<?php

function printing_progress () {
    global $user;
    $result = db_fetch_array(db_query("select count, total from {printing_progress} printing_progress WHERE session ='" . $user->sid . "' ORDER BY count ASC limit 0,1"));
    if ($result) {
        print json_encode($result);
    } else {
        print json_encode("none");
    }
}
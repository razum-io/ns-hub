<?php

function printing_content ($pid, $nid) {

    global $user;
    drupal_add_js(drupal_get_path('module', 'printing') .'/javascript/jquery.simplemodal.js');
    drupal_add_js(drupal_get_path('module', 'printing') .'/javascript/basic.js');
    db_query("DELETE FROM {printing_progress} WHERE session = '$user->sid'");

    $html = '<div id="basic-modal-content">Printing in progress<br />&nbsp;<br /><div id="counting"></div><br />&nbsp;<br /><img src="/sites/all/modules/printing/images/ajax-loader.gif" /></div>';

    return $html . drupal_get_form('printing_page_form', $pid, $nid);
}

function theme_printing_page_form($form) {
    $sql = "SELECT name, PID, TID
            FROM {printing_paper} printing_paper
            ORDER BY name ASC";
    $query = db_query($sql);
    $output = '<div class="clear-block">';
        $documentname = $form['hidden']['#value']['document_name'];
        $articlename = $form['hidden']['#value']['article_name'];
        $output .= '<h3 class="printing-subtitle"><strong>Selected document:</strong> ' . $documentname . "<br /> <strong>Selected article:</strong> " . $articlename . "</h3>";
        $output .= '<div class="print-left-float">';
            $output .= '<fieldset><legend class="main-legend">Select paper format</legend>';
            $output .= '<div class="print-left-float-top"><ul>';
                while($row = db_fetch_array($query)) {
                    if ($row['TID'] == $form['hidden']['#value']['tid']) {
                        $this_paper = " (this)";
                    } else {
                        $this_paper = "";
                    }
                    if ($form['hidden']['#value']['pid'] != $row['PID']) {
                        $output .= '<li><a href="/printing/content/' . $row['PID'] . '/' . $form['hidden']['#value']['nid'] . '">' . $row['name'] . $this_paper . '</a></li>';
                    } else {
                        $output .= '<li><span class="selected">' . $row['name'] . $this_paper . '</span></li>';
                    }
                }
            $output .= '</ul></div>';
            $output .= '<div class="print-left-float-bottom">';
                $output .= '<div class="print-form-button">' . drupal_render($form['cancel']) . '</div>';
                $output .= '<div class="print-form-button">' . drupal_render($form['save']) . '</div>';
                $output .= '<div class="print-form-button">' . drupal_render($form['submit']) . '</div>';
                $output .= '<div class="print-form-button">' . drupal_render($form['whole']) . '</div>';
            $output .= '</div>';
            $output .= '</fieldset>';
        $output .= '</div>';
        $output .= '<div class="print-right-float">';
            $output .= '<fieldset><legend class="main-legend">Paper format values</legend>';
            $output .= $form['hidden']['#value']['hmessage'];
                $output .= '<div class="clear-block nameselect">';
                    $output .= drupal_render($form['pname']);
                    $output .= drupal_render($form['amend']);
                $output .= '</div>';
                $output .= '<div class="clear-block">';
                    $output .= '<div class="basicselect">';
                        $output .= '<fieldset><legend>Global settings</legend>';
                            $output .= drupal_render($form['fontshape']);
                            $output .= drupal_render($form['fontssize']);
                            $output .= drupal_render($form['orphan']);
                            $output .= drupal_render($form['facing']);
                            $output .= drupal_render($form['geometry']);
                        $output .= '</fieldset>';
                    $output .= '</div>';
                    $output .= '<div class="basicselect-right">';
                        $output .= '<fieldset><legend>Titles</legend>';
                            $output .= '<table><tbody><thead><td>&nbsp</td><td>Font size</td><td>Font shape</td></thead>';
                                for ($i=1; $i < 7; $i++) {
                                    $output .= '<tr><td>' . drupal_render($form['headers']["title_h$i"]) . '</td><td>' .
                                               drupal_render($form['headers']["fontsize_h$i"]) . '</td><td>' .
                                               drupal_render($form['headers']["fontshape_h$i"]) . '</td></tr>';
                                }
                            $output .= '</tbody></table>';
                        $output .= '</fieldset>';
                    $output .= '</div>';
                $output .= '</div>';
                // Watermark block
                $dirarrwat = array('/Document-' . $form['hidden']['#value']['tid'], '/Paper-' . $form['hidden']['#value']['pid'], '/watermark');
                $wattpath = file_directory_path() . '/private';
                foreach ($dirarrwat as $subfolder) {
                    $wattpath .= $subfolder;
                    if (!is_dir($wattpath)) {
                        $old = umask(0);
                        mkdir($wattpath, 0777);
                        umask($old);
                    }
                }
                $watermark = $wattpath . '/watermark.png';
                if (!is_file($watermark)) {
                    $watermark = drupal_get_path("module", "printing") . '/images/watermark.png';
                }
                $output .= '<div class="clear-block"><div style="float:left;">' . '<img src="/' . $watermark . '?'  . time() .
                           '" /></div><div style="float:left;padding-left:10px;">' . drupal_render($form['image']) . '</div>' .
                           '<div style="float:left;padding: 26px 0 0 10px;">' . drupal_render($form['delete']) . '</div></div>';
                //
            $output .= drupal_render($form);
            $output .= '</fieldset>';
        $output .= '</div>';
    $output .= '</div>';
  return $output;
}

function printing_page_form($form, $pid, $nid) {
    $form = array();
    $sql = "SELECT printing_paper.name AS pname, printing_paper.fontshape, printing_paper.fontssize, printing_paper.head_1, printing_paper.head_2, printing_paper.head_3,
            printing_paper.head_4, printing_paper.head_5, printing_paper.head_6, printing_paper.orphan, printing_paper.facing, printing_paper.def_GID,
            printing_paper.TID, printing_geometry.name AS gname, printing_geometry.GID
            FROM {printing_paper} printing_paper
            LEFT JOIN {printing_geometry} printing_geometry
                ON printing_geometry.GID = printing_paper.def_GID
            WHERE printing_paper.PID = " . $pid;
    $result = db_fetch_array(db_query($sql));
    $mynode = node_load($nid);
    $tid = drillingstandardmod_get_booktid($mynode);
    $documentname = taxonomy_get_term($tid);
    $mynode = node_build_content($mynode);
    if (trim($mynode->field_subtitle[0]['value']) != '') {
        $articlename = trim($mynode->field_subtitle[0]['value']);
    } else {
        $articlename = trim($mynode->title);
    }
        //
    $form['pname'] = array(
        '#type' => 'textfield',
	'#title' => t('Paper format name'),
        '#default_value' => $result['pname'],
	);
    $form['amend'] = array(
        '#type' => 'radios',
	'#title' => t('Behaviour'),
        '#options' => array(
            'Amend' => 'Amend',
            'New' => 'New'
            )
	);
    $form['image'] = array(
        '#type' => 'file',
        '#title' => t('Watermark'),
        '#size' => 20
        );
    $form['#attributes'] = array('enctype' => "multipart/form-data");
    if ($result['TID'] == -1) {
        $form['amend']['#default_value'] = 'New';
        $form['amend']['#disabled'] = true;
        $form['image']['#disabled'] = true;
        $geometry_value = '<strong><label for="geometrybutton">Geometry:</label></strong><div id="geometrybutton">Disabled</div>';
        $hmessage = '<span class="warning">Default formats cannot be edited, new format will be created if amended.</span>';
    } elseif ($result['TID'] != $tid) {
        $form['amend']['#default_value'] = 'New';
        $form['amend']['#disabled'] = true;
        $form['image']['#disabled'] = true;
        $geometry_value = '<strong><label for="geometrybutton">Geometry:</label></strong><div id="geometrybutton">Disabled</div>';
        $hmessage = '<span class="warning">Only formats belongs to document can be edited, new format will be created if amended.</span>';
    } else {
        $hmessage = "";
        $form['amend']['#default_value'] = 'Amend';
        $form['amend']['#disabled'] = false;
        $geometry_value = '<strong><label for="geometrybutton">Geometry:</label></strong><a href="/printing/geometry/' .
                          $pid . '/' . $nid . '/' . $result['def_GID'] . '" id="geometrybutton">' . $result['gname'] . '</a>';
    }
    $form['fontshape'] = array(
        '#type' => 'select',
	'#title' => t('Font shape'),
	'#options' => array(
            'Roman' => t('Roman'),
            'Sans Serif' => t('Sans Serif'),
            ),
        '#default_value' => $result['fontshape']
        );
    $form['fontssize'] = array(
        '#type' => 'select',
	'#title' => t('Font size'),
	'#options' => array(
		'11' => t('11pt'),
		'12' => t('12pt'),
  		),
        '#default_value' => $result['fontssize']
	);
    $form['orphan'] = array(
	'#type' => 'checkbox',
        '#prefix' => '<strong>' . t('Orphan control') . ':</strong>',
	'#title' => t('Yes'),
        '#default_value' => $result['orphan']
	);
    $form['facing'] = array(
	'#type' => 'checkbox',
        '#prefix' => '<strong>' . t('Facing pages') . ':</strong>',
	'#title' => t('Yes'),
        '#default_value' => $result['facing']
	);
    $form['geometry'] = array(
        '#type' => 'markup',
        '#value' => $geometry_value
        );
        // Headers
    for ($i=1; $i < 7; $i++) {
        $parts = explode(" ", $result["head_$i"]);
            if (count($parts) == 3) {
                $font_shape = $parts[1] . " " . $parts[2];
            } elseif (count($parts) == 2) {
                $font_shape = $parts[1];
            } else {
                $font_shape = "normal";
            }
        $form['headers']["title_h$i"] = array(
            '#type' => 'markup',
            '#value' => '<strong>Title ' . $i . '</strong>'
            );
        $form['headers']["fontsize_h$i"] = array(
            '#type' => 'select',
            '#options' => array(
		'\normalsize' => t('Text size'),
		'\large' => t('Bigger'),
                '\Large' => t('Title'),
                '\LARGE' => t('Chapter'),
                '\huge' => t('Section'),
                '\Huge' => t('Huge'),
  		),
            '#default_value' => $parts[0]
            );
        $form['headers']["fontshape_h$i"] = array(
            '#type' => 'select',
            '#options' => array(
                'normal' => t('Normal'),
		'\itshape' => t('Italics'),
		'\bfseries' => t('Bold'),
                '\itshape \bfseries' => t('Bold italics'),
  		),
            '#default_value' => $font_shape
            );
    }
    $form['cancel'] = array('#type' => 'submit', '#value' => t('Cancel'));
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete watermark'));
    $form['save'] = array('#type' => 'submit', '#value' => t('Save paper'));
    $form['submit'] = array('#type' => 'submit', '#value' => t('Print page'), '#id' => 'print-page');
    $form['whole'] = array('#type' => 'submit', '#value' => t('Print document'), '#id' => 'print-document');
    foreach (array('PID', 'TID', 'pname', 'fontshape', 'fontssize', 'head_1', 'head_2', 'head_3', 'head_4', 'head_5', 'head_6', 'orphan', 'facing', 'def_GID') as $item) {
        $database[$item] = $result[$item];
    }
    $form['hidden'] = array('#type' => 'value', '#value' => array("article_name" => $articlename, "document_name" => $documentname->name, "hmessage" => $hmessage, "tid" => $tid, "pid" => $pid, "nid" => $nid, "database" => $database));
    return $form;
}

function printing_page_form_validate($form, &$form_state) {
    if ($form_state['values']['op'] == $form_state['values']['cancel']) {
        return;
    }
    $save = false;
    for ($i=1; $i < 7; $i++) {
        if ($form_state['values']["fontshape_h$i"] == 'normal') {
            $fontshapes = '';
        } else {
            $fontshapes = $form_state['values']["fontshape_h$i"];
        }
        $title[$i] = trim($form_state['values']["fontsize_h$i"] . ' ' . $fontshapes);
        if ($title[$i] != $form_state['values']['hidden']['database']["head_$i"]) {
            $save = true;
        }
    }
    if (!$save) {
        foreach (array('pname', 'fontshape', 'fontssize', 'orphan', 'facing') as $key) {
            if ($form_state['values']['hidden']['database'][$key] != $form_state['values'][$key]) {
                $save = true;
                break;
            }
        }
    }
    if ($save) {
        $record = array(
            'name' => $form_state['values']['pname'],
            'TID' => $form_state['values']['hidden']['tid'],
            'fontshape' => $form_state['values']['fontshape'],
            'fontssize' => $form_state['values']['fontssize'],
            'head_1' => $title[1],
            'head_2' => $title[2],
            'head_3' => $title[3],
            'head_4' => $title[4],
            'head_5' => $title[5],
            'head_6' => $title[6],
            'orphan' => $form_state['values']['orphan'],
            'facing' => $form_state['values']['facing'],
            'def_GID' => $form_state['values']['hidden']['database']["def_GID"]
        );
        if ($form_state['values']['amend'] == "New") {
            $newname = $origname = $form_state['values']['pname'];
            $sql = "SELECT name
                    FROM {printing_paper} printing_paper
                    ORDER BY name ASC";
            $query = db_query($sql);
            while ($name = db_fetch_array($query)) {
                $names[] = $name['name'];
            }
            $sequencer = 1;
            while (in_array($newname, $names)) {
                $newname = $origname . $sequencer;
                $sequencer++;
            }
            $record['name'] = $newname;
            $return_value = drupal_write_record('printing_paper', $record);
        } else {
            $record['PID'] = $form_state['values']['hidden']['pid'];
            $return_value = drupal_write_record('printing_paper', $record, 'PID');
        }
        $form_state['values']['hidden']['pid'] = $record['PID'];
    }
    // Handling image
    $validators = array();
    $dest = file_directory_path() . '/private/Document-' . $form_state['values']['hidden']['tid'] . '/Paper-' . $form_state['values']['hidden']['pid'] . '/watermark';
    if ($file = file_save_upload('image', $validators, $dest)) {
        // we have saved, so let's convert file
        $file_type = explode(".", $file->filename);
        if ($file_type[count($file_type) - 1] == "eps") {
            $printing_command = "cd ./$dest && ps2pdf " . $file->filename . " watermark.pdf";
        } else {
            $printing_command = "cd ./$dest && convert " . $file->filename . " watermark.pdf";
        }
        exec($printing_command, $op, $return_var);
        if ($return_var) {
            drupal_set_message("Watermark file is not valid.", 'error');
        } else {
            $printing_command = "cd ./$dest && convert -density 96 -resize x50 watermark.pdf watermark.png";
            exec($printing_command, $op, $return_var);
        }
        // Clean-up directory
        db_query('DELETE FROM {files} WHERE filepath = "%s"', $dest . '/' . $file->filename);
        $handle = opendir($dest);
        while ($my_file = readdir($handle)) {
            if (!in_array($my_file, array('watermark.pdf', 'watermark.png'))) {
                $realfile = realpath($dest . "/" . $my_file);
                if (is_file($realfile)) {
                    unlink($realfile);
                }
            }
        }
    }
}

function printing_page_form_submit($form, &$form_state) {

    global $user;
    if ($form_state['values']['op'] == $form_state['values']['submit']) {
        // Include printing functionality
        module_load_include("inc", "printing", "parser/printing.helpers");
        // Create PDF of node
        printing_create_pdf($form_state['values']['hidden']['pid'], $form_state['values']['hidden']['nid']);
    } elseif ($form_state['values']['op'] == $form_state['values']['whole']) {
        // Include printing functionality
        module_load_include("inc", "printing", "parser/printing.helpers");
        // Create PDF of document
        printing_create_pdf($form_state['values']['hidden']['pid'], $form_state['values']['hidden']['nid'], true);
    } elseif ($form_state['values']['op'] == $form_state['values']['save']) {
        drupal_goto("printing/content/" . $form_state['values']['hidden']['pid'] . '/' . $form_state['values']['hidden']['nid']);
    } elseif ($form_state['values']['op'] == $form_state['values']['delete']) {
        $dest = file_directory_path() . '/private/Document-' . $form_state['values']['hidden']['tid'] . '/Paper-' . $form_state['values']['hidden']['pid'] . '/watermark';
        $handle = opendir($dest);
        while ($my_file = readdir($handle)) {
            $realfile = realpath($dest . "/" . $my_file);
            if (is_file($realfile)) {
                unlink($realfile);
            }
        }
        drupal_goto("printing/content/" . $form_state['values']['hidden']['pid'] . '/' . $form_state['values']['hidden']['nid']);
    }
    // Go to node where you did come from
    drupal_goto("node/" . $form_state['values']['hidden']['nid']);
}
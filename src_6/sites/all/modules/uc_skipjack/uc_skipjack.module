<?php

// $Id: uc_skipjack.module,v 1.2 2010/09/22 12:18:56 longwave Exp $

/**
 * Implementation of hook_payment_gateway().
 */
function uc_skipjack_payment_gateway() {
  $gateways[] = array(
    'id' => 'skipjack',
    'title' => t('Skipjack'),
    'description' => t('Process credit card payments using the Skipjack Financial Services gateway.'),
    'settings' => 'uc_skipjack_settings_form',
    'credit' => 'uc_skipjack_charge',
  );

  return $gateways;
}

/**
 * Payment gateway settings form.
 */
function uc_skipjack_settings_form() {
  if (!function_exists('curl_init')) {
    drupal_set_message(t('The Skipjack service requires cURL. Please talk to your system administrator to get this configured.'));
  }

  $form["uc_skipjack_authorize_server"] = array(
    '#type' => 'select',
    '#title' => t('Skipjack transaction server'),
    '#default_value' => variable_get('uc_skipjack_authorize_server', 'development'),
    '#options' => array(
      'development' => 'Development',
      'production' => 'Production',
    ),
  );
  $form["uc_skipjack_html_serial_number"] = array(
    '#type' => 'textfield',
    '#title' => t('HTML Serial Number'),
    '#default_value' => variable_get('uc_skipjack_html_serial_number', ''),
    '#size' => 12,
    '#maxlength' => 20,
    '#required' => TRUE,
  );
  $form["uc_skipjack_developer_serial_number"] = array(
    '#type' => 'textfield',
    '#title' => t('Developer Serial Number'),
    '#default_value' => variable_get('uc_skipjack_developer_serial_number', ''),
    '#size' => 12,
    '#maxlength' => 20,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Callback for processing a credit card transaction.
 */
function uc_skipjack_charge($order_id, $amount, $data) {
  global $user;
  $order = uc_order_load($order_id);

  $order_string = '';
  foreach ($order->products as $product) {
    $order_string .= $product->model ."~". $product->title ."~". $product->price ."~". $product->qty ."~N~||";
  }

  $submit_data = array(
    'SerialNumber' => variable_get('uc_skipjack_html_serial_number', ''),

    // Billing details.
    'SJName' => $order->billing_first_name .' '. $order->billing_last_name,
    'Email' => $order->primary_email,
    'StreetAddress' => $order->billing_street1,
    'StreetAddress2' => $order->billing_street2,
    'City' => $order->billing_city,
    'State' => uc_get_zone_code($order->billing_zone),
    'ZipCode' => $order->billing_postal_code,
    'Phone' => $order->billing_phone,

    // Delivery details.
    'ShipToName' => $order->delivery_first_name .' '. $order->delivery_last_name,
    'ShipToStreetAddress' => $order->delivery_street1,
    'ShipToStreetAddress2' => $order->delivery_street2,
    'ShipToCity' => $order->delivery_city,
    'ShipToState' => uc_get_zone_code($order->delivery_zone),
    'ShipToZipCode' => $order->delivery_postal_code,
    'ShipToPhone' => $order->delivery_phone ? $order->delivery_phone : '0000000000',

    // Order and credit card details.
    'OrderNumber' => $order->order_id,
    'AccountNumber' => $order->payment_details["cc_number"],
    'Month' => $order->payment_details["cc_exp_month"],
    'Year' => $order->payment_details["cc_exp_year"],
    'CVV2' => $order->payment_details["cc_cvv"],
    'TransactionAmount' => uc_currency_format($amount, FALSE, FALSE, '.'),
    'OrderString' => $order_string,
  );

  switch (variable_get('uc_skipjack_authorize_server', 'development')) {
    case 'development':
      $url = 'https://developer.skipjackic.com/scripts/evolvcc.dll?AuthorizeAPI';
      $submit_data['DeveloperSerialNumber'] = variable_get('uc_skipjack_developer_serial_number', '');
      break;

    case 'production':
      $url = 'https://www.skipjackic.com/scripts/evolvcc.dll?AuthorizeAPI';
      break;
  }

  $post_data = array();
  foreach ($submit_data as $key => $value) {
    if ($value) {
      $post_data[] = $key .'='. urlencode($value);
    }
  }
  $post_data = implode('&', $post_data);

  // Send the cURL request and retrieve the response.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_VERBOSE, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
  $response = curl_exec($ch);

  if ($error = curl_error($ch)) {
    watchdog('uc_skipjack', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    return array('success' => FALSE);
  }

  curl_close($ch);

  // Parse the response string into a usable array format.
  $response = explode("\n", $response);
  $keys = str_replace('"', '', split('","', $response[0]));
  $values = str_replace('"', '', split('","', $response[1]));
  $response = array_combine($keys, $values);

  if ($response['szIsApproved'] == 1 && $response['szReturnCode'] == 1 && !empty($response['AUTHCODE'])) {
    // Sale approved.
    $message = t('Transaction ID: @id<br />Authorization code: @code', array('@id' => $response['szTransactionFileName'], '@code' => $response['AUTHCODE']));
    $result = array(
      'success' => TRUE,
      'comment' => $message,
      'message' => $message,
      'data' => array('module' => 'uc_skipjack', 'txn_id' => $response['szTransactionFileName']),
      'uid' => $user->uid,
    );
  }
  else {
    // Sale declined.
    $message = _uc_skipjack_parse_return_code($response['szReturnCode']);
    $result = array(
      'success' => FALSE,
      'message' => t('Credit card payment declined: @message', array('@message' => $message)),
      'uid' => $user->uid,
    );
  }

  uc_order_comment_save($order_id, $user->uid, $message, 'admin');

  return $result;
}

function _uc_skipjack_parse_return_code($code) {
  switch ($code) {
    case "1": return t("Declined by issuing bank.");
    case "0": return t("Communication Failure");
    case "-1": return t("Error in request - Data was not by received intact by Skipjack Transaction Network.");
    case "-35": return t("Invalid credit card number");
    case "-37": return t("Skipjack is unable to communicate with payment processor.");
    case "-39": return t("Invalid HTML Serial Number.");
    case "-51": return t("The value or length for billing zip code is incorrect.");
    case "-52": return t("The value or length for shipping zip code is incorrect.");
    case "-53": return t("The value or length for credit card expiration month is incorrect.");
    case "-54": return t("Credit card account number incorrect.");
    case "-55": return t("The value or length or billing street address is incorrect.");
    case "-56": return t("The value or length of the shipping address is incorrect.");
    case "-57": return t("The length of the transaction amount must be at least 3 digits.");
    case "-58": return t("Merchant Name associated with Skipjack account is misconfigured or invalid.");
    case "-59": return t("Merchant Address associated with Skipjack account is misconfigured or invalid.");
    case "-60": return t("Length or value in Merchant State misconfigured or invalid.");
    case "-61": return t("The value or length for shipping state/province is empty.");
    case "-62": return t("The value for length orderstring is empty.");
    case "-64": return t("Error invalid phone number.");
    case "-65": return t("The value or length for billing name is empty.");
    case "-66": return t("Error empty e-mail.");
    case "-67": return t("Error empty street address.");
    case "-68": return t("Error empty city");
    case "-69": return t("Error empty state");
    case "-70": return t("Empty zipcode");
    case "-71": return t("Empty ordernumber");
    case "-72": return t("Empty accountnumber");
    case "-73": return t("Empty month");
    case "-74": return t("Empty year");
    case "-75": return t("Empty serialnumber");
    case "-76": return t("Empty transactionamount");
    case "-77": return t("Empty orderstring");
    case "-78": return t("Empty shiptophone");
    case "-79": return t("Length or value sjname");
    case "-80": return t("Length shipto name");
    case "-81": return t("Length or value of Customer location");
    case "-82": return t("The value or length for billing state is empty.");
    case "-83": return t("The value or length for shipping phone is empty.");
    case "-84": return t("There is already an existing pending transaction in the register");
    case "-85": return t("Airline leg field value is invalid or empty.");
    case "-86": return t("Airline ticket info field is invalid or empty");
    case "-87": return t("Point of Sale check routing number is invalid or empty.");
    case "-88": return t("Point of Sale check MICR invalid or empty.");
    case "-89": return t("Point of Sale check MICR invalid or empty.");
    case "-90": return t("Point of Sale check number invalid or empty.");
    case "-91": return t("CVV2 Invalid or empty");
    case "-92": return t("Approval Code Invalid. Approval Code is a 6 digit code.");
    case "-93": return t("'Allow Blind Credits' option must be enabled on the Skipjack Merchant Account.");
    case "-94": return t("Blind Credits Failed");
    case "-95": return t("Voice Authorization Request Refused");
    case "-96": return t("Voice Authorizations Failed");
    case "-97": return t("Fraud Rejection");
    case "-98": return t("Invalid Discount Amount");
    case "-99": return t("POS PIN Debit Pin Block");
    case "-100": return t("Debit-specific Number");
    case "-101": return t("Data for Verified by Visa/MC Secure Code is invalid.");
    case "-102": return t("Authentication Data Not Allowed");
    case "-103": return t("POS Check Invalid Birth Date");
    case "-104": return t("POS Check Invalid Identification Type");
    case "-105": return t("Track Data is in invalid format.");
    case "-106": return t("POS Check Invalid Account Type");
    case "-107": return t("POS PIN Debit Invalid Sequence Number");
    case "-108": return t("Invalid Transaction ID");
    case "-109": return t("Invalid From Account Type");
    case "-110": return t("Pos Error Invalid To Account Type");
    case "-112": return t("Pos Error Invalid Auth Option");
    case "-113": return t("Pos Error Transaction Failed");
    case "-114": return t("Pos Error Invalid Incoming Eci");
    case "-115": return t("POS Check Invalid Check Type");
    case "-116": return t("POS Check Invalid Lane Number");
    case "-117": return t("POS Check Invalid Cashier Number");
    case "-118": return t("Invalid POST URL");
    default: return t("Unknown Skipjack return code");
  }
}

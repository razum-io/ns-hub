<?php
# Patch file location, relative to Drupal root.
# See https://bitbucket.org/davereid/drush-patchfile
$options['patch-file'] = '../conf/patches.make';

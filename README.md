## Docker support

Just unzip files in docker/db_init_x and then run 'docker-compose up -d'

You might need content of 'files' folder for Drupal 6 - if yes, just give me a shout at seogow@gmail.com.